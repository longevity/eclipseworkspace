package dpat.tests.functional.sensors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dpat.tests.RunTest;
import dpat.tests.functional.alarm.TOset.Alarm;
import dpat.coreLib.common.config.Parameters.Params;
import dpat.coreLib.common.config.Sensors;
import dpat.coreLib.common.constants.ApplicationTypes;
import dpat.coreLib.common.constants.RoleIDs;
import dpat.coreLib.common.constants.SensorREFModes;
import dpat.coreLib.common.Do;
import dpat.coreLib.common.MethodSignature;
import dpat.coreLib.indicators.NumberDataInd;
import dpat.coreLib._model.sensorFaults.Gyro_Faults;
import dpat.coreLib._model.sensorFaults.SensorFaultsSet;
import dpat.coreLib._model.sensorFaults.Wind_Faults;
import dpat.coreLib.panels.PanelSwitcher;
import dpat.coreLib.panels.SensorPanel;
import dpat.coreLib.panels.ParamPanel;
import dpat.coreLib.parameters.ParameterWithChangeEditor;

/**
 * ��������� � Visual �� ����������� ��� ������� ������� ��� ��������� � ����������� ������ ��� ������� ������:
 * ����������� ����������� ��������� ������, �������� �, ����� ������ ������ ������������ ��� ������� ������� �� �����������.
 * - SNS_NEG_WindSpecificFormatErrors.java - ����������� ������ ������� �������(�-�: ��� Wind Shadow/Squall).
 * - SNS_SCN_Wind_Errors.java - ��������� ���� �� ������� ������ ��� �������� Wind.
 * - SNS_NEG_CommonFormatErrors.java - ��������� ���� �� ��������� ������ ��� �������� Wind.
 * @author v.kulikov
 */
public class SNS_NEG_WindSpecificFormatErrors extends RunTest {
	
	public dpat.tests.functional.sensors.TOset tosSNS;
	public dpat.tests.functional.alarm.TOset tosAlarm;
	public dpat.tests.functional.parameters.TOset tosParam;
	
	/* ���������-���� ��� ��������� ���������� ��� �������. */
	private final String R_DIR = "R DIR.TitleRaw";
	private final String SPD = "SPD.TitleRaw";
	
	@Override
	public void initTOsets() throws Exception {
		/*  ������������� ��������	 */
		fillAllowedAppList( RoleIDs.appVisual1, new ApplicationTypes[] {ApplicationTypes.Visual} );
		this.tosSNS = new dpat.tests.functional.sensors.TOset(RoleIDs.appVisual1);
		this.tosAlarm = new dpat.tests.functional.alarm.TOset(RoleIDs.appVisual1);
		this.tosParam = new dpat.tests.functional.parameters.TOset(RoleIDs.appVisual1);
	}
	
	@Override
	public void preconditions() throws Exception {
		/// ����� DP ������ ���� ������� ��� ���������� ���� �������� ������ ����� - RunTest.
		
		Do.debug("Check that Sensors Gyro and Wind are in this configuration");
		Do.dpAssert(Sensors.Gyro.totalELTnum() > 0 && Sensors.Wind.totalELTnum() > 0, 
															"Sensors Gyro and Wind are not in this configuration");
		/// � ������ �������� ������� Gyro � Wind.
		tosModel.btSensorFaults.pressAny();
		tosModel.switchSensorsOrREFsGroupON(Sensors.Wind);
		
		/// ������������ ������� ����������(�����, �����, �������) � ������ � ������.
		tosModel.deactivateAllSensorsOrREFsGroupErrors(Sensors.Wind);
		// tosModel.btMotionSignals.pressVW();
		// TODO: DP-15229 tosModel.motionSignals.deactivateGeneralDisturbance();
		
		/** �� ����� (�����) ������ ���������� ������������� ����� �.�. ������� Sensors; 
		  * �� ������ (������) ������� ���� Visual � DP ����������� Alarm
		  */
		ArrayList<PanelSwitcher> pSW = PanelSwitcher.createAllpsws(getAllowedAppList().keySet());
		for( PanelSwitcher each :pSW ) each.bringPanelRight("Alarm");
	}

	@Override
	public void scenario() throws Exception {
		/// �������� ������ ������������.
		ArrayList<SensorPanel> sensGyro = tosSNS.getAllSensorsInGroup(Sensors.Gyro);
				
		for(SensorPanel sensorGyro : sensGyro){
			///> ���������� ����� ���������� Sensor Faults ��� ������� magnetic
			SensorFaultsSet snsModelFaults = tosModel.sensorsFaults.getParamSet(sensorGyro.sensorGroup, sensorGyro.getNum());
			
			if(sensorGyro.isMagnetic()){
				///- ���������, ��� ������� HCHDM ������� magnetic - ������������.
				snsModelFaults.activateProtocol(sensorGyro.sensorGroup, Gyro_Faults.HCHDM);
			} else {
				///- ���������, ��� ������� HEHDT �������� ������� - ������������.
				snsModelFaults.activateProtocol(sensorGyro.sensorGroup, Gyro_Faults.HEHDT);
			}
		}
		
		/// ����� �������� ��������� ������� � �������� Gyro �������� �� � ������ � �������.
		tosModel.switchSensorsOrREFsGroupON(Sensors.Gyro);
		
		ArrayList<Integer> gyroSel = tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false);
		Do.debug("Switch sensors " + Sensors.Gyro + " to mode Select"); // ������ �������� ��������, ������������ � Select.
				
		if(gyroSel.size() < 1) Do.skip("Less than 1 sensor " + Sensors.Gyro + " selected . Check mode of this sensor.");
		
		ArrayList<Integer> windSel = new ArrayList<>(); // ������ �������� �������� Gyro � Wind, ������������ � ����� Select.
	
		/// ���������� ������� ������ Wind � ����� Select(�������������, ��� ������������� ����������).
		windSel = tosSNS.setWholeGroupFast(Sensors.Wind, SensorREFModes.Select, false);
		Do.debug("Switch sensors to mode Select");
		
		/** ����������, ���� �� ��������� Wind (���� �� 1).
		  * ���� ��������� Wind ��� ������, ���-�� ����� ������������ ������� ������, ���� ��������� ��� �� ������.
		  * ��������� ����������� ����� ������������ ��� skipped � ��������� � ������ ��������.
		  */
		if(windSel.size() < 1){
			Do.skip("Less than 1 sensor " + Sensors.Wind + " selected . Check mode of this sensor.");
		}
		
		ParamPanel units = this.tosParam.params.get(Params.UNITS);
		ParameterWithChangeEditor windSpeed = units.get�hangeEditParameter("Wind Speed");
		
		String[] modes = {"kn", "m/s", "km/h"};
		
		/// ��������� Squall/Shadow ��� ���� ������������ �������� ����� [kn, m/s, km/h].
		for(String mode : modes){
			ArrayList<PanelSwitcher> pSW = PanelSwitcher.createAllpsws(getAllowedAppList().keySet());
			
			///> �� ������ ������� ���� Visual � DP ����������� Param.
			for( PanelSwitcher each : pSW ) each.bringPanelRight("Param");
			
			///> ��������� ������ ����������� �� ������ [kn, m/s, km/h].
			Do.debug("Change dimension of Wind Speed to "+mode+" in the window Param -> Units.");
			
			///> ������� �� ����� Param -> Units.
			this.tosParam.bringParamPanel(units);
			
			///> ���������� � ���� Param -> Units � Wind Speed ������ �����������.
			windSpeed.setCurVal(mode);
			units.pressApply();
			
			///> �� ������ ������� ���� Visual � DP ����������� Alarm.
			for( PanelSwitcher each : pSW ) each.bringPanelRight("Alarm");
			
			///> ������� ������� �� ������, ��������� ������� Squall/Shadow.
			for (int i=0; i<windSel.size(); i++){
				///- �������� ��������� �������(������), ����� �������� � ���.
				SensorPanel sensor = tosSNS.snsesAll.get(Sensors.Wind)[i];
				Do.debug("Check "+sensor.button.getCaption());
				
				///- ������� �� ����� ������ ������ � ����������� ��� ���������.
				tosSNS.bringSensorPanel(sensor);
				
				///- ��������� ������ �� ������� ����������� ��������� ������.
				for(Wind_Faults windFault : Wind_Faults.values()){
					if(windFault.equals(Wind_Faults.Squall) || windFault.equals(Wind_Faults.Shadow)){
						/**
	    				 * - ������� ����� checkWindSpecificFormatError(SensorPanel sensor, Wind_Faults windFault, String mode)
	    				 * - ��� �������� ��������� �������� ������� Wind ����� ����������� Squall/Shadow..
	    				 */
						checkWindSpecificFormatError(sensor, windFault, mode);
					}
				}
			}
		}
	}
	
	/**> ����� ��� �������� �����������(Dir/SPD) ������� Wind ����� ����������� ����������� ��������� ������ Squall/Shadow.
	*@param sensor ������� ������(������).
	*@param windFault ��� ������ (�-�: Squall, Shadow).
	*@param mode ����������� (�-�: kn, m/s, km/h).
	**/
	private void checkWindSpecificFormatError(SensorPanel sensor, Wind_Faults windFault, String mode) {
		/**
		 * > ������� ����� saveCurrentResults(SensorPanel sensor)
		 * > ��� �������� ��������� �������� ������� Wind ����� ����������� Squall/Shadow..
		 * > ����� ������������� ������ ������� ���������� �������� ����������� data | deviation.
		 */
		Map<String, Double> values = saveCurrentResults(sensor);
		
		int snsNum = sensor.getNum();
		int index = snsNum - 1;
		
		// ��������� ����� ����������, ��� ������ � �������� ������� � ������.
		SensorFaultsSet sensFaultSet = tosModel.sensorsFaults.getParamSet(sensor.sensorGroup, snsNum);
		
		// �������� ����������� ����� ������������� ������(����� ��� ���������).
		double oldValOfSpeed = 0.0;
		double oldValOfDir = 0.0;
		
		// �������� ����������� ����� ������������ ������.
		double currValOfSpeed = 0.0;
		double currValOfDir = 0.0;
		
		// ������� ��� ���������.
		double range = 1.0;
		
		///> ���������� ������ Squall/Shadow.
		sensFaultSet.activate(windFault.name());
		
		Do.debug("Check Specific Format Error: " + windFault.name());
		
		// �������(currValOfDir) - ����������(oldValOfDir) = ������������ ���������(DEV_DIR_OF_SHADOW).
		switch(windFault){
		case Shadow:
			// ��������� ��������� ������ Shadow ��� �����������. ����������� [�������].
			final double DEV_DIR_OF_SHADOW = 10.0;
			
			oldValOfDir = values.get(R_DIR);
			
			///> �������� ���������� �� ����������� � �������� ������� Wind �� 10 ��������, � ����� ������ ��������, ��� ������ ���� ����������.
			NumberDataInd ndiDir;
			
			if(sensor.isPriority()){
				ndiDir = tosSNS.snsesAll.get(Sensors.Wind)[index].dataIndicators.get(R_DIR);
				
				///- ��� ���� ���������� ����������(data) ����������� ���������� �� 10 ��������.
				Do.check4actionTO(ndiDir, "Direction of priority sensor Wind is not correct." + ndiDir.getNumberVal(),
				new MethodSignature(true, "isEqualInRangeToValue", ndiDir.getNumberVal(), range, DEV_DIR_OF_SHADOW, 1, 180));
			} else {
				ndiDir = tosSNS.snsesAll.get(Sensors.Wind)[index].deviationIndicators.get(R_DIR);
				
				///- ��� ���� ���������� ����������(deviation) ����������� ���������� �� 10 ��������.
				Do.check4actionTO(ndiDir, new MethodSignature(true, "isEqualInRangeToValue", ndiDir.getNumberVal(), range, DEV_DIR_OF_SHADOW, 1, 180));
			}
			Do.debug("Deviation of dir wind: current Value: " + currValOfDir + " - " + oldValOfDir + " = " + (currValOfDir-oldValOfDir));
			
			///> �������� ���������� ������� �������� �� 10 ��������.
			Do.dpAssert(ndiDir.isEqualInRangeToValue(oldValOfDir, range, DEV_DIR_OF_SHADOW, 1, 180), 
															"Error! Data deviation of direction current Wind not equal 10 degrees");
			/**
			 * > ������� ����� checkSensor(SensorPanel sensor, Wind_Faults windFault, boolean state)
			 * > ��� �������� ��������� �������� ������� Wind.
			 */
			checkSensor(sensor, windFault, true);
			break;	
		case Squall:
			// �������� ���������� ������ Squall ��� �����������. ����������� [����|m/s|�������].
			double DEV_SPEED_OF_SQUALL = 0.0;
			
			final double DEV_DIR_OF_SQUALL = 45.0;
			
			if("kn".equals(mode)){
				DEV_SPEED_OF_SQUALL = 30.0;
			} else
			if("m/s".equals(mode)){
				DEV_SPEED_OF_SQUALL = 15.4;
			} else
			if("km/h".equals(mode)){
				DEV_SPEED_OF_SQUALL = 55.6;
			} else {
				Do.stop("Unrecognized mode of Speed");
			}
			
			Do.debug("Speed of Squall equals "+DEV_SPEED_OF_SQUALL);
			
			// �������� �������� �������� �� ����������� ������ Squall.
			oldValOfSpeed = values.get(SPD);
			
			// �������� �������� ����������� �� ����������� ������ Squall.
			oldValOfDir = values.get(R_DIR);
			
			NumberDataInd dir;
			NumberDataInd spd;
			
			
			if(sensor.isPriority()){
				///- �������� ��������� �������� � ����������� ��� ������������� �������(data).
				spd = tosSNS.snsesAll.get(Sensors.Wind)[index].dataIndicators.get(SPD);
				dir = tosSNS.snsesAll.get(Sensors.Wind)[index].dataIndicators.get(R_DIR);
			} else {
				///- �������� ���������� �������� � ����������� ��� ��������������� �������(deviation).
				spd = tosSNS.snsesAll.get(Sensors.Wind)[index].deviationIndicators.get(SPD);
				dir = tosSNS.snsesAll.get(Sensors.Wind)[index].deviationIndicators.get(R_DIR);	
			}
			
			///> ��� ���� ���������� ���������� �������� ���������� �� 30 �����.
			Do.check4actionTO(spd, new MethodSignature(true, "isEqualInRangeToValue", spd.getNumberVal(), range, DEV_SPEED_OF_SQUALL, 1, 180));
			
			///> ��� ���� ���������� ���������� ����������� ���������� �� 45 ��������.
			Do.check4actionTO(dir, new MethodSignature(true, "isEqualInRangeToValue", oldValOfDir+DEV_DIR_OF_SQUALL, range, 0.0, 1, 180));
			
			///> �������� ������� ����������� �������� ��� �������� � �����������.
			currValOfSpeed = spd.getNumberVal();	
			currValOfDir = dir.getNumberVal();
			
			Do.debug("Deviation of speed wind: current Value: " + currValOfSpeed + " - " + oldValOfSpeed + " = " + (currValOfDir-oldValOfSpeed));
			
			Do.debug("Deviation of DIR wind: current Value: " + currValOfDir + " - " + oldValOfDir + " = " + (currValOfDir-oldValOfDir));
			
			///> �������� ���������� �� �������� � �������� ������� Wind �� 30 ����� | 15.4 m/s | 55.6 km/h.
			Do.dpAssert(spd.isEqualInRangeToValue(oldValOfSpeed+DEV_SPEED_OF_SQUALL, range, 0.0, 1, 180),
													"Error! Deviation of Speed current Wind not equals "+DEV_SPEED_OF_SQUALL+" knots");
			
			///> �������� ���������� �� ����������� � �������� ������� Wind �� 45 ��������.
			Do.dpAssert(dir.isEqualInRangeToValue(oldValOfDir+DEV_DIR_OF_SQUALL, range, 0.0, 1, 180), 
													"Error! Deviation of direction current Wind not equals "+DEV_DIR_OF_SQUALL+" degrees");
			
			/**
			 * > ������� ����� checkSensor(SensorPanel sensor, Wind_Faults windFault, boolean state)
			 * > ��� �������� ��������� �������� ������� Wind ����� ����������� Squall/Shadow..
			 */
			checkSensor(sensor, windFault, true);
			break;
		default:
		}

		///> ������������ ������ Squall/Shadow
		sensFaultSet.deactivate(windFault.name());
		Do.pause(2);
		Do.debug("Deactivated Specific Format Error: " + windFault.name());
	}
	
	/**> ����� ��� ���������� ������� �������� ������� Wind ����� �����������(���������� ��� ���������).
	*@param sensor ������� ������(������).
	*@return ������, ��� key - ��� �������� ���������� / value - ��������������� ��� ��������.
	**/
	private Map<String, Double> saveCurrentResults(SensorPanel sensor){
		// ��������� ��� �������� �������� ������ ��� ������� ���������� ������� Wind
		Map<String, Double> values = new HashMap<>();
		
		int index = sensor.getNum() - 1;
		
		NumberDataInd ndi;
		
		if(sensor.isPriority()){
			ndi = tosSNS.snsesAll.get(Sensors.Wind)[index].dataIndicators.get(SPD);
			ndi.mustBeUserVisible();
			
			double speed = ndi.getNumberVal();
			values.put(SPD, speed);
				
			ndi = tosSNS.snsesAll.get(Sensors.Wind)[index].dataIndicators.get(R_DIR);
			ndi.mustBeUserVisible();
			
			double direction = ndi.getNumberVal();
			values.put(R_DIR, direction);
		} else {
			ndi = tosSNS.snsesAll.get(Sensors.Wind)[index].deviationIndicators.get(SPD);
			ndi.mustBeUserVisible();
			double speed = ndi.getNumberVal();
			values.put(SPD, speed);
				
			ndi = tosSNS.snsesAll.get(Sensors.Wind)[index].deviationIndicators.get(R_DIR);
			ndi.mustBeUserVisible();
			
			double direction = ndi.getNumberVal();
			values.put(R_DIR, direction);
		}
		return values;
	}
	
	/**> ����� ��� �������� ��������� ������� {������, ��������� �������, ������, ($.$|---)}
	*@param sensor ������� ������(������).
	*@param windFault ��� ������ (�-�: Empty Data, Invalid Msg...).
	*@param state ��� ������ (Present/Absent - �����������/���������� ������).
	**/
	private void checkSensor(SensorPanel sensor, Wind_Faults windFault, boolean state){
		/// �������, ��� � Visual:
		///- ��������� ������� ��� � ������ Shadow, Squall (Wind) - Select.
		sensor.modeMustBe(SensorREFModes.Select);
		Do.debug("Sensor " + sensor.button.getCaption() + " is Select.");
		
		///- ����� Wind discrepancy ��� ������ Squall, ��� Shadow ������ ���.
		switch(windFault){
		case Squall :
			Do.debug("Check alarm of " + sensor.button.getCaption());
			
			Alarm alarm = tosAlarm.mustBeLastPresentWithReqStatus("WIND_SENSOR_DISCREPANCY", 
					"(?iu)wind\\s+discrepancy", true, false);			
		    Do.debug(Sensors.Wind+" discrepancy: " + alarm.getTimeStart() + "| actual + not confirmed | => OK.");
			break;
		default :
			Do.debug("Not to check Alarm for Shadow.");
		}
		
		///- ������, ����������, ��� - ��������.
		sensor.allDataIndicatorsMustBe(state);
	}
	
	/// ���������� DP � �������� ���������
	@Override
	public void postconditions() throws Exception {
		///- � ������ �������� ������� Wind.
		tosModel.switchSensorsOrREFsGroupON(Sensors.Wind);
		
		///- � Visual �������� ������� ��� ������� Wind � Select.
		Do.debug("Set All Wind Sensors to Select after test.");
		tosModel.deactivateAllSensorsOrREFsGroupErrors(Sensors.Wind);
		tosSNS.setWholeGroupFast(Sensors.Wind, SensorREFModes.Select, false);
	}
}