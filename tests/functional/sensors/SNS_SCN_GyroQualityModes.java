package dpat.tests.functional.sensors;

import java.util.ArrayList;

import dpat.coreLib._model.sensorFaults.Gyro_Faults;
import dpat.coreLib._model.sensorFaults.SensorFaultsSet;
import dpat.coreLib.common.Do;
import dpat.coreLib.common.MethodSignature;
import dpat.coreLib.common.config.Sensors;
import dpat.coreLib.common.constants.ApplicationTypes;
import dpat.coreLib.common.constants.RoleIDs;
import dpat.coreLib.common.constants.SensorREFModes;
import dpat.coreLib.indicators.StringInd;
import dpat.coreLib.indicators.StringMultiStateInd;
import dpat.coreLib.indicators.NumberDataInd.NumberDataIndStates;
import dpat.coreLib.panels.PanelSwitcher;
import dpat.coreLib.panels.SensorPanel;
import dpat.tests.RunTest;

/**
 * ��������� ������ Quality ������� Gyro (��������, ����, �������).
 * @author v.kulikov
 */
public class SNS_SCN_GyroQualityModes extends RunTest {
	
	public dpat.tests.functional.sensors.TOset tosSNS; /* �������� ������ ��� Sensors. */
	
	/* ������������ ��� ��������� ����������� ��� �������� */
	private String HDG = "HDG.Title";

	@Override
	public void initTOsets() throws Exception {
		/*  ������������� ��������	 */
		fillAllowedAppList( RoleIDs.appVisual1, new ApplicationTypes[] {ApplicationTypes.Visual} );
		this.tosSNS = new dpat.tests.functional.sensors.TOset(RoleIDs.appVisual1);
	}
	
	@Override
	public void preconditions() throws Exception {
		/// ����� DP ������ ���� ������� ��� ���������� ���� �������� ������ ����� - RunTest.

		Do.debug("Check that Sensors Gyro are in this configuration");
		Do.dpAssert(Sensors.Gyro.totalELTnum() > 0, "Sensors Gyro are not in this configuration");
		
		/// � ������ �������� ������� Gyro.
		tosModel.btSensorFaults.pressVW();
		tosModel.switchSensorsOrREFsGroupON(Sensors.Gyro);
		tosModel.deactivateAllSensorsOrREFsGroupErrors(Sensors.Gyro);
				
		/** �� ����� (�����) ������ ���������� ������������� ����� �.�. ������� Sensors 
		  * �� ������ (������) ������� ���� Visual � DP ����� ���� ������� ����� ����. 
		  */
		ArrayList<PanelSwitcher> pSW = PanelSwitcher.createAllpsws(getAllowedAppList().keySet());
		for( PanelSwitcher each : pSW ) each.bringPanelRight("Alarm");
	}

	@Override
	public void scenario() throws Exception {
		/// ���������� ������� ������� ������ � ����� Select(�������������, ��� ������������� ����������).
		ArrayList<Integer> sensSel = tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false);
		Do.debug("Switch sensors to mode Select");
		
		tosSNS.snsNB.btAll.pressVW();
		
		/// �������� Quality � Raw/Used Data
		String[] state2switch = {"RawData", "UsedData"};
				
		for (String state : state2switch){
			/**
			 * > ������� ����� checkQuality(SensorPanel sensorPanel, String state)
			 * > ��� �������� ������� Quality ������� Gyro � ������� Raw/Used Data.
			 */
			for(int i=0; i<sensSel.size(); i++){ 
				checkQuality(tosSNS.snsesAll.get(Sensors.Gyro)[i], state); 
			}
		}
	}
	
	/**
	 * ����� ��� �������� ������� Quality � ������� Gyro.
	 * @param sensorPanel ������ (������ SensorPanel).
	 * @param state �����, � ������� ��������� ������.
	 */
	private void checkQuality(SensorPanel sensorPanel, String state){
		/// ���������� �������� ��� �������, � �������� ����������� ������� HETHS (������ Magn).
		if(sensorPanel.isMagnetic()) return;
				
		// �������� ����� ������ ��� ������ � �������� �������� � ������.
		SensorFaultsSet snsModelFaults = tosModel.sensorsFaults.getParamSet(Sensors.Gyro, sensorPanel.getNum());
		
		/// ���������� ������� HEHTS � ������� Gyro, � ��������� ������������.
		snsModelFaults.activateProtocol(sensorPanel.sensorGroup, Gyro_Faults.HETHS);
		
		/// ���������, ��� ���������� ����� Quality Autonomous � �������� ������� � ������. ��� ��� � ������ ������� ������ �� ������� � Select.
		setAutonomous(sensorPanel, snsModelFaults);
		
		///> �������� ��������� ����� ����������� ������
		tosSNS.switchRawUsedData(state);
		
		///> �������� ������ ��������� ������� � ����������� �� ������� ������ - Used/Raw
		StringMultiStateInd quality = (sensorPanel.isUsedDataMode()) ? sensorPanel.qualityGyro : sensorPanel.qualityGyroRaw;
		
		/// �� ������� �������� ������ A..V - (Autonomous .. V = Not Data Valid)
		for(String item : Gyro_Faults.Mode.getParams()){
			///> �������� ������ ����� �� ������ ��� Gyro. �-�: Autonomous
			snsModelFaults.selectCombo(Gyro_Faults.Mode.name(), item);
			Do.debug("Current item is " + item);
			
			///> ������ �������� ��� Noise �� ���� - �������(���>2) ; �� ���������(���<2), ���� �������.
			if(sensorPanel.modeIs(SensorREFModes.FaultSelect)){
				Do.check4actionTO(sensorPanel.noiseIndicators.get(HDG), new MethodSignature(false, "isDataPresent"));
			} else {
				if(sensorPanel.noiseIndicators.get(HDG).isEqualInRangeToValue(3.0, 1.0, 0.0, 1, 0)){
					Do.check4actionTO(sensorPanel.noiseIndicators.get(HDG), new MethodSignature("isInState", NumberDataIndStates.RED));
				} else {
					Do.check4actionTO(sensorPanel.noiseIndicators.get(HDG), new MethodSignature("isInState", NumberDataIndStates.DEFAULT));
				}
			}

			///> ��������� ���������.
			StringInd caption = quality; // �������� ������ Quality (��������� �������������), ������� ������������ � �������
			String captionInVisual = getVisualQuality(item);
			Do.check4actionTO(caption.getVal(), 
								"Caption " + caption.getVal() + " is not equals " + captionInVisual, 
									new MethodSignature("equalsIgnoreCase", captionInVisual));
			///> ��������� ����.
			if("Autonomous".equals(caption.getVal())){
				Do.check4actionTO(quality, new MethodSignature("isGreen"));
				sensorPanel.modeMustBe(SensorREFModes.Select);
			} else {
				Do.check4actionTO(quality, new MethodSignature("isRed"));
				sensorPanel.modeMustBe(SensorREFModes.FaultSelect);
			}
			
			/// ���������, ��� ���������� ����� Quality Autonomous � �������� ������� � ������
			setAutonomous(sensorPanel, snsModelFaults);
		}
		
		/// ���������� ������� HEHDT � ������� Gyro, � ��������� ������������.
		snsModelFaults.activateProtocol(sensorPanel.sensorGroup, Gyro_Faults.HEHDT);

		/// ��������������, ��� � ������ ��������� HEHDT - ������� �����������.
		Do.check4actionTO(quality, new MethodSignature(false, "isUserVisible"));
		
		// TODO: � ������ ��������� ����� � �������.
	}
	
	/**
	 * �������� ��������� ������ Quality � �������.
	 * @param mode - ������� ���� Quality
	 * @return ������� Quality � �������.
	 */
	private String getVisualQuality(String mode){
		String subString = ""; // �������� ������, ������� ������ ����
		if(mode.equals("A = Autonomous") || mode.equals("E = Estimated")) subString = mode.substring(4);
		else if(mode.equals("M = Manual input")) subString = "Manual";
		else if(mode.equals("S = Simulator mode")) subString = "Simulator";
		else if(mode.equals("V = Data not valid")) subString = "Not valid";
		return subString;
	}
	
	/**
	 * ����� ��� �������������� ������ Autonomous
	 * @param sensorPanel - ������� ������ �������
	 * @param snsModelFaults - ����� ���������� ��� ������ � �������� � ������.
	 */
	private void setAutonomous(SensorPanel sensorPanel, SensorFaultsSet snsModelFaults){
		snsModelFaults.selectCombo(Gyro_Faults.Mode.name(), Gyro_Faults.Mode.getParams().get(0)); // Autonomous.
		sensorPanel.setMode(SensorREFModes.Select);
		sensorPanel.modeMustBe(SensorREFModes.Select);
	}
	
	/// ���������� DP � �������� ���������
	@Override
	public void postconditions() throws Exception {
		/// �������� ������� Gyro � Model.
		tosModel.switchSensorsOrREFsGroupON(Sensors.Gyro);
		/// �������� ������� Gyro ������� � Select.
		tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false);
	}
}