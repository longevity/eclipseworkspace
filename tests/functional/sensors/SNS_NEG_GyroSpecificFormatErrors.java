package dpat.tests.functional.sensors;

import java.util.ArrayList;

import dpat.tests.RunTest;
import dpat.tests.functional.alarm.TOset.Alarm;
import dpat.coreLib.common.config.Sensors;
import dpat.coreLib.common.constants.ApplicationTypes;
import dpat.coreLib.common.constants.RoleIDs;
import dpat.coreLib.common.constants.SensorREFModes;
import dpat.coreLib.common.constants.Time;
import dpat.coreLib.common.Do;
import dpat.coreLib.common.MethodSignature;
import dpat.coreLib._model.sensorFaults.Gyro_Faults;
import dpat.coreLib._model.sensorFaults.SensorFaultsSet;
import dpat.coreLib.panels.PanelSwitcher;
import dpat.coreLib.panels.SensorPanel;

/**
 * ��������� � Visual �� ����������� ��� ������� ������� ��� ��������� � ����������� ������ ��� ������� ������:
 * ����������� ����������� ��������� ������, �������� �, ����� ������ ������ ������������ ��� ������� ������� �� �����������.
 * - SNS_NEG_GyroSpecificFormatErrors.java - ����������� ������ ������� Gyro(NoCS/FaultSelect/RepeatMsg).
 * - SNS_NEG_CommonFormatErrors.java - ����� ��������� ������ ���� ��������(Sensors).
 * - Errors - ��������� ����� ��� ������ ������ ��������.
 * @author v.kulikov
 */

public class SNS_NEG_GyroSpecificFormatErrors extends RunTest {
	public dpat.tests.functional.sensors.TOset tosSNS;
	public dpat.tests.functional.MFpanel.TOset tosMFP;
	public dpat.tests.functional.Map.TOset tosMAP;
	public dpat.tests.functional.alarm.TOset tosAlarm;
	
	private final String HDG = "HDG.Title";
	
	@Override
	public void initTOsets() throws Exception {
		/*  ������������� ��������	 */
		fillAllowedAppList( RoleIDs.appVisual1, new ApplicationTypes[] {ApplicationTypes.Visual} );
		this.tosSNS = new dpat.tests.functional.sensors.TOset(RoleIDs.appVisual1);
		this.tosMFP = new dpat.tests.functional.MFpanel.TOset(RoleIDs.appVisual1);
		this.tosMAP = new dpat.tests.functional.Map.TOset(RoleIDs.appVisual1);
		this.tosAlarm = new dpat.tests.functional.alarm.TOset(RoleIDs.appVisual1);
	}
	
	@Override
	public void preconditions() throws Exception {
		/// ����� DP ������ ���� ������� ��� ���������� ���� �������� ������ ����� - RunTest.
		
		Do.debug("Check that Sensors Gyro are in this configuration");
		Do.dpAssert(Sensors.Gyro.totalELTnum() > 0, "Sensors Gyro are not in this configuration");
		
		/// � ������ �������� ��� �������
		tosModel.switchAllSensorsAndREFsON();
		
		/// ���� Gyro ������ ���� ����������� �������.
		tosSNS.setNsensorsInGroup(Sensors.Gyro, SensorREFModes.Select, 1, false, true);
			
		/** �� ����� (�����) ������ ���������� ������������� ����� �.�. ������� Sensors; 
		  * �� ������ (������) ������� ���� Visual � DP ����������� Alarm
		  */
		ArrayList<PanelSwitcher> pSW = PanelSwitcher.createAllpsws(getAllowedAppList().keySet());
		for( PanelSwitcher each : pSW ) each.bringPanelRight("Alarm");
	}

	@Override
	public void scenario() throws Exception {	
		ArrayList<Integer> gyroSel = new ArrayList<Integer>(); // ������ �������� �������� Gyro, ������������ � Select.
		
		/// ���������� ������� ������� ������ � ����� Select(�������������, ��� ������������� ����������).
		gyroSel = tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false);
		Do.debug("Switch sensors Gyro to mode Select");
			
		/** ����������, ���� �� ��������� Gyro (���� �� 1).
		  * ���� ��������� Gyro ��� ������, ���-�� ����� ������������ ������� ������, ���� ��������� ��� �� ������.
		  * ��������� ����������� ����� ������������ ��� skipped � ��������� � ������ ��������.
		  */
		if(gyroSel.size() < 1) Do.skip("Less than 1 sensor " + Sensors.Gyro + " selected . Check mode of this sensor.");
		
		SensorPanel[] availableSensPanels = new SensorPanel[gyroSel.size()];
		
		// ���������� ������ ������� �������� � ��������� Select.
		for(int i=0; i<gyroSel.size(); i++) availableSensPanels[i] = tosSNS.snsesAll.get(Sensors.Gyro)[gyroSel.get(i)];
		
		/// ���� ��� ������������� ���� �������� �� ����.
		if(isCourseZero(availableSensPanels)){
			double course = tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(HDG).getNumberVal();
			tosMFP.btHdgAUTO.pressVW();
			tosMFP.dlgHdgAuto.setValue(Double.toString(Do.to_0_360(course - 1.0))); // �������� ���������� �� 1 ������.
			/// ���������� �������� �� ��������� �����, ���� �������� ����� � ������� 1 ������ �� �������� � �������.
			Do.check4actionTO(tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(HDG), 
										new MethodSignature(true, "isEqualInRangeToValue", course, 0.1, -1.0, 1, 360));
			Do.debug("New Set = " + tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(HDG).getNumberVal() + " degrees. (MFP->HDG->Auto).");
		}
		
		Do.dpAssert(!isCourseZero(availableSensPanels), "Course of some senosr Gyro is not zero!!");
		
		/// ������� ������ ��������� �������.
		for (SensorPanel sensor : availableSensPanels){
			Do.debug("Check "+sensor.button.getCaption());
			
			///> ������� �� ����� ������ ������ � ����������� ��� ���������.
			tosSNS.bringSensorPanel(sensor);
			
			///> ��������� ������ �� ������� ����������� ��������� ������.
			for(Gyro_Faults gyroFault : Gyro_Faults.values()){
				if(Gyro_Faults.OutOfLimit.equals(gyroFault)){
					/**
    				 * - ������� ����� checkGyroSpecificFormatError(SensorPanel sensor, Gyro_Faults gyroFault)
    				 * - ��� �������� ��������� �������� ������� Gyro ����� ����������� OutOfLimit.
    				 */
					checkGyroSpecificFormatError(sensor, gyroFault);
				} else 
				if(Gyro_Faults.NoCS.equals(gyroFault) || Gyro_Faults.RepeatMsg.equals(gyroFault)){
					/**
    				 * - ������� ����� checkGyroSpecificFormatError(SensorPanel sensor, Gyro_Faults gyroFault)
    				 * - ��� �������� ��������� �������� ������� Gyro ����� ����������� NoCS.
    				 */
					checkGyroSpecificFormatError(sensor, gyroFault);
				}
			}
		}
	}
	
	/**> ����� ��� �������� ����������� ������ �����������.
	*@param sens ������� ������(������).
	*@param gyroFault ��� ������ (�-�: RepeatMsg, OutOfLimit, NoCS).
	**/
	private void checkGyroSpecificFormatError(SensorPanel sensor, Gyro_Faults gyroFault) {
		int snsNum = sensor.getNum(); // ���������� ����� �������� �������.
		
		// ��������� ����� ����������, ��� ������ � �������� ������� � ������.
		SensorFaultsSet sensFaultSet = tosModel.sensorsFaults.getParamSet(sensor.sensorGroup, snsNum);
		
		/// ���������� ������ OutOfLimit/NoCS/RepeatMsg
		sensFaultSet.activate(gyroFault.name());
		
		Do.debug("Check Specific Format Error: " + gyroFault.name());
		
		switch(gyroFault){
		case OutOfLimit:
			if(!sensor.dataIndicators.get(HDG).isDataPresent()  ||
					(sensor.dataIndicators.get(HDG).isDataPresent() && (Do.roundResult(sensor.dataIndicators.get(HDG).getNumberVal(), 1) != 0.0))){
				/**
				 * > ������� ����� checkSensor(SensorPanel sensor, Gyro_Faults gyroFault, boolean state)
				 * > ��� �������� ��������� ����������� �������� ������� Gyro ����� ����������� OutOfLimit.
				 */
				checkSensor(sensor, gyroFault, false);
			}
			break;
		case NoCS:
			/**
			 * > ������� ����� checkSensor(SensorPanel sensor, Gyro_Faults gyroFault, boolean state)
			 * > ��� �������� ��������� ����������� �������� ������� Gyro ����� ����������� NoCS.
			 */
			checkSensor(sensor, gyroFault, false);
			// TODO : ��� ������� ������� ������ ��� ���� UseCheckSum, ��� ��� ��� �������� �� ������ �� ��������� �������.
			break;
		case RepeatMsg:
			// ���������� ��� �������� ���-�� ������������� ������� ������ RepeatMsg � ����� Sensors.ini
			int count = RoleIDs.appVisual1.getIniFilesParameters().getSimulIniGyroRepeatCount(sensor.getNum());
			
			if(count != 0){
				///> ���������, ��� ����� 30 ������ ������ ��������� � ��� �� ���������, � ������ � ��������� ���������.
				Do.debug("Start waiting for sensor data confidence period (" + Time.POP_UP_ON_SCREEN + " sec) of mode RepeatMsg...");
				
				Do.pause(Time.POP_UP_ON_SCREEN );
				Do.debug("Parameters data confidence period expired");
				
				/**
				 * > ������� ����� checkSensor(SensorPanel sensor, Gyro_Faults gyroFault, boolean state)
				 * > ��� �������� ��������� ����������� �������� ������� Gyro ����� ����������� RepeatMsg.
				 */
				checkSensor(sensor, gyroFault, true);
			}
			break;
		default:
		}

		/// ������������ ������ OutOfLimit/NoCS/RepeatMsg
		sensFaultSet.deactivate(gyroFault.name());
		
		sensor.setMode(SensorREFModes.Select);
	}
	
	/**> ����� ��� �������� ������� {������, ��������� �������, ������, ����������� ������}
	*@param sensor ������� ������(������).
	*@param gyroFault ��� ������ (�-�: OutOfLimit, NoCS, RepeatMsg...).
	*@param state ��� ������ (Present/Absent - �����/��������).
	**/
	private void checkSensor(SensorPanel sensor, Gyro_Faults gyroFault, boolean state){
		/// �������, ��� � Visual : ������ � ��������� Select/FaultSelect
		if(state) sensor.modeMustBe(SensorREFModes.Select);
		else sensor.modeMustBe(SensorREFModes.FaultSelect);
		Do.debug("Sensor " + sensor.button.getCaption() + " is " + ((state) ? SensorREFModes.Select : SensorREFModes.FaultSelect));
		
		if(!state){
			///> Alarm: Sensor{Num}: Invalid Data + ���������� + ���������������.
			Do.debug("Check alarm of " + sensor.button.getCaption().replaceAll("\\^", "-"));
			
			Alarm alarm = tosAlarm.mustBeLastPresentWithReqStatus(sensor.getNameForAlarmType()  + "_FAULT",
					"(?iu)" + sensor.button.getCaption().replaceAll("\\^", "-") + ":\\s+invalid\\s+data$", true, false);			
			Do.debug(sensor.sensorGroup+" Invalid Data " + alarm.getTimeStart() + "| actual + not confirmed | => OK");	
		}
		
		/// ���������� ������, ����������, ���� - ��������.
		sensor.allDataIndicatorsMustBe(state);
	}
	
	private boolean isCourseZero(SensorPanel[] availableSensPanels){
		for(SensorPanel sensorPanel : availableSensPanels){
			if(sensorPanel.dataIndicators.get(HDG).isEqualInRangeToValue(0.0, 0.5, 0.0, 1, 360)) return true;
		}
		return false;
	}
	
	/// ���������� DP � �������� ���������
	@Override
	public void postconditions() throws Exception {
		/// � ������ �������� ��� ������� Gyro.
		tosModel.switchSensorsOrREFsGroupON(Sensors.Gyro);
		
		/// � Visual �������� ������� ��� ������� Gyro � Select.
		Do.debug("Set All Sensors Gyro to Select after test");

		tosModel.deactivateAllSensorsOrREFsGroupErrors(Sensors.Gyro);
		tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false);
	}
}