package dpat.tests.functional.sensors;

import java.util.ArrayList;

import dpat.coreLib.common.config.Sensors;
import dpat.coreLib.common.constants.ApplicationTypes;
import dpat.coreLib.common.constants.RoleIDs;
import dpat.coreLib.common.constants.SensorREFModes;
import dpat.coreLib.common.Do;
import dpat.coreLib.panels.SensorPanel;
import dpat.tests.RunTest;

/**
 * ������� ��������� ������� � Select ��� ���������� ������� � ������.
 * ��������� ����� ������ ����� ������������ ��� Select, � ����� �������� � Fault Select.
 * @author v.kulikov
 *
 */
public class SNS_NEG_sensor_ModelOffSel extends RunTest {
	public dpat.tests.functional.sensors.TOset tosSNS;
	ArrayList<Sensors> availableSensors;

	@Override
	public void initTOsets() throws Exception {
		/*  ������������� ��������	 */
		fillAllowedAppList( RoleIDs.appVisual1, new ApplicationTypes[] {ApplicationTypes.Visual} );
		this.tosSNS = new dpat.tests.functional.sensors.TOset(RoleIDs.appVisual1);
	}
	
	@Override
	public void preconditions() throws Exception {
		/// ����� DP ������ ���� ������� ��� ���������� ���� �������� ������ ����� - RunTest.
		
		/** 
		 * ���������� ������ ��������, ������������� � ������� ������������. 
		 * ��� ������� ����� ����� �������� ��� ��� �� ����� ��������� � ������������� ���������� ������������ ���� SkipException, 
		 * ���������� ���������� ���������, � ���� � ������ ����� ������� ��� Skipped.
		 * */
		availableSensors = Sensors.checkSensorsInCurrentConfiguration();
		Do.debug("Available sensors in current configuration: " +  availableSensors);
	}

	@Override
	public void scenario() throws Exception {
		/// � Visual ��������� ��� ������� � NotUse, ����� �������� ��������� � Fault Select.		
		Do.debug("Set All Sensors to NotUse");
		for (Sensors sns : availableSensors) {
			if( !sns.isModeSupported(SensorREFModes.NotUse) ) continue;
			tosSNS.setWholeGroupFast(sns, SensorREFModes.NotUse, true);
		}	
		
		/// � ������ ��������� ��� �������.		
		tosModel.switchAllSensorsAndREFsOFF();
		
		/// ��� ���� �������� �� ������ ������������ (������������� �� �������� � �����), ��������� �� �� ������:				
		for( Sensors sns: availableSensors ){// ���� ������ �� ���������� �������� � ������ ������������
			if( !sns.isModeSupported(SensorREFModes.Select) ) continue; // ���������� �������, ��� ������� ��� �������� ����������� (TS, ...)
			for( SensorPanel sensorI: tosSNS.snsesAll.get(sns) ){
				Do.debug("Check "+sensorI.button.getCaption());
				///> ������������� ��������� ������� �������
				tosSNS.snsNB.bringIt(sensorI.button);
				sensorI.button.mustBeUserVisible();
		
				sensorI.modeMustBe(SensorREFModes.NotUse);///> ���������, ��� ������ � ������ NotUse
				sensorI.setMode(SensorREFModes.Select);///> ��������� ������ � ����� Select
							
				///> ���������:
				if( !sns.name().equals("LOG") ){/// - ��� ������� ������ ����, ����� LOG:					
					sensorI.button.mustBeLightGreen();/// - > ������ ������� - �������					
					sensorI.dataFilter.mustBeRed();   /// - > ������ ������� - �������
					/// - > ������� ���
					// TODO �������� ����� ���������� ������� 14944
				}
				else sensorI.modeMustBe(SensorREFModes.FaultSelect);/// - ��� ������� LOG �������� ���, ����� ��������� � FaultSelect
								
				/// - ����� ��������� ����� ������ ������ ���� ������ ���� � FaultSelect.
				sensorI.modeMustBe(SensorREFModes.FaultSelect);
			} 			
		}
	}

	/**
	 * ���������� DP � �������� ���������
	 */
	@Override
	public void postconditions() throws Exception {
		///> � ������ �������� ��� �������.
		tosModel.switchAllSensorsAndREFsON();
		///> � Visual �������� ������� ��� ������� � Select.
		Do.debug("Set All Sensors to Select after test");
		//for (Sensors sns : tosSNS.snsesAll.keySet()) {
		for (Sensors sns : availableSensors) {//����� ���������� �� ������� � Gyro ���������� �������
			if( !sns.isModeSupported(SensorREFModes.Select) ) continue;
			tosSNS.setWholeGroupFast(sns, SensorREFModes.Select, false);
		}
	}
}

/*for( Sensors sns: tosSNS.snsesAll.keySet()){ // ���� ������ �� ���������� �������� � ������ ������������
	if( sns == Sensors.TS ) continue; // ���������� TS, ��� ���� ��� �������� �����������
	if( sns.getLayout().length>0 ){ // �.�. �������� ����� ���� >0
		// ����������� ��������� ������ ������			
		tosSNS.snsNB.bringIt(tosSNS.snsesAll.get(sns)[tosSNS.snsesAll.get(sns).length-1].button); // ��������� ��-�
		for( SensorPanel expl: tosSNS.snsesAll.get(sns) ){
			expl.setMode(SensorREFModes.NotUse);// ���������� ������������ � NotUse
		}				
	}
}*/

/*for( Sensors snsI: tosModel.sensorsFaults.sns.keySet()){// ���������� ������ ��������
for( int i=0;i<tosModel.sensorsFaults.sns.get(snsI).length;i++ ){
	if(tosModel.sensorsFaults.sns.get(snsI)[i].button.isActive())
		tosModel.sensorsFaults.sns.get(snsI)[i].button.pressAny(); // ��������� (������ ��� ����������� �� ���������)
}
}*/	
