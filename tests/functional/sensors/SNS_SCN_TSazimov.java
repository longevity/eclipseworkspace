package dpat.tests.functional.sensors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import dpat.coreLib.common.Do;
import dpat.coreLib.common.MethodSignature;
import dpat.coreLib.common.config.Sensors;
import dpat.coreLib.common.config.Parameters.Params;
import dpat.coreLib.common.constants.ApplicationTypes;
import dpat.coreLib.common.constants.RoleIDs;
import dpat.coreLib.common.constants.SensorREFModes;
import dpat.coreLib.indicators.NumberDataInd;
import dpat.coreLib.indicators.NumberWithIndependentUnitsDataInd;
import dpat.coreLib.panels.PanelSwitcher;
import dpat.coreLib.panels.ParamPanel;
import dpat.coreLib.panels.SensorPanel;
import dpat.coreLib.parameters.ParameterWithChangeEditor;
import dpat.coreLib.parameters.ParameterWithDialogEditor;
import dpat.tests.RunTest;
import dpat.NvWidget.NvWidget;

/**
 * �������������� ���� �� �������� ���������������� ������� TS.
 * �����������(��� Manual value, Azimuth, Elevation):
 * - �������� �� ���������(0.0);
 * - ����������� ������������� � ������������ ��������;
 * - ����������� t, kN.
 * @author v.kulikov
 */
public class SNS_SCN_TSazimov extends RunTest {
	/** �������� ������� ��� ��������, �������, ���������� � �������. **/
	public dpat.tests.functional.sensors.TOset tosSNS;
	public dpat.tests.functional.alarm.TOset tosAlarm;
	public dpat.tests.functional.parameters.TOset tosParam;
	
	@Override
	public void initTOsets() throws Exception {
		/*  ������������� ��������	 */
		fillAllowedAppList( RoleIDs.appVisual1, new ApplicationTypes[] {ApplicationTypes.Visual} );
		tosSNS = new dpat.tests.functional.sensors.TOset(RoleIDs.appVisual1);
		tosAlarm = new dpat.tests.functional.alarm.TOset(RoleIDs.appVisual1);
		tosParam = new dpat.tests.functional.parameters.TOset(RoleIDs.appVisual1);
	}

	@Override
	public void preconditions() throws Exception {
		/** �� ����� (�����) ������ ���������� ������������� ����� �.�. ������� Sensors 
		  * �� ������ (������) ������� ���� Visual � DP ����� ���� ������� ���� Param. 
		  */
		ArrayList<PanelSwitcher>  pSW = PanelSwitcher.createAllpsws(getAllowedAppList().keySet());
		for( PanelSwitcher each : pSW ) each.bringPanelRight("Param");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void scenario() throws Exception {
		for(String unit : new String[]{ "t", "kN" }){
			/// ������� �� ����� Param -> Units.
			ParamPanel paramUnits = tosParam.params.get(Params.UNITS);
			tosParam.bringParamPanel(paramUnits);

			/// ���������� ��� ������� TS ����������� (���� Units) - t, kn.
			ParameterWithChangeEditor param = paramUnits.get�hangeEditParameter("Force");
			param.setCurVal(unit);
			paramUnits.pressApply();
			
			tosSNS.snsNB.btTS.pressVW(); // ������� ���� ��������� ���������� ������� TS.
			
			/// ���������� ������� TS � ����� Select(�������������, ��� ������������� ����������).
			tosSNS.setWholeGroupFast(Sensors.TS, SensorREFModes.UseSensor, false);
			Do.debug("Switch " + Sensors.TS + " sensors to mode UseSensor.");
				
			/** ����������, ���� �� ��������� TS (���� �� 1).
			  * ���� ��������� TS ���, ���-�� ����� ������������ ������� ������, ���� ��������� ��� �� ������.
			  * ��������� ����������� ����� ������������ ��� skipped � ��������� � ������ ��������.
			  */
			ArrayList<SensorPanel> listSensPanels = tosSNS.getTheSameSensorsInGroup(Sensors.TS, 
					new MethodSignature("modeIs", SensorREFModes.UseSensor));
			
			// ���������� ������ ������� �������� � ��������� Select.
			SensorPanel[] availableSensPanels = listSensPanels.toArray(new SensorPanel[listSensPanels.size()]);
			
			if(availableSensPanels.length < 1) Do.skip("Less than 1 sensors " + Sensors.TS + " selected . Check mode of this sensor.");
			
			/// ������� ������ ������ �� ������ � ��������� ���������������� �������.
			for(SensorPanel sensorPanel : availableSensPanels)
			{
				///> ���������, ��� �� ���������(������ ������) ��� �������� ����.
				checkInitConditions(sensorPanel);
				
				///> ���������� ����� Manual.
				sensorPanel.setMode(SensorREFModes.UseManual);
				sensorPanel.modeMustBe(SensorREFModes.UseManual);
				
				///> ������ ������ �������� Manual Value, Manual Azimuth, Manual Elevation.
				for(String title : new String[]{"value", "Azimuth", "Elevation"})
				{
					///- �������� ������ ������ ��� ����������� ��������� ����������� �����.
					ParameterWithDialogEditor dlg = tosSNS.getPnTSParam().getDialogEditParameter(title);
					Do.debug("Getted ParameterWithDialogEditor with title: " + dlg.getTitle());

					ArrayList<Double> values =
							("value".equals(title)) ? new ArrayList(Arrays.asList(0.0, 100.0, 100.1, 0.0)) 
						: 
							("Azimuth".equals(title)) ? new ArrayList(Arrays.asList(0.0, 359.9, 360.0, 0.0)) : new ArrayList(Arrays.asList(0.0, 90.0, 90.1, -90.0, -90.1, 0.0));	
					
					// ���� ����������� kn, �� ������������ ������ �������� �������� ��� ��������� Manual value.
					boolean isUnitkN = "value".equals(title) && "kN".equals(unit);
					if(isUnitkN) values = new ArrayList(Arrays.asList(0.0, 980.0, 981.0, 0.0));
					Do.debug("Getted list nums: " + values);
							
					for(Double value : values)
					{
						///- ��������� �������� � ������� �������. ���� �-��� �� ������ � �������� �� ��������� ������ Cancel.
						try {
							String val = (isUnitkN) ? Integer.toString(value.intValue()) : Double.toString(value);
							dlg.setCurVal(val);
						} catch(NvWidget.NvExceptionFatal nf) {
							Do.debug(nf.getMessage());
							if(!title.equals("Elevation")) dlg.dlgParamSet.btSign.mustBeDisabled(); 
							else dlg.dlgParamSet.btSign.mustBeEnabled();
							
							dlg.dlgParamSet.bt1OK.mustBeDisabled();
							dlg.dlgParamSet.bt2Cancel.pressVW();
							Do.debug("Can not set value : " + value + ". Not In Range.");
						}

						tosSNS.getPnTSParam().pressApply();
						
						///- ���������, ��� ������������ �������� ������������ �� ������ �����������.
						String resTitle = ("value".equals(title)) ? "Force.Title Details" : title + ".Title Details";
						
						NumberWithIndependentUnitsDataInd indResult = 
								(NumberWithIndependentUnitsDataInd) tosSNS.pnResDet.get(Sensors.TS).processedData.get(resTitle);

						Do.check4actionTO(indResult, 
							"Value for indicator " + indResult.getCaption() + " is not equal " + value,
								new MethodSignature("isEqualInRangeToValue", value, 0.2, ((isUnitkN && 981.0==value) ? -1.0 : 0.0), 1, 0));
						Do.debug("Setted value: " + value);
						
						///- ��������� ������������ �����������.
						if("value".equals(title)){
							Do.dpAssert(indResult.getUnits().equalsIgnoreCase(unit), "Unit : " + indResult.getUnits() + " Not Equals " + unit);
						} else {
							Do.dpAssert(indResult.getUnits().equalsIgnoreCase("�"), "Unit : " + indResult.getUnits() + " Not Equals " + unit);
						}
						Do.debug("Unit is: " + indResult.getUnits());
						
						///- ���������, ��� �������� ��������� ����� ������������� (� ������� ������).
						ArrayList<Double> incorrectNums = new ArrayList<>(Arrays.asList(-90.1, 90.1, 100.1, 360.0, 981.0));
						if(incorrectNums.contains(value)) continue;
						
						String strNum = Double.toString(value + ((isUnitkN && 981.0==value) ? -1.0 : 0.0));
						Do.dpAssert(dlg.value.isEqualToString(strNum) || dlg.value.isEqualToString(strNum.replace(".0", "")), "ParamValue is not Equal " + strNum + " It's " + dlg.value.getCurVal());
						Do.debug("Checked number(string format): " + strNum);
					}
					
					///> ���������� ����� UseSensor.
					Do.debug("Set mode UseSensor");
					sensorPanel.setMode(SensorREFModes.UseSensor);
					sensorPanel.modeMustBe(SensorREFModes.UseSensor);
				}
			}
		}
	}
	
	/**
	 * ����� ��� �������� ��������� �������� �������. (��������� ����� ������� ������� �������.)
	 * @param sensorPanel - ������ ������� (������ SensorPanel)
	 */
	private void checkInitConditions(SensorPanel sensorPanel)
	{
		///> ��� �������� �� ������ - ��������.
		sensorPanel.allDataIndicatorsMustBe(true);
		///> ��������� ����������� �������� �� ������.
		HashMap<String, NumberDataInd> nums = sensorPanel.dataIndicators;
		for(String title : sensorPanel.dataIndicators.keySet()){
			Do.dpAssert(nums.get(title).isEqualInRangeToValue(0.0, 0.01, 0.0, 2, 0), 
					"Value for indicator " + nums.get(title).getCaption() + " is not decreased (not equal zero).");
		}	
	}

	@Override
	public void postconditions() throws Exception {
		///> � ������ �������� ������� TS.
		tosModel.switchSensorsOrREFsGroupON(Sensors.TS);
		///> � Visual �������� ������� ��� ������� TS � UseSensor.
		tosSNS.setWholeGroupFast(Sensors.TS, SensorREFModes.UseSensor, false);
		Do.debug("All " + Sensors.TS + " Sensors were setted to Select after test.");
	}
}