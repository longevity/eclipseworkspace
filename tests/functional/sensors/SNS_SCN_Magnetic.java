package dpat.tests.functional.sensors;

import dpat.coreLib._model.sensorFaults.Gyro_Faults;
import dpat.coreLib._model.sensorFaults.SensorFaultsSet;
import dpat.coreLib.common.Do;
import dpat.coreLib.common.config.Sensors;
import dpat.coreLib.common.constants.SensorREFModes;
import dpat.coreLib.panels.PanelSwitcher;
import dpat.coreLib.panels.SensorPanel;
import dpat.tests.RunTest;
import dpat.coreLib.common.config.REFs;
import dpat.coreLib.common.constants.ApplicationTypes;
import dpat.coreLib.common.constants.RoleIDs;
import dpat.coreLib.common.MethodSignature;
import dpat.coreLib.indicators.CompareDeviations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * ��������� ��������� ������� Magnetic ��� ������� ������ � �������� ������.
 * - SNS_SCN_GyroQualityModes.java - �������� ��� Quality.
 * - SNS_NEG_GyroSpecificFormatErrors.java - ����������� ������ ������� Gyro.
 * - SNS_SCN_Gyro_Errors.java - ������� ������ ������� Gyro(Noise, Jump, Drift, Single) ��� ����� Magnetic.
 * @author v.kulikov
 */
public class SNS_SCN_Magnetic extends RunTest {
	
	/* �������� ������� ��� ��������, REF-��������, ������� � �������. */
	public dpat.tests.functional.sensors.TOset tosSNS;
	public dpat.tests.functional.REF.TOset tosREF;
	public dpat.tests.functional.alarm.TOset tosAlarm;
	
	/* ������������ ��� ��������� ����������� ��� �������� */
	private final String HDG = "HDG.Title";
	private final String BIAS = "Bias.Title";
	private final String ROT = "ROT.Title";
	
	@Override
	public void initTOsets() throws Exception {
		/*  ������������� ��������	 */
		fillAllowedAppList( RoleIDs.appVisual1, new ApplicationTypes[] {ApplicationTypes.Visual} );
		tosSNS = new dpat.tests.functional.sensors.TOset(RoleIDs.appVisual1);
		tosREF = new dpat.tests.functional.REF.TOset(RoleIDs.appVisual1);
		tosAlarm = new dpat.tests.functional.alarm.TOset(RoleIDs.appVisual1);
	}
	
	@Override
	public void preconditions() throws Exception {
		/// ����� DP ������ ���� ������� ��� ���������� ���� �������� ������ ����� - RunTest.
		
		/// ��������� ������� �������� Gyro � ������������.
		Do.debug("Check that Sensors " + Sensors.Gyro + " are in this configuration.");
		Do.dpAssert(Sensors.Gyro.totalELTnum() > 0, "Sensors Gyro are not in this configuration.");
		
		/// � ������ �������� ������� Gyro.
		tosModel.btFaults.pressAny();
		tosModel.switchSensorsOrREFsGroupON(Sensors.Gyro);
				
		/** �� ����� (�����) ������ ���������� ������������� ����� �.�. ������� Sensors 
		  * �� ������ (������) ������� ���� Visual � DP ����������� Alarm
		  */
		ArrayList<PanelSwitcher> pSW = PanelSwitcher.createAllpsws(getAllowedAppList().keySet());
		for( PanelSwitcher each : pSW ) each.bringPanelRight("Alarm");
	}

	@Override
	public void scenario() throws Exception {
		/// ���������� ������� Gyro � ����� Select(�������������, ��� ������������� ����������).
		SensorPanel[] availableSensPanels = getAvailableSensors(Sensors.Gyro, 2); // ������ ������� ��������, ������������ � Select.
		Do.debug("Switch sensors " + Sensors.Gyro + " to mode Select");
		
		/// ��������� ������� ������� Magn, ��� ������� ���������������� ������.
		SensorPanel magnetic = tosSNS.hasThereMagnetic(availableSensPanels);
		
		/// ���� Magnetic-������ �����������, �� �������� ���� ��� skip.
		if(null == magnetic) Do.skip("Magnetic sensor is missing for current configuration.");
		
		/// ���������� �������� ������������� ������� ��� ������������.
		String priorityInGyroGroup = tosSNS.snsPRind.get(Sensors.Gyro).getCaption();
		
		/// ���� Magnetic - ������������, �� ���������� ����� �������������� ����������.
		boolean state = priorityInGyroGroup.equalsIgnoreCase(magnetic.button.getCaption()) ? false : true;

		/// ���������� ������������ ������(�� ��������) ��� ��������� � ������ ���������� ��� ������ � �������� � ������.
		SensorPanel notMagnetic = Do.sureFirstObject(availableSensPanels, // ���������� ������������ ������ ��� ���������.
						new MethodSignature(true, "modeFast", SensorREFModes.Select),  
						new MethodSignature(state, "isPriority"),
						new MethodSignature(false, "isMagnetic"));
		
		/// ����������� �������������� � Magnetic ������� �� ������.
		tosSNS.returnPriority(notMagnetic);
		
		/// ��������������, ��� ��Magnetic - ������������.
		if(state) notMagnetic.mustBePriority();

		Do.debug("Current priority sensor is " + notMagnetic.button.getCaption());

		// ���������� ����� ���������� Sensor Faults ��� ������� magnetic
		SensorFaultsSet snsMagneticModelFaults = tosModel.sensorsFaults.getParamSet(magnetic.sensorGroup, magnetic.getNum());
		// ���������� ����� ���������� Sensor Faults ��� ������������� �������.
		SensorFaultsSet snsPriorityModelFaults = tosModel.sensorsFaults.getParamSet(Sensors.Gyro, notMagnetic.getNum());
		
		tosSNS.snsNB.btAll.pressVW(); // ����������� ������� �� ������ All, ��� ������������ ������ Details ��� ��������.
		
		/// ��������� ��������� ��������� Gyro - ��������.
		checkProtocols(availableSensPanels);
		
		tosSNS.snsNB.btGyro.pressVW(); // ������� ������ Details �������� Gyro � �������.
		
		/**
		 * ������� ����� checkValuesMagnetic(SensorPanel magnetic, SensorPanel snsPriority, 
		 * SensorFaultsSet snsMagneticModelFaults, SensorFaultsSet snsPriorityModelFaults)
		 * ��� �������� ���������� ����������� ������� magnetic � ������� ��� ������������ ��������� HEHDT/HCHDG.
		 */
		checkMagneticValues(magnetic, notMagnetic, snsMagneticModelFaults, snsPriorityModelFaults);
		
		boolean isPriorityMagnetic = magnetic.isPriority();
		
		/// ���������� ������������ � �������������� �������.
		SensorPanel priority = (isPriorityMagnetic) ? magnetic : notMagnetic;
		SensorPanel notPriority = (!isPriorityMagnetic) ? magnetic : notMagnetic;
		
		/// ��������� ���������� dev � ������������� ������� � ������� � ������� Magn(���� �� ��������������).
		Do.dpAssert(!priority.deviationIndicators.get(HDG).isUserVisible(), 
					"Dev of priority sensor " + priority.button.getCaption() + " is Visible");
		Do.dpAssert(notPriority.deviationIndicators.get(HDG).isUserVisible(), 
					"Dev of not priority sensor " + notPriority.button.getCaption() + " is Visible");
		
		if(isPriorityMagnetic){
			///- �������� ���� �������������� ������ ����� ��������� ���� magnetic-������� + bias.
			CompareDeviations cmp = 
					new CompareDeviations(tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(HDG), 
										  tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(BIAS), 360, 1);
			Do.debug("pnRes - BIAS = " + cmp.calculateDeviation());
			Do.check4actionTO(cmp, 
					"Data of Magnetic hdg is not equals Data Result - Bias.", 
						new MethodSignature("isEqualInRangeToValue", magnetic.dataIndicators.get(HDG).getNumberVal(), 0.5, 0.0));
		} else {
			CompareDeviations compare = 
					new CompareDeviations(magnetic.dataIndicators.get(HDG), magnetic.deviationIndicators.get(HDG), 360, 1);
			
			double hdg = compare.calculateDeviation();
			
			Do.debug("magnHDG - DEV = " + hdg);
			
			///- ��������� ���� ����������� � ������� magnetic.
			Do.dpAssert(tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(HDG)
				.isEqualInRangeToValue(hdg, 0.5, tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(BIAS).getNumberVal(), 1, 360), 
					"Data of panel Result is not equal Data Magnetic sensor + Magnetic Deviation.");
		}
	}

	/**
	 * ��������� ���������, ����������� ���������� ������������.
	 * @param availableSensPanels - ������ ��������� ������� Gyro.
	 */
	private void checkProtocols(SensorPanel[] availableSensPanels){
		tosModel.btSensorFaults.pressAny();
		
		///> ������� ������ ������ ����������� � ��������� :
		for(SensorPanel sp : availableSensPanels){
			SensorFaultsSet snsGyroModelFaults = tosModel.sensorsFaults.getParamSet(sp.sensorGroup, sp.getNum());
			List<Gyro_Faults> proto = (sp.isMagnetic()) ?
					Arrays.asList(Gyro_Faults.HCHDM, Gyro_Faults.HEHDT, Gyro_Faults.HCHDG) 
					: 
					Arrays.asList(Gyro_Faults.HETHS, Gyro_Faults.HEHDT, Gyro_Faults.HEROT);
					
			///- ����������� ���������, ��������� � �����������. ��������� ������ Rotation ��� Gyro. ���������� ����������� ������� � Quality.
			for(Gyro_Faults gf : proto){
				tosSNS.bringSensorPanel(sp);
				String protocol = gf.name();
				if(snsGyroModelFaults.isActiveFault(protocol)) continue;
				snsGyroModelFaults.activate(protocol);

				if(Gyro_Faults.HEROT.equals(gf)){
					
					if(!sp.isUsedDataMode()) tosSNS.switchRawUsedData("UsedData");
					
					Do.check4actionTO(sp.dataIndicators.get(ROT), 
							"Rotation indicator is not Visible For sensor " + sp.button.getCaption(), 
								new MethodSignature("isUserVisible"));
					Do.check4actionTO(sp.dataIndicators.get(ROT), 
							"Rotation indicator is not Data Present For sensor " + sp.button.getCaption() + 
								" Value is " + sp.dataIndicators.get(ROT).getNumberVal(), 
									new MethodSignature("isDataPresent"));
				}
				
				snsGyroModelFaults.deactivate(protocol);
			}			
		}
	}
	
	/**
	 * ��������� ��������� magnetic - �������.
	 * @param magnetic - ������ ������� Magn.
	 * @param snsPriority - ������ ������������� �������.
	 * @param snsMagneticModelFaults - ����� ���������� magnetic-�������.
	 * @param snsPriorityModelFaults - ����� ���������� ������������� �������.
	 */
	private void checkMagneticValues(SensorPanel magnetic, 
								SensorPanel snsPriority,
								SensorFaultsSet snsMagneticModelFaults,
								SensorFaultsSet snsPriorityModelFaults)
	{
		tosModel.btSensorFaults.pressAny();
		
		if(snsMagneticModelFaults.isActiveFault(Gyro_Faults.HCHDM.name()))
			snsMagneticModelFaults.deactivate(Gyro_Faults.HCHDM.name());
		
		List<Gyro_Faults> proto = Arrays.asList(Gyro_Faults.HCHDG, Gyro_Faults.HEHDT);
		
		for(Gyro_Faults gf : proto){
			///- ��������� ������ ��� ���������� HEHDT/HCHDG
			snsMagneticModelFaults.activateProtocol(magnetic.sensorGroup, gf);
			///- ��������� magnetic � ������ ����������� ������ ��������� � ������ ����������.
			if(magnetic.isPriority()){
				Do.check4actionTO(tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(HDG), 
						"Data of Magnetic sensor is not equals Data of Panel Result.", 
							new MethodSignature("isEqualInRangeToIndValue", magnetic.dataIndicators.get(HDG), 0.5, magnetic.dataIndicators.get(BIAS), 1, 360));
			} else {
				CompareDeviations cmp = new CompareDeviations(snsPriority.dataIndicators.get(HDG), magnetic.dataIndicators.get(HDG) , 360);
				Do.check4actionTO(cmp, 
						"Data of Magnetic sensor is not equals priority sensor with Deviation.", 
							new MethodSignature("isEqualInRangeToValue", magnetic.deviationIndicators.get(HDG).getNumberVal(), 0.5, 0.0));
			}
			
			snsMagneticModelFaults.deactivate(gf.name());
		}
		snsMagneticModelFaults.activateProtocol(magnetic.sensorGroup, Gyro_Faults.HCHDM);
		
		tosSNS.returnPriority(snsPriority);
	}
	
	/**
	 * �������� ������ ������� ��������, �������������� �������� ������� �������� � ������.
	 * @param group - ����������� ������ ��������.
	 * @param num - ���������� ��������, ����������� ��� �����.
	 * @return - ������ �������� ��������, ������������ � Select.
	 */
	SensorPanel[] getAvailableSensors(Sensors group, int num){
		///> �������� ������ ������������ � ������������.
		ArrayList<SensorPanel> sensGyro = tosSNS.getAllSensorsInGroup(group);
		
		for(SensorPanel sensorGyro : sensGyro){
			// ���������� ����� ���������� Sensor Faults ��� ������� magnetic
			SensorFaultsSet snsModelFaults = tosModel.sensorsFaults.getParamSet(sensorGyro.sensorGroup, sensorGyro.getNum());
			
			if(sensorGyro.isMagnetic()){
				///- ���������, ��� ������� HCHDM ������� magnetic - ������������.
				snsModelFaults.activateProtocol(sensorGyro.sensorGroup, Gyro_Faults.HCHDM);
			} else {
				///- ���������, ��� ������� HEHDT �������� ������� - ������������.
				snsModelFaults.activateProtocol(sensorGyro.sensorGroup, Gyro_Faults.HEHDT);
			}
		}
		
		///> ����� �������� ��������� ������� � �������� Gyro �������� �� � ������ � �������.
		tosModel.switchSensorsOrREFsGroupON(Sensors.Gyro);
		
		///> �������� ������� � Select ��� ������������� ����������..
		ArrayList<Integer> gyroSel = tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false);
		Do.debug("Switch sensors " + Sensors.Gyro + " to mode Select"); // ������ �������� ��������, ������������ � Select.
				
		/** > ����������, ���� �� ��������� Gyro.
		  * > ���� ��������� Gyro ������ ��� ����������, ���-�� ����� ������������ ������� ������, ���� ��������� ��� �� ������.
		  * > ��������� ����������� ����� ������������ ��� skipped � ��������� � ������ ��������.
		  */	
		if(gyroSel.size() < num) Do.skip("Less than " + num + " sensor " + Sensors.Gyro + " selected . Check mode of this sensor.");
		
		SensorPanel[] availableSensPanels = new SensorPanel[gyroSel.size()]; // ��������� ������ ��������
		for(int i=0; i<gyroSel.size(); i++) availableSensPanels[i] = tosSNS.snsesAll.get(Sensors.Gyro)[i];
		
		return availableSensPanels;
	}
	
	/// ���������� DP � �������� ���������
	@Override
	public void postconditions() throws Exception {
//		tosModel.deactivateAllSensorsOrREFsGroupErrors(Sensors.Gyro);
//		tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false);
//		tosSNS.setWholeGroupFast(Sensors.Wind, SensorREFModes.Select, false);
//		tosREF.setWholeGroupFast(REFs.GPS, SensorREFModes.Select, false, false);
	}
}