package dpat.tests.functional.sensors;

import java.util.ArrayList;

import dpat.coreLib._model.sensorFaults.Gyro_Faults;
import dpat.coreLib._model.sensorFaults.SensorFaultsSet;
import dpat.coreLib.common.Do;
import dpat.coreLib.common.MethodSignature;
import dpat.coreLib.common.config.REFs;
import dpat.coreLib.common.config.Sensors;
import dpat.coreLib.common.constants.ApplicationTypes;
import dpat.coreLib.common.constants.RoleIDs;
import dpat.coreLib.common.constants.SensorREFModes;
import dpat.coreLib.indicators.CompareDeviations;
import dpat.coreLib.indicators.NumberDataInd;
import dpat.coreLib.indicators.NumberDataInd.NumberDataIndStates;
import dpat.coreLib.panels.PanelSwitcher;
import dpat.coreLib.panels.ParamPanel;
import dpat.coreLib.panels.SensResults;
import dpat.coreLib.panels.SensorPanel;
import dpat.coreLib.parameters.ParameterWithBreakingArrowsEditor;
import dpat.tests.RunTest;

/**
 * ��������� ��������� �������� Gyro(priority/not priority) ��� ����������� ��������� ����������(������) � ������.
 * - SNS_SCN_GyroQualityModes.java - �������� ��� Quality.
 * - SNS_NEG_GyroSpecificFormatErrors.java - ����������� ������ ������� Gyro.
 * - SNS_SCN_Gyro_Errors.java - ������� ������ ������� Gyro(Noise, Jump, Drift, Single)
 * - SNS_NEG_CommonFormatErrors - ������� ��������� ������ �������.
 * @author v.kulikov
 */
public class SNS_SCN_Gyro_Errors extends RunTest {
	
	/* �������� ������� ��� ��������, REF-��������, ������� � �������. */
	public dpat.tests.functional.sensors.TOset tosSNS;
	public dpat.tests.functional.REF.TOset tosREF;
	public dpat.tests.functional.alarm.TOset tosAlarm;
	
	/* ������������ ��� ��������� ����������� ��� �������� */
	private final String HDG = "HDG.Title";
	private final String BIAS = "Bias.Title";
	
	/* ��������� �������� ���������� ������ ��� ����������� � Visual. */
	private final double FIVE = 5.0;
	private final double SIX = 6.0;
	private final double TWENTY = 20.0;
	
	// � ����� �������� ��� ������ Single ��������� T=10, �������� 2, �.�. �� ���������������� ��������.
	private final double TIME = 2.0;
	
	/* ���������� */
	private final double DEV = 0.5;
	
	/* ������������ ���������� ������ ��� ����������� � Visual. */
	private final String A = "A";
	private final String T = "T";
	private final String WZ = "Wz";

	@Override
	public void initTOsets() throws Exception {
		/*  ������������� ��������	 */
		fillAllowedAppList( RoleIDs.appVisual1, new ApplicationTypes[] {ApplicationTypes.Visual} );
		this.tosSNS = new dpat.tests.functional.sensors.TOset(RoleIDs.appVisual1);
		this.tosREF = new dpat.tests.functional.REF.TOset(RoleIDs.appVisual1);
		this.tosAlarm = new dpat.tests.functional.alarm.TOset(RoleIDs.appVisual1);
	}
	
	@Override
	public void preconditions() throws Exception {
		/// ����� DP ������ ���� ������� ��� ���������� ���� �������� ������ ����� - RunTest.
		
		/// ��������� ������� �������� Gyro � ������������.
		Do.debug("Check that Sensors Gyro are in this configuration");
		Do.dpAssert(Sensors.Gyro.totalELTnum() > 0, "Sensors Gyro are not in this configuration");
		
		/// � ������ �������� ������� Gyro.
		tosModel.btSensorFaults.pressAny();
		tosModel.switchSensorsOrREFsGroupON(Sensors.Gyro);
		
		/// �������������� ��� ������ ��������.
		tosModel.btSensorFaults.pressAny();
		tosModel.deactivateAllSensorsOrREFsGroupErrors(Sensors.Gyro);
		
		removeBias(); // �������� Bias
				
		/** �� ����� (�����) ������ ���������� ������������� ����� �.�. ������� Sensors 
		  * �� ������ (������) ������� ���� Visual � DP ����������� Alarm
		  */
		ArrayList<PanelSwitcher> pSW = PanelSwitcher.createAllpsws(getAllowedAppList().keySet());
		for( PanelSwitcher each : pSW ) each.bringPanelRight("Alarm");
	}

	@Override
	public void scenario() throws Exception {
		/// ���������� ������� Gyro � ����� Select(�������������, ��� ������������� ����������).
		ArrayList<Integer> gyroSel = tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false);
		Do.debug("Switch sensors " + Sensors.Gyro + " to mode Select"); // ������ �������� ��������, ������������ � Select.
		
		/** ����������, ���� �� ��������� Gyro (���� �� 2).
		  * ���� ��������� Gyro ������ ����, ���-�� ����� ������������ ������� ������, ���� ��������� ��� �� ������.
		  * ��������� ����������� ����� ������������ ��� skipped � ��������� � ������ ��������.
		  */	
		if(gyroSel.size() < 2) Do.skip("Less than 2 sensor " + Sensors.Gyro + " selected . Check mode of this sensor.");
		
		SensorPanel[] availableSensPanels = new SensorPanel[gyroSel.size()]; // ��������� ������ ��������
		for(int i=0; i<gyroSel.size(); i++) availableSensPanels[i] = tosSNS.snsesAll.get(Sensors.Gyro)[i];
		
		/*
		 * ����������� ������� �� ������ All, ��� ������������ ������ Details ��� ��������.
		 * ��� ��� �������(�� ���������) ������� �� ���� �������� ���������� ��������� ��� �������.
		 */
		tosSNS.snsNB.btAll.pressVW();
		tosSNS.snsNB.btGyro.pressVW();
		
		/// ���������� ������������ ������ ��� ��������� � ������ ���������� ��� ������ � �������� � ������.
		SensorPanel snsPriority = Do.sureFirstObject(availableSensPanels, // ���������� ������������ ������ ��� ���������.
						new MethodSignature(true, "modeFast", SensorREFModes.Select),  
						new MethodSignature(true, "isPriority"),
						new MethodSignature(false, "isMagnetic"));
		snsPriority.mustBePriority();
		Do.debug("Current priority sensor is " + snsPriority.button.getCaption());
		
		/// ���������� �������������� ������(�� ��������) ��� ���������.
		SensorPanel snsNotPriority = Do.sureFirstObject(availableSensPanels, 
							new MethodSignature(true, "modeFast", SensorREFModes.Select),  
							new MethodSignature(false, "isPriority"),
							new MethodSignature(false, "isMagnetic"));
		Do.debug("Sensor for comparison is " + snsNotPriority.button.getCaption());
		
		// ����� ���������� Sensor Faults ������������� ������� ��� ������ � �������� � ������.
		SensorFaultsSet snsPriorityModelFaults = tosModel.sensorsFaults.getParamSet(Sensors.Gyro, snsPriority.getNum());
		// ����� ���������� Sensor Faults ��������������� ������� ��� ������ � �������� � ������.
		SensorFaultsSet snsNotPriorityModelFaults = tosModel.sensorsFaults.getParamSet(Sensors.Gyro, snsNotPriority.getNum());
		
		/// ���������� ��� �������� ��� ����������� magnetic-�������. �������� ������ ����� �� ������ ��������������.
		SensorPanel magnetic = tosSNS.hasThereMagnetic(availableSensPanels);
		if (null != magnetic) tosModel.switchSensorOrRefOFF(magnetic.sensorGroup, magnetic.getNum());

		/// ���������� �������� ���������� discrOn � discrOff �� ����� Sensors.ini
		double discrOn = RoleIDs.appVisual1.getIniFilesParameters().getSensorsIniFilterGyroGyroDiscrOn();
		double discrOff = RoleIDs.appVisual1.getIniFilesParameters().getSensorsIniFilterGyroGyroDiscrOff();
		
		String gyroFault = Gyro_Faults.Noise.name();
		
		/// ��������� ��������������� ������� Noise 3'(discrOn) T=10s. ������ 2.0, ��� ��� �� ���������������� ��������.
		snsNotPriorityModelFaults.setValue(gyroFault, A, discrOn);
		snsNotPriorityModelFaults.setValue(gyroFault, T, TIME);
		snsNotPriorityModelFaults.activate(gyroFault);
		
		Do.debug("Set error Noise A="+discrOn+"; T="+TIME+" for Not Priority sensor "+snsNotPriority.button.getCaption());
		
		CompareDeviations cmp = 
				new CompareDeviations(tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(HDG),  snsPriority.dataIndicators.get(HDG), 360, 1);
		
		/// ������� ��������� ��������� ���� ������������� ������� � ������ ����������� ��� ����������(bias).
		Do.check4actionTO(cmp, 
				"HDG(pnRes) - HDG(snsPriority) = BIAS => " +
					tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(HDG).getNumberVal() + " - " + snsPriority.dataIndicators.get(HDG).getNumberVal() + " = " +
						tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(BIAS).getNumberVal(),
				new MethodSignature("isEqualInRangeToIndValue", tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(BIAS), 0.2, 0.0));
		
		/// ��������� Noise ��� �������� ����� 2 ���������� �������, ���� �� ��������� - ������.
		if(snsNotPriority.noiseIndicators.get(HDG).isEqualInRangeToValue(3.0, 1.0, 0.0, 1, 0)){
			Do.check4actionTO(snsNotPriority.noiseIndicators.get(HDG), new MethodSignature("isInState", NumberDataIndStates.RED));
		} else {
			Do.check4actionTO(snsNotPriority.noiseIndicators.get(HDG), new MethodSignature("isInState", NumberDataIndStates.DEFAULT));
		}
		
		snsNotPriorityModelFaults.deactivate(gyroFault);
		
		/// ��������� ������������� ������� Noise 1'(discrOn) T=20s
		snsPriorityModelFaults.setValue(gyroFault, A, discrOn + DEV);
		snsPriorityModelFaults.setValue(gyroFault, T, TWENTY);
		snsPriorityModelFaults.activate(gyroFault);	
		
		/// ���������� ����� ������ ����������� = ���� ������������� + ����������(bias)
		Do.dpAssert(tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(HDG).isEqualInRangeToIndValue(
				snsPriority.dataIndicators.get(HDG), 0.5, tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(BIAS).getNumberVal(), 1, 360), 
					"data priority sensor + bias != data pn result.");
		
		snsPriorityModelFaults.deactivate(gyroFault); // ������������ Noise.
		
		/// �������� ��������� ��������� ��� Start/Stop.
		ParamPanel gyroCorrections = checkCorrections();
		
		/// ���������� Start=max ; Stop=min, ��� �� ��������� ������������ ��������� �����.
		ParameterWithBreakingArrowsEditor start = gyroCorrections.getBreakingArrowsEditParameter("Start");
		ParameterWithBreakingArrowsEditor stop = gyroCorrections.getBreakingArrowsEditParameter("Stop");
		
		start.setCurVal(start.getPossibleParamValues()[start.getPossibleParamValues().length - 1]);
		stop.setCurVal(stop.getPossibleParamValues()[0]);
		gyroCorrections.pressApply();
		
		tosSNS.returnPriority(snsPriority);
		
		/** ----------------------------------------------<DONE>---------------------------------------------- **/
		
		gyroFault = Gyro_Faults.Single.name();
		
		/// ��������� ������������� ������� Single A = discrOn + 1; T = 5s
		snsPriorityModelFaults.setValue(gyroFault, A, SIX);
		snsPriorityModelFaults.setValue(gyroFault, T, FIVE);
		snsPriorityModelFaults.activate(gyroFault);
		
		snsPriorityModelFaults.deactivate(gyroFault);
		
		/// �������� ������ � ������� ������� ~ 0.05.
		snsPriorityModelFaults.setValue(gyroFault, T, 0.01);
		snsPriorityModelFaults.activate(gyroFault);
		
		snsPriority.modeMustBe(SensorREFModes.FaultSelect); // TODO: ������ ���������� ������� � ����� Alarms.lst
		
		/// ��������� ��������� ������� : Gyro Discrepancy � No data for a long time.
		tosAlarm.mustBeLastPresentWithReqStatus(snsPriority.getNameForAlarmType() + "_NO_GOOD_DATA",
				"(?iu)" + snsPriority.button.getCaption() + ":\\s+no\\s+good\\s+data\\s+for\\s+a\\s+long\\s+time", true, false);
		
		tosAlarm.mustBeLastPresentWithReqStatus("GYRO_DISCREPANCY",
				"(?iu)gyro\\s+discrepancy:\\s+" + snsPriority.button.getCaption() +
					"\\s+and\\s+" + snsNotPriority.button.getCaption(), true, false);

		snsPriorityModelFaults.deactivate(gyroFault);
		
		tosSNS.returnPriority(snsPriority); // ��������������� ���������� ������������ ������.
				
		/// ��������� ���� �������� � ������ ��������� Select.
		snsPriority.modeMustBe(SensorREFModes.Select);
		
		gyroFault = Gyro_Faults.Jump.name();
		
		/// ����������� Jump A = Start + 0.5
		double valStart = Double.parseDouble(start.value.getCurVal());
		snsPriorityModelFaults.setValue(gyroFault, A, valStart + DEV);
		snsPriorityModelFaults.activate(gyroFault);
		
		gyroSel = tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false);
		
		checkAlarms(snsPriority, snsNotPriority, true);
		
		/// ���������� ����� ������������ ������
		SensorPanel newPrioritySensorPanel = Do.sureFirstObject(availableSensPanels, 
				new MethodSignature(true, "modeFast", SensorREFModes.Select),  
					new MethodSignature(true, "isPriority"));
		newPrioritySensorPanel.mustBePriority();
		Do.debug("New priority sensor is " + newPrioritySensorPanel.button.getCaption());

		/// ���������, ��� ������������ ������ ���������
		Do.dpAssert(!newPrioritySensorPanel.button.getCaption().equals(snsPriority.button.getCaption()), 
				"priority sensor "+ snsPriority.button.getCaption() +" hasn't been changed!");

		/// ���������, ��� ��������� ��������� ������������� ������� ���������
		Do.dpAssert(tosSNS.snsPRind.get(Sensors.Gyro).getVal().equals(newPrioritySensorPanel.button.getCaption()),
				"String primary indicator is WRONG! (not "+newPrioritySensorPanel.button.getCaption()+")");
		Do.debug("Current string primary indicator for "+Sensors.Gyro+" is "+tosSNS.snsPRind.get(Sensors.Gyro).getVal());
				
		/// ����������������� ��������� ������ �� �������������� ����� ������������� ������� � ���������� ���������� ��������
//		tosAlarm.mustBeLastPresentWithReqStatus("CHANGE_MAIN_" + snsPriority.sensorGroup.name().toUpperCase(), 
//				"(?iu)^Main\\s+" + Sensors.Gyro.name() + "\\s+Changed:\\s+"
//						+ newPrioritySensorPanel.button.getCaption() + "\\s+\\->\\s+" 
//							+ snsPriority.button.getCaption() + ":\\s+Auto$", false, false);
		
		Do.debug("Priority change alarm for "+Sensors.Gyro+" was generated");
						
		/// ���������, ��� ���������� ������������ ������ ������ �� �������� ������������ 
		Do.dpAssert( !snsPriority.isPriority(),"Old sensor is priority as before change");
		Do.debug("Old priority sensor " + snsPriority.button.getCaption() + " is not priority now");

		snsPriorityModelFaults.setValue(gyroFault, A, discrOn - 1);
		snsPriorityModelFaults.activate(gyroFault);
		
		tosSNS.returnPriority(snsPriority); // ��������������� ���������� ������������ ������.
		
		/// ��������� ������ ������(���=True), � ����������� �� ���������� ��������.
		checkAlarms(snsPriority, snsNotPriority, true);
		
		snsPriorityModelFaults.deactivate(gyroFault);
		
		double valStop = Double.parseDouble(stop.value.getCurVal());
		
		snsPriorityModelFaults.setValue(gyroFault, A, ((valStop == discrOff) ? discrOff : valStop));    
		snsPriorityModelFaults.activate(gyroFault);
		
		gyroSel = tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false);
		
		/// ��������� ������ ������������ � �������.
		checkAlarms(snsPriority, snsNotPriority, false);
		
		snsPriorityModelFaults.deactivate(gyroFault);
		
		openCorrectionWindow(); // ������� ���� ���������� ���������.
		
		/// ��������� Start = 2 ; Stop = 1.5;
		start.setCurVal("2.0"); stop.setCurVal("1.5"); gyroCorrections.pressApply();
		
		/// ���������� Jump A = 3.0;
		snsPriorityModelFaults.setValue(gyroFault, A, 3.0);
		snsPriorityModelFaults.activate(gyroFault);
		
		/// ��������� ������ � �����������(���=True).
		checkAlarms(snsPriority, snsNotPriority, true);
		
		snsPriorityModelFaults.deactivate(gyroFault);
		
		/// ��������� ������ � �����������(���=False).
		checkAlarms(snsPriority, snsNotPriority, false);
		
		/// ��������������� ������� �.�. ��� ���������� �������� ��� Drift, ����� �� 3 � �����.
		tosModel.btSensorFaults.pressAny();
		tosModel.switchSensorsOrREFsGroupON(Sensors.Gyro); 
		gyroSel = tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false);
		tosSNS.returnPriority(snsPriority); // ���������� �������������� Gyro1
		tosSNS.snsNB.btAll.pressVW();;
		
		gyroFault = Gyro_Faults.Drift.name();
		
		snsPriorityModelFaults.setValue(gyroFault, WZ, DEV); // ��������� ����� 0.5
		snsPriorityModelFaults.activate(gyroFault);
		
		removeBias();
				
		// ���������� �������� �� ��������� �����, ���� �������� � ������������� ������� �� �������� � 3 ���� �-��� start.
		
		NumberDataInd numInd = snsPriority.dataIndicators.get(HDG);
		if(valStop < numInd.getNumberVal()){
			Do.check4actionTO(numInd, 
					"HDG : " + numInd.getNumberVal() + " is Not Equal " + valStop,
						new MethodSignature("isEqualInRangeToValue", 3 * valStop, 0.5, 0.5, 1, 0));
		}
		
		/// �������� ������ gyro median test: snsPriority rejected
		tosAlarm.mustBeLastPresentWithReqStatus("GYRO_DISCREPANCY", 
				"(?iu)gyro\\s+median\\s+test:\\s+"+snsPriority.button.getCaption()+"\\s+rejected", false, false);
		
		snsPriorityModelFaults.deactivate(gyroFault);
		
		tosModel.switchSensorsOrREFsGroupON(Sensors.Gyro);
		
		magnetic.setMode(SensorREFModes.Select);
		
		snsPriority.setMode(SensorREFModes.Select); 
		snsPriority.modeMustBe(SensorREFModes.Select);
		
		tosSNS.returnPriority(snsPriority);

		gyroFault = Gyro_Faults.Jump.name();
		
		openCorrectionWindow(); // ������� ���� ���������� ���������.
		
		/// ��������� Start = 3 ; Stop = 1.0;
		start.setCurVal("3.0"); stop.setCurVal("1.0"); gyroCorrections.pressApply();
		
		/// ���� � ������� 3 Gyro, ��������� ������ Jump ������, ��� Start
		if(gyroSel.size() > 2){
			valStart = Double.parseDouble(start.value.getCurVal());
			
			snsPriorityModelFaults.setValue(gyroFault, A, valStart + DEV);
			snsPriorityModelFaults.activate(gyroFault);	
			
			String name = (tosSNS.snsPRind.get(Sensors.Gyro).getCaption() == snsNotPriority.button.getCaption()) ? snsPriority.button.getCaption() : "magn";
			
			Do.debug(name, Do.Level.Request);
			
			/// �������� ������ gyro median test: snsPriority discrepancy
			tosAlarm.mustBeLastPresentWithReqStatus("GYRO_DISCREPANCY", 
					"(?iu)gyro\\s+median\\s+test:\\s+"+name+"\\s+discrepancy", true, false);
			
			snsNotPriorityModelFaults.setValue(gyroFault, A, valStart + DEV);
			snsNotPriorityModelFaults.activate(gyroFault);
			
			/// �������� ������ gyro median test: snsPriority discrepancy
			tosAlarm.mustBeLastPresentWithReqStatus("GYRO_DISCREPANCY", 
					"(?iu)gyro\\s+median\\s+test:\\s+"+name+"\\s+discrepancy", true, false);
		
			snsPriorityModelFaults.deactivate(gyroFault);
			snsNotPriorityModelFaults.deactivate(gyroFault);
			
			openCorrectionWindow();
			
			gyroCorrections.getDialogEditParameter("Gyro2 Correction").setCurVal("2.0");
			gyroCorrections.pressApply();
			
			// DONED
			
			snsPriorityModelFaults.setValue(gyroFault, A, 5.0 + DEV);
			snsPriorityModelFaults.activate(gyroFault);
			
			checkAlarms(magnetic, snsNotPriority, true); // snsPriority
			
			snsPriorityModelFaults.deactivate(gyroFault);
			
			checkAlarms(magnetic, snsNotPriority, true);
			
			gyroCorrections.getDialogEditParameter("Gyro2 Correction").setCurVal("0.0");
			gyroCorrections.pressApply();
			
			checkAlarms(magnetic, snsNotPriority, false);
		}
	}
	/**
	 * ������� ���������� �� �������������� ������ ������������.
	 */
	private void removeBias()
	{
		tosSNS.snsNB.btAll.pressVW();
		SensResults pnGyroResults = tosSNS.pnResAll.get(Sensors.Gyro);
		pnGyroResults.getBtRemoveBias().pressVW();
		pnGyroResults.getDlgRemoveBias().pressButton1OK();
	}
	
	/**
	 * ������� ���� ��������� ������� Gyro, ��������� ���������� ������������ ������� Param -> Graph.
	 */
	private void openCorrectionWindow(){
		///> ������� ���� ��������� ���������� �������� Gyro.
		tosSNS.snsNB.btAll.pressVW();
		tosSNS.snsNB.btGyro.pressVW();
			
		///> ���������� ���� Gyro Parameters(Correction). ���������, ��� ������������ Param(�� ���������) ����� Graph
		Do.dpAssert(tosSNS.snsNB.btParamGraph.getCaption().equalsIgnoreCase("param"), "Must be activated button Param");
		tosSNS.snsNB.btParamGraph.pressVW();
		Do.dpAssert(tosSNS.snsNB.btParamGraph.getCaption().equalsIgnoreCase("graph"), "Must be activated button Graph");
	}
	
	/**
	 * ����� ��� �������� ��������� ������������
	 * @return ParamPanel - ������ ���������
	 */
	private ParamPanel checkCorrections(){
		tosSNS.snsNB.btAll.pressVW(); tosSNS.snsNB.btGyro.pressVW();
		
		///> ���������� ���� Gyro Parameters(Correction).
		Do.dpAssert(tosSNS.snsNB.btParamGraph.getCaption().equalsIgnoreCase("param"), "Must be activated button Param");
		tosSNS.snsNB.btParamGraph.pressVW();
		Do.dpAssert(tosSNS.snsNB.btParamGraph.getCaption().equalsIgnoreCase("graph"), "Must be activated button Graph");
		
		ParamPanel gyroCorrections = tosSNS.getPnGyroParam();
		
		///> ��������� �������� ��������� �������� ��������� ��� Start/Stop.
		for(String param : gyroCorrections.getBreakingArrowsEditParametersKeySet()){
			ParameterWithBreakingArrowsEditor p = gyroCorrections.getBreakingArrowsEditParameter(param);	
			Do.dpAssert(p.isPossibleParamValuesArrayEquals(p.getPossibleParamValues()), 
					"Incorrect range of numbers for param " + param);
		}
		
		return gyroCorrections;
	}
	
	/**
	 * ��������� ������, � ����������� �� ���������� �������� Gyro.
	 * @param snsPriority - ������������ ������.
	 * @param snsNotPriority - �������������� ������.
	 * @param state - true - Actual / false - Not Actual.
	 */
	private void checkAlarms(SensorPanel snsPriority, 
							 SensorPanel snsNotPriority, 
							 boolean state){
		ArrayList<Integer> gyroSel = tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false); 
		if(gyroSel.size() > 2){
			tosAlarm.mustBeLastPresentWithReqStatus("GYRO_DISCREPANCY", 
					"(?iu)gyro\\s+median\\s+test:\\s+"+snsPriority.button.getCaption()+"\\s+discrepancy", state, false);
		} else if(gyroSel.size() == 2){
			tosAlarm.mustBeLastPresentWithReqStatus("GYRO_DISCREPANCY", 
					"(?iu)gyro\\s+discrepancy:\\s+"+snsPriority.button.getCaption()+"\\s+and\\s+"+snsNotPriority.button.getCaption(), state, false);
		}
	}

	/// ���������� DP � �������� ���������
	@Override
	public void postconditions() throws Exception {
		///> �������������� ��� ������ ��������.
		tosModel.deactivateAllSensorsOrREFsGroupErrors(Sensors.Gyro);
		
		///> �������� Gyro, Wind, GPS � Select.
		tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false);
		tosSNS.setWholeGroupFast(Sensors.Wind, SensorREFModes.Select, false);
		tosREF.setWholeGroupFast(REFs.GPS, SensorREFModes.Select, false, false);
	}
}