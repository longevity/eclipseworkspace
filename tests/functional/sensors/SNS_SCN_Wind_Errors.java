package dpat.tests.functional.sensors;

import java.util.ArrayList;
import java.util.Arrays;

import dpat.coreLib.common.config.Parameters.Params;
import dpat.coreLib.common.config.Sensors;
import dpat.coreLib.common.constants.ApplicationTypes;
import dpat.coreLib.common.constants.Modes.HdgModes;
import dpat.coreLib.common.constants.RoleIDs;
import dpat.coreLib.common.constants.Time;
import dpat.coreLib.common.Do;
import dpat.coreLib.common.MethodSignature;
import dpat.coreLib.panels.PanelSwitcher;
import dpat.coreLib.common.constants.SensorREFModes;
import dpat.coreLib.indicators.CompareDeviations;
import dpat.coreLib.indicators.NumberDataInd;
import dpat.coreLib.indicators.NumberWithIndependentUnitsModelEditField;
import dpat.coreLib.panels.SensorPanel;
import dpat.coreLib.panels.ParamPanel;
import dpat.coreLib.parameters.ParameterWithChangeEditor;
import dpat.coreLib._model.motionSignals.ReinitParametersDialog;
import dpat.coreLib._model.sensorFaults.SensorFaultsSet;
import dpat.coreLib._model.sensorFaults.Wind_Faults;
import dpat.tests.RunTest;

/**
 * ��������� ��������� �������� Wind(priority/not priority) ��� ������� ������ � �������� ������:
 * - ��������� ������ Wind discrepancy ����� ����������� ������, ������ �� �������������� ����� ������������� �������,
 * - ��������� �������� �������������/��������������� ��������,
 * - ��������� ������� ��� ���������� ������������� �������� �������� ����� (���������).
 * - ��������� ������������ ������ ������ ������ ���������.
 * - ������������� ��������, ����� ������ ����� ���� ������� � ���������� ���������.
 * - �������� ����������� �������� � ����������� ����� � ������ � ��������� ��������� � ��������.
 * ������ Squall, Shadow. ����������� � ������ �����.(SNS_NEG_WindSpecific.java)
 * @author v.kulikov
 */
public class SNS_SCN_Wind_Errors extends RunTest {
	
	/* �������� ������� ��� ��������, ��� - ������������������� ������, �������, ����������. */
	public dpat.tests.functional.sensors.TOset tosSNS;
	public dpat.tests.functional.MFpanel.TOset tosMFP;
	public dpat.tests.functional.alarm.TOset tosAlarm;
	public dpat.tests.functional.parameters.TOset tosParam;
	
	/* ���������� �������� ����� �� ���������� �������� [�/�]. */
	private final double DEV = 0.5;
	private final double ZERO = 0.0;
	
	/* ������������ ���������� ������ ��� ����������� � ������. */
	private final String AS = "As";
	private final String AA = "Aa";
	private final String A = "A";
	private final String T = "T";
	
	/* ������������ ��� ��������� ����������� ��� �������� */
	private final String HDG = "HDG.Title"; 
	private final String SPD = "SPD.Title";
	private final String DIR = "R DIR.Title";
	private final String TDIR = "TDir.Title";
	
	@Override
	public void initTOsets() throws Exception {
		/*  ������������� ��������	 */
		fillAllowedAppList( RoleIDs.appVisual1, new ApplicationTypes[] {ApplicationTypes.Visual} );
		tosSNS = new dpat.tests.functional.sensors.TOset(RoleIDs.appVisual1);
		tosMFP = new dpat.tests.functional.MFpanel.TOset(RoleIDs.appVisual1);
		tosAlarm = new dpat.tests.functional.alarm.TOset(RoleIDs.appVisual1);
		tosParam = new dpat.tests.functional.parameters.TOset(RoleIDs.appVisual1);
	}
	
	@Override
	public void preconditions() throws Exception {
		/// ����� DP ������ ���� ������� ��� ���������� ���� �������� ������ ����� - RunTest.
		
		/// ����������, ����� �������� �������, � Sensors.ini � [FilterWind] �����  Discrepancy Vx = 5, Discrepancy Vy = 2
		if(RoleIDs.appVisual1.getIniFilesParameters().getSensorsIniFilterWindDiscrepancyVx() == 
				RoleIDs.appVisual1.getIniFilesParameters().getSensorsIniFilterWindDiscrepancyVy()){
			RoleIDs.appVisual1.getIniFilesParameters().setSensorsIniFilterWindDiscrepancyVx(5.0);
			RoleIDs.appVisual1.getIniFilesParameters().setSensorsIniFilterWindDiscrepancyVy(2.0);
			RoleIDs.appVisual1.restartSystem();
		}
		
		/// ��������� ������� �������� Gyro � Wind � ������������.
		Do.dpAssert(Sensors.Gyro.totalELTnum() > 0 && Sensors.Wind.totalELTnum() > 0, 
				"Sensors " + Sensors.Gyro + " or " + Sensors.Wind + " are not in this configuration.");
		Do.debug("Verified. Sensors " + Sensors.Gyro + " and " + Sensors.Wind + " are in this configuration.");
		
		/// ���� Gyro ������ ���� ����������� �������.
		tosSNS.setNsensorsInGroup(Sensors.Gyro, SensorREFModes.Select, 1, false, true);
		
		/// � ������ �������� ������� �����
		tosModel.switchSensorsOrREFsGroupON(Sensors.Wind);
			
		/// ������ ����������� � ���� Param->Units �� m/s.
		Do.debug("Change units of Wind Speed to [m/s] in the window Param.");
		
		/** �� ����� (�����) ������ ���������� ������������� ����� �.�. ������� Sensors; 
		  * �� ������ (������) ������� ���� Visual � DP ����������� Param
		  */
		ArrayList<PanelSwitcher> pSW = PanelSwitcher.createAllpsws(getAllowedAppList().keySet());
		for( PanelSwitcher each : pSW ) each.bringPanelRight("Param");
		
		ParamPanel units = tosParam.params.get(Params.UNITS);
		
		tosParam.bringParamPanel(units);
		
		/// � ������� ���������� ����������� � m/s.
		ParameterWithChangeEditor param = units.get�hangeEditParameter("Wind Speed");
		param.setCurVal("m/s");
		units.pressApply();
	
		/// � ������ ������� ������� ����������(������ MotionSignals).
		tosModel.btMotionSignals.pressVW();
		tosModel.motionSignals.deactivateGeneralDisturbance();

		/** �� ����� (�����) ������ ���������� ������������� ����� �.�. ������� Sensors; 
		  * �� ������ (������) ������� ���� Visual � DP ����������� Alarm
		  */
		for( PanelSwitcher each : pSW ) each.bringPanelRight("Alarm");
	}
	
	@Override
	public void scenario() throws Exception {						
		/// ���������� ������� Wind � ����� Select(��� ������������� ����������).
		ArrayList<Integer> windSel = tosSNS.setWholeGroupFast(Sensors.Wind, SensorREFModes.Select, false);
		Do.debug("Switch All " + Sensors.Wind + " Sensors to mode Select.");
			
		/** ����������, ���� �� ��������� Wind (���� �� 2).
		  * ���� ��������� Wind ������ ����, ���-�� ����� ������������ ������� ������, ���� ��������� ��� �� ������.
		  * ��������� ����������� ����� ������������ ��� skipped � ��������� � ������ ��������.
		  */
		if(windSel.size() < 2) Do.skip("Less than 2 sensors " + Sensors.Wind + " selected . Check mode of this sensor."); 
			
		/// ��������� ���������(Vx, Vy) �� Sensors.ini, ��� ���������� �������� ������� ����� ���������� ������.
		double discrVx = RoleIDs.appVisual1.getIniFilesParameters().getSensorsIniFilterWindDiscrepancyVx();
		double discrVy = RoleIDs.appVisual1.getIniFilesParameters().getSensorsIniFilterWindDiscrepancyVy();
		
		Do.debug("Filter Wind Discrepancy Vx = " + discrVx + "; Filter Wind Discrepancy Vy = " + discrVy);
			
		/// ��������� ������������ �������� ����� �� Sensors.ini, ��� ���������� ������� ������ ������� � FaultSelect.
		double maxWindSpeed = RoleIDs.appVisual1.getIniFilesParameters().getSensorsIniFilterWindMaxWindSpeed();
	
		SensorPanel[] availableSensPanels = new SensorPanel[windSel.size()]; // ��������� ������ ������� �������� � Select
				
		for(int i=0; i<windSel.size(); i++) availableSensPanels[i] = tosSNS.snsesAll.get(Sensors.Wind)[windSel.get(i)];
		
		tosSNS.snsNB.btWind.pressVW(); /// ������� ���� ��������� ���������� �������� Wind.
				
		/// ���������� ������������ ������ ��� ���������.
		SensorPanel snsPriority = Do.sureFirstObject(availableSensPanels, 
						new MethodSignature(true, "modeFast", SensorREFModes.Select),  
						new MethodSignature(true, "isPriority"));
		snsPriority.mustBePriority();
		Do.debug("Current priority sensor is " + snsPriority.button.getCaption());
			
		/// ������� ������ �� �����.
		tosSNS.bringSensorPanel(snsPriority);
				
		/// ���������� �������������� ������ ��� ���������.
		SensorPanel snsNotPriority = Do.sureFirstObject(availableSensPanels,
							new MethodSignature(true, "modeFast", SensorREFModes.Select),  
							new MethodSignature(false, "isPriority"));
		Do.debug("Sensor for comparison is " + snsNotPriority.button.getCaption());
			
		// ���������� ����� ���������� Sensor Faults ��� ������������� �������
		SensorFaultsSet snsPriorityModelFaults = tosModel.sensorsFaults.getParamSet(Sensors.Wind, snsPriority.getNum());
							
		NumberDataInd hdg = tosSNS.pnResAll.get(Sensors.Gyro).processedData.get(HDG);
		
		/// ���� ���� ����� �� 90..270, �� ������ ��� � ��������� ���������, ���� ��� �����������.
		if(hdg.isEqualInRangeToValue(180.0, 90.0, 0.0, 1, 0)){
			setInitialVesselCoords();
		}
		
		/// ���������� ���� 0 ��������(HDG Auto->New Set->0).
		tosMFP.btHdgAUTO.pressVW();
		tosMFP.dlgHdgAuto.setValue("0");
		tosMFP.mustBeActiveHdgMode(HdgModes.HDG_AUTO);
		
		/**
		 * ���������� �������� �� ��������� �����, ���� ���� �� ������ 0 �������� �� ������ ����������� Gyro.
		 */
		Do.check4actionTO(hdg, "HDG is not reset!!",
				new MethodSignature(true, "isEqualInRangeToValue", 0.0, 0.5, 0.0, 1, 180));	
		Do.debug("Choised MFP->HDG Auto->New Set->0.");
			
		/// ������������� ������� ��������� ���� ������, ��� 2X. ��������: Jump As=10.5, Aa=0.0 [kn] [�������].
		double jump_as = 2 * discrVx + DEV;
		double jump_aa = ZERO;
		
		String windFault = Wind_Faults.Jump.name();
		
		snsPriorityModelFaults.setValue(windFault, AS, jump_as);
		snsPriorityModelFaults.setValue(windFault, AA, jump_aa);
		snsPriorityModelFaults.activate(windFault);
		
		/// ���� ������� ����� ������� ����������� � �������������� �������� >= Vx => Wind discrepancy.
		Do.check4actionTO(tosSNS.pnResAll.get(Sensors.Wind).processedData.get(SPD),
			"Wind Speed is not Increased To " + discrVx + " It's " + tosSNS.pnResAll.get(Sensors.Wind).processedData.get(SPD).getNumberVal(),
				new MethodSignature(true, "isEqualInRangeToValue", discrVx, 0.1, 0.0, 1, 0));
		
		// ������ ��� ��������� �������� � ���� �����������.
		CompareDeviations cmpWindDev = new CompareDeviations(snsNotPriority.dataIndicators.get(SPD), 
														  snsPriority.dataIndicators.get(SPD), 180, 1);
		double deviation = cmpWindDev.calculateDeviation(); // ���������� ���������� ��������.
		
		/// ��������� ���������� ������������� � ��������������� ������� � ������ �������� �� Sensors.ini
		Do.check4actionTO(cmpWindDev,
			"Error!! Deviation between " + snsPriority.button.getCaption() +
				" and " + snsNotPriority.button.getCaption() + " are not equals in range [" +
						-(discrVx + 2 * DEV) + ", " + -discrVx + "]. Deviation is " + deviation,	
				new MethodSignature("isEqualInRangeToValue", -5.5, 0.5, 0.0));
		
		/// ���������� �������� �� ��������� �����, ���� �������� �������� �� ������ ����������� �� ������ ������ ����������.
		Do.check4actionTO(tosSNS.pnResAll.get(Sensors.Wind).processedData.get(SPD),
			"Invalid SPD ind value : " + tosSNS.pnResAll.get(Sensors.Wind).processedData.get(SPD).getNumberVal(),
				new MethodSignature(true, "isEqualInRangeToValue", discrVx, 0.5, DEV, 1, 0));
		
		/// ����� �����: Wind Discrepancy
		tosAlarm.mustBeLastPresentWithReqStatus("WIND_SENSOR_DISCREPANCY", "(?iu)wind\\s+discrepancy", true, false);
		
		snsPriorityModelFaults.deactivate(windFault); // ������������ Jump
		
		waitZeroValues(); // ��� ��������� �������� ����� �� ������ �����������.
		
			jump_as = 4.0; jump_aa = ZERO;
		
		/// ���������� Jump As = 4.0 Aa = 0.0 [kn].
		snsPriorityModelFaults.setValue(windFault, AS, jump_as);
		snsPriorityModelFaults.setValue(windFault, AA, jump_aa);
		snsPriorityModelFaults.activate(windFault); Do.debug("Set Jump " + AS + " = " + jump_as + ", " + AA + " = " + ZERO);
		
		/// ���������� �������� �� ��������� �����, ���� �������� �� ������ ����������� � �������� �� ������ ������ 2.1.
		Do.check4actionTO(tosSNS.pnResAll.get(Sensors.Wind).processedData.get(SPD),
			"SPD num : " + tosSNS.pnResAll.get(Sensors.Wind).processedData.get(SPD).getNumberVal() + " is not equal 2.0",
				new MethodSignature("isEqualInRangeToValue", 2.0, 0.5, 0.5, 1, 360));
		
		deviation = cmpWindDev.calculateDeviation(); // ������������ ����� ����������.
		
		/// ��������� ���������� ������������� � ��������������� ������� � ������ �������� �� Sensors.ini
		Do.check4actionTO(cmpWindDev,
			"Error!! Deviation between " + snsPriority.button.getCaption() +
				" and " + snsNotPriority.button.getCaption() + " are not equals -2.0 . Deviation is " + deviation,	
					new MethodSignature("isEqualInRangeToValue", -jump_as/2, 0.5, 0.0));
		
		/// �����: Wind Discrepancy ������������.
		tosAlarm.mustBeLastPresentWithReqStatus("WIND_SENSOR_DISCREPANCY", "(?iu)wind\\s+discrepancy", false, false);
		
		snsPriorityModelFaults.deactivate(windFault);
		
		waitZeroValues(); // ��� ��������� �������� ����� �� ������ �����������.
		
		/// ������ ����������� � 0 ��������.
		snsPriorityModelFaults.setValue(windFault, AA, ZERO);
		
			jump_as = 2 * discrVy + DEV;
			jump_aa = 90;
			
		/// ���������� Jump As = 2Y + 0.5; Aa = 90; [kn, degree].
		snsPriorityModelFaults.setValue(windFault, AS, jump_as);
		snsPriorityModelFaults.setValue(windFault, AA, jump_aa);
		snsPriorityModelFaults.activate(windFault);
		
		/// ���������� �������� �� ��������� �����, ���� �������� �������� �� ������ ����������� �� ������ 2.5.
		Do.check4actionTO(tosSNS.pnResAll.get(Sensors.Wind).processedData.get(SPD), 
							new MethodSignature(true, "isEqualInRangeToValue", discrVy, 0.4, DEV, 1, 0));
		
		/// ����� �����: Wind Discrepancy
		boolean state = (tosSNS.pnResAll.get(Sensors.Wind).processedData.get(SPD).getNumberVal() > discrVy) ? true : false;
		
		tosAlarm.mustBeLastPresentWithReqStatus("WIND_SENSOR_DISCREPANCY", "(?iu)wind\\s+discrepancy", state, false);	
		
		snsPriorityModelFaults.setValue(windFault, AA, ZERO); // ������ ����������� � 0 ��������.
		
		/// ����� �����: Wind Discrepancy (������������).
		tosAlarm.mustBeLastPresentWithReqStatus("WIND_SENSOR_DISCREPANCY", "(?iu)wind\\s+discrepancy", false, false);	
		
		snsPriorityModelFaults.deactivate(windFault);
		
		waitZeroValues(); // ��� ��������� �������� ����� �� ������ �����������.

		/// �������������� �������� ���������� Invalid Msg (������������ ����������).
		for(int i=0; i<availableSensPanels.length; i++){
			if(i == snsPriority.getNum() - 1) continue;
			String fault = Wind_Faults.InvalidMsg.name();
			tosModel.sensorsFaults.getParamSet(Sensors.Wind, availableSensPanels[i].getNum()).activate(fault);
		}
		
		jump_as = 2 * discrVx + 5.0; 
		jump_aa = 25; // ���������� ����������� [�������].
		
		/// ���������� Jump As = 15; Aa = 25;
		snsPriorityModelFaults.setValue(windFault, AS, jump_as);
		snsPriorityModelFaults.setValue(windFault, AA, jump_aa);
		snsPriorityModelFaults.activate(windFault);
		
		/// ����� �����: Wind Discrepancy (����������+�������������).
		tosAlarm.mustBeNotLastPresentWithReqStatus("WIND_SENSOR_DISCREPANCY", "(?iu)wind\\s+discrepancy", true, true);
		
		for(int i=0; i<availableSensPanels.length; i++){
			String fault = Wind_Faults.InvalidMsg.name();
			tosModel.sensorsFaults.getParamSet(Sensors.Wind, availableSensPanels[i].getNum()).deactivate(fault);	
			availableSensPanels[i].setMode(SensorREFModes.Select);
		}
		
		snsPriorityModelFaults.deactivate(windFault);
		
		windFault = Wind_Faults.Noise.name();
		
		waitZeroValues(); // ��� ��������� �������� ����� �� ������ �����������.
		
		/// ���������� Noise 10 � 5 � ������� �� ��������� ����������� � �������� +- 5 ��������.
		snsPriorityModelFaults.setValue(windFault, A, 10.0);
		snsPriorityModelFaults.setValue(windFault, T, 5.0);
		snsPriorityModelFaults.activate(windFault);
		
		/**
		 * ������� ����� checkNoiseData(SensorPanel snsPriority)
		 * ��� �������� ��������� ����������� �������� ������� Wind ����� ����������� Noise.
		 */
		checkNoiseData(snsPriority);
		
		snsPriorityModelFaults.deactivate(windFault);
		
		waitZeroValues(); // ��� ��������� �������� ����� �� ������ �����������.
		
		windFault = Wind_Faults.Single.name();
		
		// ���������� �������� ����������� ����� ������������ ��������� Single.
		double valBeforeSingle = snsPriority.dataIndicators.get(DIR).getNumberVal();
				
		/// ���������� Single A=10, T=5.
		snsPriorityModelFaults.setValue(windFault, A, 10.0);
		snsPriorityModelFaults.setValue(windFault, T,  5.0);
		snsPriorityModelFaults.activate(windFault);
		
		/**
		 * ������� ����� checkSingleData(SensorPanel snsPriority, double valBeforeSingle)
		 * ��� �������� ��������� ����������� �������� ������� Wind ����� ����������� Single.
		 */
		checkSingleData(snsPriority, valBeforeSingle);

		snsPriorityModelFaults.deactivate(windFault);
		
			jump_as = +1.0; 
			jump_aa = -6.0;
			
		windFault = Wind_Faults.Jump.name();
		
		/// ���������� Jump As = 1.1; Aa = -6.0;
		snsPriorityModelFaults.setValue(windFault, AS, jump_as);
		snsPriorityModelFaults.setValue(windFault, AA, jump_aa);
		snsPriorityModelFaults.activate(windFault);
		
		waitZeroValues(); // ��� ��������� �������� ����� �� ������ �����������.
		
		deviation = cmpWindDev.calculateDeviation(); // ���������� ���������� ��������.
		
		Do.check4actionTO(cmpWindDev, 
			"Error! Deviation between priority and not priority sensors are not equal in range [-1.5, -0.5]. Deviation is " + deviation,
				new MethodSignature("isEqualInRangeToValue", -jump_as, 0.5, 0.0));
		
		CompareDeviations cmpDev2 = new CompareDeviations(snsNotPriority.dataIndicators.get(DIR), snsPriority.dataIndicators.get(DIR));
		
		double dev = cmpDev2.calculateDeviation();
		
		String msg = "Deviation of Direction Priority - NotPriority sensors " + 
									((Math.abs(dev) == jump_aa) ? " equals " + dev : " not equals " + dev);
		Do.debug(msg);
		
		snsPriorityModelFaults.deactivate(windFault);
		
		waitZeroValues(); // ��� ��������� �������� ����� �� ������ �����������.
			
		tosSNS.snsNB.btAll.pressVW();
		
			jump_as = 2 * maxWindSpeed + DEV;
			jump_aa = ZERO;
			
		/// ��������� ������� ����� �������� > 30 �����.(�������� ������ �� ����� Sensors.ini)
		snsPriorityModelFaults.setValue(windFault, AS, jump_as); // ��������� �� 2 �.�. SPD in knots
		snsPriorityModelFaults.setValue(windFault, AA, jump_aa);
		snsPriorityModelFaults.activate(windFault);
		
		/// ��������� ������� ������������� ������� � ���������� ����� ����������.
		snsPriority.modeMustBe(SensorREFModes.FaultSelect);
		
		/// ����� �����: Wind: no good data for a long time
//		tosAlarm.mustBeLastPresentWithReqStatus("WIND_NO_GOOD_DATA", 
//			"(?iu)" + snsPriority.button.getCaption().toLowerCase() + ":\\s+no\\s+good\\s+data\\s+for\\s+a\\s+long\\s+time", true, false);
		
		snsPriorityModelFaults.deactivate(windFault);
		
		SensorPanel snsIsNot = Do.sureFirstObject(availableSensPanels, // ��������� ������ � ��������� FaultSelect.
				new MethodSignature(true, "modeFast", SensorREFModes.FaultSelect));
		
		snsPriorityModelFaults.deactivate(windFault);
		
		/// ������ �������� ���� �������� Wind.
		snsIsNot.button.longPressVW();
		tosSNS.reSelAll.pressButton1OK();
		snsIsNot.modeMustBe(SensorREFModes.Select);
		
		/// ���������� �������������� ������� �������.
		tosSNS.returnPriority(snsPriority);
		
		/// ��������� ������������ ������ ������ ������ ���������.
		switchRelTrueData();
	
		/// ������������� ��������, ����� ������ ����� ���� ������� � ���������� ���������. �������������� MWV_R. ������������ MWV_T.
		snsPriorityModelFaults.deactivate(Wind_Faults.MWV_R.name());
		snsPriorityModelFaults.activateProtocol(snsPriority.sensorGroup, Wind_Faults.MWV_T);
		
		Do.check4actionTO(snsPriority, new MethodSignature("modeIs", SensorREFModes.FaultSelect));
		
		/// ����� ����� (���������� + ���������������): Wind: invalid data
		tosAlarm.mustBeLastPresentWithReqStatus(snsPriority.getNameForAlarmType() + "_FAULT", 
				"(?iu)" + snsPriority.button.getCaption() + ":\\s+invalid\\s+data", true, false);
		
		snsPriorityModelFaults.activate(Wind_Faults.MWV_R.name());
		
		/// ����� �����(������������ + ���������������): Wind: invalid data 
		tosAlarm.mustBeLastPresentWithReqStatus(snsPriority.getNameForAlarmType() + "_FAULT", 
									"(?iu)" + snsPriority.button.getCaption() + ":\\s+invalid\\s+data", false, false);
		
		/// ���������� �������������� ��������� MWV_R � ����������� MWV_T.
		snsPriorityModelFaults.deactivate(Wind_Faults.MWV_R.name());
		snsPriorityModelFaults.activateProtocol(snsPriority.sensorGroup, Wind_Faults.MWV_R);
		
		/// �������� ��� ������� �����.
		tosModel.switchSensorsOrREFsGroupOFF(Sensors.Wind);
		
		Do.check4actionTO(snsPriority, new MethodSignature("modeIs", SensorREFModes.FaultSelect));
		
		Do.pause(Time.POP_UP_ON_SCREEN);
		
		tosModel.switchSensorsOrREFsGroupON(Sensors.Wind);
		
		snsPriority.setMode(SensorREFModes.Select);
		
		/// ����� �����(������������ + ���������������): Wind: invalid data 
		tosAlarm.mustBeLastPresentWithReqStatus("WIND_FILTER_STARTED", 
				"(?iu)" + Sensors.Wind.name() + "\\s+filter\\s+started", false, false);
		
		/// ��������� ������ ����� ��� ����������� �������� � ������.
		checkWindSpeedDir(availableSensPanels);
	}
	
	/**
	 * ����� ��� �������� ���������������� ��������� ������ � Visual ��� ����������� ������ Noise. 
	 * ��������� ������ �������� �������.
	 * @param snsPriority ������������ ������ (������ SensorPanel).
	 */
	private void checkNoiseData(SensorPanel snsPriority){
		/// ��������� 5 ���... ��������� ������ ������ �������� �������� ������� Noise.
		for(int i=0; i!=5; i++){
			///> ������ �������� �� ��������� ���������� (������� ��������) +- 5 ��������.
			Do.check4actionTO(snsPriority.dataIndicators.get(DIR), // �������� ���������, ���������� �� ����������� �����.
								new MethodSignature("isEqualInRangeToValue", snsPriority.dataIndicators.get(DIR).getNumberVal(), 5.0, 0.0, 1, 360));
		}
	}
	
	/**
	 * ����� ��� �������� �������������� ��������� ������ � Visual ��� ����������� ������ Single. 
	 * ��������� ������ �������� �������.
	 * @param snsPriority ������������ ������ (������ SensorPanel).
	 * @param valBeforeSingle �������� ����������� ����� ������������ Single.
	 */
	private void checkSingleData(SensorPanel snsPriority, double valBeforeSingle){
		///> ������������� ���� � ����.
		tosMFP.btHdgAUTO.pressVW();
		tosMFP.dlgHdgAuto.hold();
		
		///> ��������� 5 ���... ��������� ������ ������ �������� �������� ������� Single.
		for(int i=0; i!=5; i++){
			for(double dev : Arrays.asList(+10.0, -10.0)){
				// �������� ���������, ���������� �� ����������� �����.
				NumberDataInd dir = snsPriority.dataIndicators.get(DIR);
				
				///- ������ �������� �� ��������� ���������� (��������+10) +- 2 �������.
				Do.check4actionTO(dir, "Ind DIR value " + dir.getNumberVal() + " is not equal : " + (valBeforeSingle + dev),
					new MethodSignature("isEqualInRangeToValue", valBeforeSingle, 5.0, dev, 1, 360));
			}
		}
	}
	
	/**
	 * ��������� ���������� ����� � ������� �� ��������� � ������������� � ������.
	 * @param availableSensPanels - ������ ��������� ������� ��������.
	 */
	private void checkWindSpeedDir(SensorPanel[] availableSensPanels){
		///> ���������� � ������ ��� ����� ��������=12 ; �����������=150.
		tosModel.btMotionSignals.pressVW();
		tosModel.motionSignals.deactivateGeneralDisturbance();
		tosModel.motionSignals.setWind(12.0, 150.0);
		
		///> � ������� ����������� ������ ��������� ������� � RawData.
		tosSNS.switchRawUsedData("RawData");
		
		for(SensorPanel sensorPanel : availableSensPanels){
			///- �������� ���������, ���������� �� �������� �����.
			Do.check4actionTO(sensorPanel.dataIndicators.get(SPD),
					new MethodSignature("isEqualInRangeToValue", 12.0, 0.5, 0.0, 1, 0));
			///- �������� ���������, ���������� �� �������� �����.
			Do.check4actionTO(sensorPanel.dataIndicators.get(DIR),
					new MethodSignature("isEqualInRangeToValue", 150.0, 1.0, 0.0, 1, 0));
		}
		
		///> � ������������� ����������� ���������� ������ ��������� ������������ � ������.
		if(tosSNS.pnResAll.get(Sensors.Wind).isWindTrueDataMode())
		{
			tosSNS.pnResAll.get(Sensors.Wind).switchWindTrueRelData();
			///- �������� ���������, ���������� �� �������� ����� �� ������ �����������.
			Do.check4actionTO(tosSNS.pnResAll.get(Sensors.Wind).processedData.get(SPD),
					new MethodSignature("isEqualInRangeToValue", 12.0, 0.5, 0.0, 1, 0));
			///- �������� ���������, ���������� �� �������� ����� �� ������ �����������.
			Do.check4actionTO(tosSNS.pnResAll.get(Sensors.Wind).processedData.get(TDIR), 
					new MethodSignature("isEqualInRangeToValue", 150.0, 1.0, 0.0, 1, 0));
		}
		
		///> ������� ����� � ������.
		tosModel.motionSignals.setWind(0.0, 0.0);
	}
	
	/**
	 * ����� ��� �������� ������������ ������ ������ ������ ���������.
	 */
	private void switchRelTrueData()
	{
		if(tosSNS.pnResAll.get(Sensors.Wind).isWindTrueDataMode()){
			tosSNS.pnResAll.get(Sensors.Wind).mustBeWindTrueDataMode();
		} else {
			tosSNS.pnResAll.get(Sensors.Wind).mustBeWindRelDataMode();
			tosSNS.pnResAll.get(Sensors.Wind).switchWindTrueRelData();
			tosSNS.pnResAll.get(Sensors.Wind).mustBeWindTrueDataMode();
		}
	}
	
	/**
	 * ����� ��� �������� ���������� �������� �������� ����� �� ������ �����������.
	 */
	private void waitZeroValues(){
		///> ���������� �������� �� ��������� �����, ���� �������� �� ������ ����������� �� ���������.
		Do.check4actionTO(tosSNS.pnResAll.get(Sensors.Wind).processedData.get(SPD), 
			new MethodSignature("isEqualInRangeToValue", 0.0, 0.5, 0.0, 1, 0));
	}
	
	/**
	 * ����� ����������� ���������� ��������� �����. ���������� ��������� ����� ����������� �������.
	 */
	private void setInitialVesselCoords()
	{
		///> �������� �������� � ReinitDialog ����� ��������� ���������� �����.
		ReinitParametersDialog reinit = tosModel.reinitParamsDialog;
		
		tosModel.btReinit.pressVW(); // ������� ������ ReinitParameters.
		reinit.mustBeOpen();
		
		NumberWithIndependentUnitsModelEditField[] numInds = // ������ ����� � ��������� ���������.
								{ reinit.getXg(), reinit.getYg(), reinit.getCourse(), reinit.getVelocity() };

		for(NumberWithIndependentUnitsModelEditField numInd : numInds)
		{
			reinit.setValueWithoutApplyChanges(numInd, 0.0);
		}
		
		reinit.pressOk(); // ������������ �������� ���������.
		reinit.mustBeClosed(); // ������ ������.
	}
	
	/// ���������� DP � �������� ���������
	@Override
	public void postconditions() throws Exception {
		/// � ������ �������������� ��� ������.
		tosModel.deactivateAllSensorsOrREFsGroupErrors(Sensors.Wind);
		
		/// � ������ �������� ������� Wind.
		tosModel.switchSensorsOrREFsGroupON(Sensors.Wind);
		
		/// � Visual �������� ������� ��� ������� Wind � Select.
		tosSNS.setWholeGroupFast(Sensors.Wind, SensorREFModes.Select, false);
		Do.debug("Set All " + Sensors.Wind + " Sensors to Select after test.");
	}
}