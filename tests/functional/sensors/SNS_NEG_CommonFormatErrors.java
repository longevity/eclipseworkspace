package dpat.tests.functional.sensors;

import dpat.coreLib._model.sensorFaults.Gyro_Faults;
import dpat.coreLib._model.sensorFaults.SensorFaultsSet;
import dpat.coreLib.common.Do;
import dpat.coreLib.common.MethodSignature;
import dpat.coreLib.common.config.Sensors;
import dpat.coreLib.common.constants.ApplicationTypes;
import dpat.coreLib.common.constants.RoleIDs;
import dpat.coreLib.common.constants.SensorREFModes;
import dpat.coreLib.panels.PanelSwitcher;
import dpat.coreLib.panels.SensorPanel;
import dpat.tests.RunTest;

import dpat.tests.functional.alarm.TOset.Alarm;

import java.util.ArrayList;
import java.util.Collections;

/**
 * ��������� ��������� ����� �������� ��� ������� ����� ��������� ������ � �������� ������:
 * - ���������� �������� �� ������� � ������ (����� �������� ��������� ������� ��� � ��� ��������� ������).
 * - Common Format Errors: Empty Data, Invalid Msg.
 * 
 * �����������:
 * - ������� ������� ��������� ������ ������ �����������;
 * - ������(��������������� ���������);
 * - ������(�����/��������), ������(���������), ������.
 * 
 * @author v.kulikov
 */
public class SNS_NEG_CommonFormatErrors extends RunTest {
	
	public dpat.tests.functional.sensors.TOset tosSNS;
	public dpat.tests.functional.alarm.TOset tosAlarm;
	
	ArrayList<Sensors> availableSensors;

	@Override
	public void initTOsets() throws Exception {
		/*  ������������� ��������	 */
		fillAllowedAppList( RoleIDs.appVisual1, new ApplicationTypes[] {ApplicationTypes.Visual} );
		tosSNS = new dpat.tests.functional.sensors.TOset(RoleIDs.appVisual1);
		tosAlarm = new dpat.tests.functional.alarm.TOset(RoleIDs.appVisual1);
	}
	
	@Override
	public void preconditions() throws Exception {
		/// ����� DP ������ ���� ������� ��� ���������� ���� �������� ������ ����� - RunTest.
		
		/** 
		 * ���������� ������ ��������, ������������� � ������� ������������. 
		 * ��� ������� ����� ����� �������� ��� ��� �� ����� ��������� � ������������� ���������� ������������ ���� SkipException, 
		 * ���������� ���������� ���������, � ���� � ������ ����� ������� ��� Skipped.
		 **/
		availableSensors = Sensors.checkSensorsInCurrentConfiguration();
		Do.debug("Available sensors in current configuration: " +  availableSensors);
		
		/// �������� Gyro � Wind � ������.
		tosModel.switchSensorsOrREFsGroupON(Sensors.Gyro);
		tosModel.switchSensorsOrREFsGroupON(Sensors.Wind);
		
		/// ���� Gyro ������ ���� ����������� �������.
		tosSNS.setNsensorsInGroup(Sensors.Gyro, SensorREFModes.Select, 1, false, true);
		
		/// ������ ������� ���������� ��������� �� ��������. ����������, �.�. �� ������ ������� ������ Gyro
		Collections.reverse(availableSensors);
		Do.debug("Reverse collection: " + availableSensors);
				
		/** �� ����� (�����) ������ ���������� ������������� ����� �.�. ������� Sensors 
		  * �� ������ (������) ������� ���� Visual � DP ����������� Alarm
		  */
		ArrayList<PanelSwitcher> pSW = PanelSwitcher.createAllpsws(getAllowedAppList().keySet());
		for( PanelSwitcher each : pSW ) each.bringPanelRight("Alarm");	
	}

	@Override
	public void scenario() throws Exception {
		/// ������� ������ ������ ��������� �������� � ���������:
		for(Sensors sens : availableSensors) {
			///> ��������� ��� ������ � ������ � ������ �������� � Visual.
			checkFormatError(sens, "Off");
			///> ���������� � ������ ������ Empty Data � ������ �������� � Visual
			checkFormatError(sens, Gyro_Faults.EmptyData.name());
			///> ���������� � ������ ������ Invalid Msg � ������ �������� � Visual
			checkFormatError(sens, Gyro_Faults.InvalidMsg.name());
		}
	}

	/**> ����� ��� �������� ��������� ����������� �������� ����� ��������� ���������� ������� � ������ ���� ���������� �������.
	*@param sens ������� ������ ��������(���).
	*@param error ��� ������ (�-�: Empty Data, Invalid Msg, Off).
	**/
	private void checkFormatError(Sensors sens, String error) {
		// false - ������� / true - ������ ��������� ������ ����������� ������ ��������. ��� Gyro ������������� �������� �� ��������� �������
		boolean isIndOfPnResGreen = false;
		
		/// ������������� ������� ������� ������ � ����� Select, ��� TS - UseSensor
		ArrayList<Integer> sensSel = new ArrayList<Integer>();
		if(sens.isModeSupported(SensorREFModes.Select)){
			sensSel = tosSNS.setWholeGroupFast(sens, SensorREFModes.Select, false);
			Do.debug("Switch sensors to mode Select");
		} else if(sens.isModeSupported(SensorREFModes.UseSensor)){
			sensSel = tosSNS.setWholeGroupFast(sens, SensorREFModes.UseSensor, false);
			Do.debug("Switch sensors to mode UseSensor");
		} else {
			Do.skip("Unrecognized type of sensor");
		}
		
		/** ����������, ���� �� ��������� ������� � ������� ������ (���� �� 1).
		  * ���� ��������� �������� ��� ������, ���-�� ����� ������������ ������� ������, ���� ��������� ��� �� ������.
		  * ��������� ����������� ����� ������������ ��� skipped � ��������� � ������ ��������.
		  */
		if(sensSel.size() < 1){ 
			Do.debug("Less than 1 sensor " + sens + " selected . Check mode of this sensor.");
			return;
		}
		
		/// ������� ������ ������� �������� ��� ������� ������(���� �������).
		SensorPanel[] groupSensors = tosSNS.snsesAll.get(sens);
		Do.debug("Get all panels for current group "+sens);
		
		/// ������� �� ����� ��������� ������ ������
		tosSNS.bringSensorPanel(groupSensors[groupSensors.length-1]);	
		
		/// ������������� ��������� ���� ������ ��������.
		for(int i=0;i<groupSensors.length;i++){
			groupSensors[i].button.mustBeUserVisible();
			Do.debug("Current sensor " + groupSensors[i].button.getCaption() + " in group " + sens + " is UserVisible.");
		}
		
		/// ������� ������ ������ � ��������� ��� ���������� � ����������� �� ������.
		for(int i=0; i<sensSel.size(); i++){
			SensorPanel sensor = tosSNS.snsesAll.get(sens)[i];
			
			boolean existInvMsgCount = false; // false - ���� InvalidMsgCounter = 0 / true InvalidMsgCounter != 0 ��� ���������������� �������
			if(error == "InvalidMsg"){
				int count = 0; // ���������� ��� �������� ���-�� ��������� ���������� ������� � ����� Simul.ini
				switch(sens) {
				case Gyro:
					count = RoleIDs.appVisual1.getIniFilesParameters().getSimulIniGyroInvalidMsgCounterMax(sensor.getNum());
					break;
				case Wind:
					count = RoleIDs.appVisual1.getIniFilesParameters().getSimulIniWindInvalidMsgCounterMax(sensor.getNum());
					break;
				case VRS:
					count = RoleIDs.appVisual1.getIniFilesParameters().getSimulIniVRSInvalidMsgCounterMax(sensor.getNum());
					break;
				case LS:
					count = RoleIDs.appVisual1.getIniFilesParameters().getSimulIniLSInvalidMsgCounterMax(sensor.getNum());
					break;
				case LOG:
					count = RoleIDs.appVisual1.getIniFilesParameters().getSimulIniLOGInvalidMsgCounterMax(sensor.getNum());
					break;
				case TS:
					count = RoleIDs.appVisual1.getIniFilesParameters().getSimulIniTSInvalidMsgCounterMax(sensor.getNum());
					break;
				default:
					break;
				}
				if(count != 0) existInvMsgCount = true;
				if(existInvMsgCount) isIndOfPnResGreen = true;
				Do.debug("Count InvMsg For "+sensor.button.getCaption()+" is "+count);
			}
			
			if(error != "Off"){
				// �������� ����� ������ ��� ������ � ������� SensorFault.
				SensorFaultsSet sensFaultSet = tosModel.sensorsFaults.getParamSet(sens, i+1);
				///- ���������� ��� ������� ������� � ������ ������.
				sensFaultSet.activate(error);
				Do.debug("Activated Error: "+error);
				///- �������� ��������� ������ Invalid Data/No Data Received
				checkAlarms(sensor, existInvMsgCount);	
			} else {
				///- ��������� ������� ������ � ����� Off � ������.
				tosModel.switchSensorOrRefOFF(sens, sensor.getNum());
			}
			
			Do.debug(sensor.button.getCaption() + " " + existInvMsgCount);
			
			if((error == "InvalidMsg") && (existInvMsgCount)){
				///- ������ �������� ������� ����-������.
				Do.check4actionTO(sensor.button, 
						"Failed for "+sensor.button.getCaption()+" - button is not LightGreen.", 
							new MethodSignature("isLightGreen"));
				Do.debug("Current button "+sensor.button.getCaption()+" is LightGreen.");
				///- ��������� ������� ������� - ������.
				Do.check4actionTO(sensor.dataFilter, 
						"Failed for "+sensor.button.getCaption()+" - Data Filter is not Green.", new MethodSignature("isGreenAny"));
				Do.debug("Current dataFilter of "+sensor.button.getCaption()+" is Green.");
				///- �������� ������(�����/��������)
				sensor.allDataIndicatorsMustBe(isIndOfPnResGreen);
			} else if (!existInvMsgCount) {
				if(sensor.sensorGroup.isModeSupported(SensorREFModes.UseSensor)){
					///- ������ �������� ������� ����-������.
					Do.check4actionTO(sensor.button, 
							"Failed for "+sensor.button.getCaption()+" - button is not LightGreen.",
								new MethodSignature("isLightGreen"));
					Do.debug("Current button "+sensor.button.getCaption()+" is LightGreen.");
				} else {
					///- ������ �������� ������� �������.
					Do.check4actionTO(sensor.button,
							"Failed for "+sensor.button.getCaption()+" - button is not Red.",
								new MethodSignature("isRed"));
					Do.debug("Current button "+sensor.button.getCaption()+" is Red.");
				}
				///- ��������� ������� ������� - ������� ("������������� ������").
				Do.check4actionTO(sensor.dataFilter,
						"Failed for "+sensor.button.getCaption()+" - Data Filter is not Red",
							new MethodSignature("isRedAny")); 
				Do.debug("Current Data Filter of "+sensor.button.getCaption()+" is Red.");
				///- ���������, ��� Data ---, Dev ----, Noise ----  ��������.
				sensor.allDataIndicatorsMustBe(false);
			}
		}
		
		/// ��������� ������ �����������.
		checkIndicatorOfPanelResult(sens, isIndOfPnResGreen);
		
		/// ��������� ������ ��� ������������ Gyro.
		if(sens.equals(Sensors.Gyro)){
			if(!isIndOfPnResGreen){
				tosAlarm.mustBeLastPresentWithReqStatus( "GYRO_FILTER_DEAD", "(?iu)heading\\s+dead\\s+reckoning", true, false);	
				
				tosAlarm.mustBeLastPresentWithReqStatus( "REF_WARNING_BAD_HEADING", 
						"(?iu)position\\s+and\\s+wind\\s+data\\s+unreliable:\\s+no\\s+heading\\s+data", true, false);	
				
				tosAlarm.mustBeLastPresentWithReqStatus( "REF_FILTER_DEAD", "(?iu)position\\s+dead\\s+reckoning", true, false);
			}
		} 
		
		/// ������ ����������� - �������� ������... TODO: ������� ������� � ���� ������...
		tosSNS.pnResAll.get(sens).mustBeDataPresentOnPanel();
		Do.debug(sens + " current processing panel result is Present.");
		
		/// ������� ����� � �������� ������ � ���� ���������� �������� �������� ������...
		if(error != "Off") tosModel.deactivateAllSensorsOrREFsGroupErrors(sens);
		
		/// �������� ��� ������ � ������ ������� � ON.
		tosModel.switchSensorsOrREFsGroupON(sens);
		
		tosSNS.setWholeGroupFast(sens, SensorREFModes.Select, false);
	}
	
	/** ����� ��� �������� ������� ��� ���������� �������.
	*@param sensor ������� ������(������).
	*@param existInvMsgCount true - ������� ����������(������ ���); false ����������� ��������.
	**/
	private void checkAlarms(SensorPanel sensor, boolean existInvMsgCount){
		Alarm alarm; // ���������� ��� �������� ���������� ������.
		/// �������� ��������� ������ �������:
		if(!sensor.sensorGroup.equals(Sensors.Wind)){
			if(!existInvMsgCount){
				Do.debug("Check alarm of " + sensor.button.getCaption().replaceAll("\\^", "-"));
				///- Alarm: Invalid Data + ���������� + ���������������.
				alarm = tosAlarm.mustBeLastPresentWithReqStatus(sensor.getNameForAlarmType() + "_FAULT",
						"(?iu)" + sensor.button.getCaption().replaceAll("\\^", "-") + ":\\s+invalid\\s+data$", true, false);			
				Do.debug(sensor.button.getCaption().replaceAll("\\^", "-")+" Invalid Data " + alarm.getTimeStart() + "| actual + not confirmed | => OK");
			}
		} else {
			///- Alarm: Sensor{Num}: No Data Received + ���������� + ���������������.
			Do.debug("Check alarm of " + sensor.button.getCaption().replaceAll("\\^", "-"));
			
			alarm = tosAlarm.mustBeLastPresentWithReqStatus(sensor.getNameForAlarmType() + "_NO_DATA",
					"(?iu)" + sensor.button.getCaption().replaceAll("\\^", "-") + ":\\s+no\\s+data\\s+received$", true, false);
			Do.debug(sensor.button.getCaption().replaceAll("\\^", "-")+" No Data Received:: " + alarm.getTimeStart() + "| actual + not confirmed | => OK");
			
			///- Alarm: Sensor{Num}: Invalid Data + ���������� + ���������������.
			Do.debug("Check alarm of " + sensor.button.getCaption().replaceAll("\\^", "-"));
			
			alarm = tosAlarm.mustBeLastPresentWithReqStatus(sensor.getNameForAlarmType()  + "_FAULT",
					"(?iu)" + sensor.button.getCaption().replaceAll("\\^", "-") + ":" + "\\s+invalid\\s+data$", true, false);
			Do.debug(sensor.button.getCaption().replaceAll("\\^", "-")+" Invalid Data " + alarm.getTimeStart() + "| actual + not confirmed | => OK");
				
			///- Alarm: Sensor{Num}: No Data Received + ������������ + ���������������.
			Do.debug("Check alarm of " + sensor.button.getCaption().replaceAll("\\^", "-"));
			
			alarm = tosAlarm.mustBeLastPresentWithReqStatus(sensor.getNameForAlarmType() + "_NO_DATA",
					"(?iu)" + sensor.button.getCaption().replaceAll("\\^", "-") + ":" + "\\s+no\\s+data\\s+received$", false, false);			
			Do.debug(sensor.sensorGroup+" No Data Received: " + alarm.getTimeStart() + "| not actual + not confirmed | => OK");
		}
	}

	/** ����� ��� �������� ���������� ������ �����������.
	*@param sens ������� ������ ��������(���).
	*@param existInvMsgCount � ������ ������������ ���� �� 1 ������ � ����������� ��������� ���������� � ��������.(true/false).
	**/
	private void checkIndicatorOfPanelResult(Sensors sens, boolean existInvMsgCount){
		/// ���������:
		switch(sens){
		case Gyro:
		case Wind:
		case VRS:
		case TS:
			if(existInvMsgCount){
				///- ��������� ������� ��������� ������ Results - ������ ("����������� ������").
				Do.check4actionTO(tosSNS.pnResAll.get(sens).dataProcInd, 
						"Failed for "+sens+" - processing filter is not Green.", 
							new MethodSignature("isGreenAny"));
				Do.debug(sens + " processing filter OK - green");
				break;
			} else {
				///- ��������� ������� ��������� ������ Results - ������� ("������������� ������").
				Do.check4actionTO(tosSNS.pnResAll.get(sens).dataProcInd, 
						"Failed for "+sens+" - processing filter is not Red",
							new MethodSignature("isRedAny"));
				Do.debug(sens + " processing filter OK - red");
				break;
			}
		case LOG:
			///- ��������� ������� ��������� ������ Results - ������ ("������ ������ �������� ��").
			Do.check4actionTO(tosSNS.pnResAll.get(sens).dataProcInd,
					"Failed for "+sens+" - processing filter is not Green",
						new MethodSignature("isGreenAny"));
			Do.debug(sens + " processing filter OK - green");
			break;
		default:
			///- �� ������������� ��������� � LS �.�. �� ����������� + ������ ���������� ��������� ������� � ����. ����������
			break;
		}
	}
	
	/// ���������� DP � �������� ���������
	@Override
	public void postconditions() throws Exception {
		///> �������� ��� ������� � ������.
		//tosModel.switchAllSensorsAndREFsON();
		///> ������������ ��� ������ � ������.
		//tosModel.deactivateAllSensorsAndREFsErrors();
		///> �������� ������� � Select.
		for (Sensors sens : availableSensors) {
			if(!sens.isModeSupported(SensorREFModes.Select)) continue;
		//	tosSNS.setWholeGroupFast(sens, SensorREFModes.Select, false);
		}
	}
}