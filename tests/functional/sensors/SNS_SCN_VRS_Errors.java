package dpat.tests.functional.sensors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dpat.coreLib.common.config.Sensors;
import dpat.coreLib.common.constants.ApplicationTypes;
import dpat.coreLib.common.constants.RoleIDs;
import dpat.coreLib.common.Do;
import dpat.coreLib.common.MethodSignature;
import dpat.coreLib.common.constants.SensorREFModes;
import dpat.coreLib.indicators.CompareDeviations;
import dpat.coreLib.indicators.NumberWithIndependentSignDataInd;
import dpat.coreLib.indicators.NumberWithIndependentUnitsModelEditField;
import dpat.coreLib.panels.PanelSwitcher;
import dpat.coreLib.panels.ParamPanel;
import dpat.coreLib.panels.SensorPanel;
import dpat.coreLib._model.motionSignals.ReinitParametersDialog;
import dpat.coreLib._model.sensorFaults.SensorFaultsSet;
import dpat.coreLib._model.sensorFaults.VRS_Faults;
import dpat.tests.RunTest;

/**
 * �����������:
 *  ��������� ������ VRS discrepancy ����� ����������� ������, 
 *  ��������� �������� �������������/��������������� ��������,
 *  ��������� �������� VRS(priority/not priority) ��� ������� ������ � �������� ������.
 *  ��������� ������� ��� ���������� ������������� �������� �������� �����(����).
 *  ������ � ����� MRU
 * @author v.kulikov
 */
public class SNS_SCN_VRS_Errors extends RunTest {
	
	/* �������� ������� ��� ��������, �������, ����� � �������. */
	public dpat.tests.functional.sensors.TOset tosSNS;
	public dpat.tests.functional.alarm.TOset tosAlarm;
	public dpat.tests.functional.Map.TOset tosMap;
	
	/* ��������� �������� ���������� ������ ��� ����������� � ������. */
	private final double JUMP_AR = 10;
	private final double JUMP_AP = -6.18;
	private final double DRIFT_WX = 1.0;
	private final double DRIFT_WY = 1.0;
	private final double NOISE = 5.0; 
	private final double SINGLE_A = 2.0;
	private final double SINGLE_T = 1.0;
	private final double DEV = 0.1;
	
	/* ��������� �������� ��������� ������� VRS ��� ����������� � �������. */
	private final String ZERO = "0.00";
	private final String TWO = "2.00";
	private final String THREE = "3.00"; // ����������� � ����������� ��������
	
	/* ��������� ��� ��������� ����������� ��� ��������.*/
	private final String PITCH = "Pitch.Title";
	private final String ROLL = "Roll.Title";
	private final String PITCH_VRS = "Pitch.VRS";
	private final String ROLL_VRS = "Roll.VRS";
	
	/* ������������ ���������� ������ ��� ����������� � Visual.*/
	private final String A = "A";
	private final String T = "T"; // ��� ��������� Noise
	private final String AP = "Ap";
	private final String AR = "Ar"; // ��� ��������� Jump
	private final String WX = "Wx";
	private final String WY = "Wy"; // ��� ��������� Drift
	
	@Override
	public void initTOsets() throws Exception {
		/*  ������������� ��������	 */
		fillAllowedAppList( RoleIDs.appVisual1, new ApplicationTypes[] {ApplicationTypes.Visual} );
		tosSNS = new dpat.tests.functional.sensors.TOset(RoleIDs.appVisual1);
		tosAlarm = new dpat.tests.functional.alarm.TOset(RoleIDs.appVisual1);
		tosMap = new dpat.tests.functional.Map.TOset(RoleIDs.appVisual1);
	}
	
	@Override
	public void preconditions() throws Exception {
		/// ����� DP ������ ���� ������� ��� ���������� ���� �������� ������ ����� - RunTest.
		
		/// ��������� ������� �������� VRS � ������������.
		Do.debug("Check that Sensors " + Sensors.VRS + " are in this configuration.");
		Do.dpAssert(Sensors.VRS.totalELTnum() > 0, "Sensors " + Sensors.VRS + " are not in this configuration.");
		
		/// � ������ �������� ������� �����/����������.
		tosModel.switchSensorsOrREFsGroupON(Sensors.VRS);
		
		/// � ������ �������������� ������� ����������(�����/�����/�������).
		tosModel.btMotionSignals.pressVW();
        tosModel.motionSignals.deactivateGeneralDisturbance();
        
        /// � ������ �������������� ��� ������.
        tosModel.btSensorFaults.pressVW();
        tosModel.deactivateAllSensorsOrREFsGroupErrors(Sensors.VRS);
		
		/** �� ����� (�����) ������ ���������� ������������� ����� �.�. ������� Sensors, 
		  * �� ������ (������) ������� ���� Visual � DP ����������� Alarm.
		  */
		ArrayList<PanelSwitcher> pSW = PanelSwitcher.createAllpsws(getAllowedAppList().keySet());
		for( PanelSwitcher each : pSW ) each.bringPanelRight("Alarm");
		
		tosSNS.snsNB.btAll.pressVW(); // ��������������, ��� ������� ���� All.
	}
	
	@Override
	public void scenario() throws Exception {
		/// ���������� ������� VRS � ����� Select(�������������, ��� ������������� ����������).
		tosSNS.setWholeGroupFast(Sensors.VRS, SensorREFModes.Select, false);
		Do.debug("Switch " + Sensors.VRS + " sensors to mode Select");
		
		/** ����������, ���� �� ��������� VRS (���� �� 2).
		  * ���� ��������� VRS ������ ����, ���-�� ����� ������������ ������� ������, ���� ��������� ��� �� ������.
		  * ��������� ����������� ����� ������������ ��� skipped � ��������� � ������ ��������.
		  */
		ArrayList<SensorPanel> listSensPanels = tosSNS.getTheSameSensorsInGroup(Sensors.VRS, new MethodSignature("modeIs", SensorREFModes.Select));
		
		SensorPanel[] availableSensPanels = listSensPanels.toArray(new SensorPanel[listSensPanels.size()]);
		
		if(availableSensPanels.length < 2) Do.skip("Less than 2 sensors " + Sensors.VRS + " selected . Check mode of this sensor.");
		
		/// ������� ���� ��������� ���������� �������� VRS.
		openCorrectionWindow();
		
		ParamPanel paramPnVRS = tosSNS.getPnVRSParam();
		
		/** ������� ����� setCorrectionsToZero(ParamPanel paramPnVRS) ��� ��������� ����� ������ ��������� ������� VRS. **/
		setCorrectionsToZero(paramPnVRS);
		
		/// ���������, ��� ����� ����� �������� ������ �������� VRS - ����.
		for(SensorPanel sensorPanel : availableSensPanels){
			for(String indicator : Arrays.asList(PITCH, ROLL)){
				Do.check4actionTO(sensorPanel.dataIndicators.get(indicator), "Data results of "
						   +sensorPanel.button.getCaption() + " not equal zero. Value is "+ sensorPanel.dataIndicators.get(indicator).getNumberVal(), 
						   		new MethodSignature("isEqualInRangeToValue", 0.0, 0.1, 0.0, 1, 0));				
			}
		}
			
		/// ���������� ������������ ������ ��� ���������.
		SensorPanel snsPriority = Do.sureFirstObject(availableSensPanels, 
						new MethodSignature(true, "modeFast", SensorREFModes.Select),  
						new MethodSignature(true, "isPriority"));
		snsPriority.mustBePriority();
		Do.debug("Current priority sensor is " + snsPriority.button.getCaption());
				
		/// ���������� �������������� ������ ��� ���������.
		SensorPanel snsNotPriority = Do.sureFirstObject(availableSensPanels, 
							new MethodSignature(true, "modeFast", SensorREFModes.Select),  
							new MethodSignature(false, "isPriority"));
		Do.debug("Sensor for comparison is " + snsNotPriority.button.getCaption());
			
		// ���������� ����� ���������� Sensor Faults ��� ������������� �������
		SensorFaultsSet snsPriorityModelFaults = tosModel.sensorsFaults.getParamSet(Sensors.VRS, snsPriority.getNum());

		String vrsFault = VRS_Faults.Noise.name();
		
		/// ������������� ������� ��������� Noise A=5
		snsPriorityModelFaults.setValue(vrsFault, A, NOISE); 
		snsPriorityModelFaults.activate(vrsFault);  /*------------------< ���������� Noise >------------------*/

		/// ����� �����: VRS Discrepancy
		tosAlarm.mustBeLastPresentWithReqStatus("VRS_SENSOR_DISCREPANCY", "(?iu)" + Sensors.VRS + "\\s+discrepancy", true, false);

		snsPriorityModelFaults.deactivate(vrsFault); /*----------------< ������������ Noise >----------------*/
		
		vrsFault = VRS_Faults.Single.name();  

		/// ��������� ������������� ������� Single A 2 T 1s
		snsPriorityModelFaults.setValue(vrsFault, A, SINGLE_A);
		snsPriorityModelFaults.setValue(vrsFault, T, SINGLE_T);
		snsPriorityModelFaults.activate(vrsFault); /*------------------< ���������� Single >------------------*/
		
		/**
		 * ������� ����� checkSingleData(SensorPanel snsPriority)
		 * ��� �������� ������ ������ Single. ��������� ������ �������� �������.
		 */
		checkSingleData(snsPriority);
				
		snsPriorityModelFaults.deactivate(vrsFault);   /*------------------< ������������ Single >------------------*/

		/// ��������� �������� VRSRollDiscrepancyMax � VRSPitchDiscrepancyMax ����� ���������� ������� ����� ���������� �����.
		double rollDiscrMax = RoleIDs.appVisual1.getIniFilesParameters().getSensorsIniVRSRollDiscrepancyMax(snsPriority.getNum())+DEV;
		double pitchDiscrMax = RoleIDs.appVisual1.getIniFilesParameters().getSensorsIniVRSPitchDiscrepancyMax(snsPriority.getNum())+DEV;
		
		/// ������������� ������� ��������� Jump Ar=rollDiscrMax/pitchDiscrMax - ������ �� ��������� �������� + 0.1 ������� - �������� �����(Roll)/������� �����(Pitch).
		for(String param : new String[] { AR, AP }){
			// ���������� ����������� �������� � ����������� �� ���������.
			double discrepancy = (AR.equals(param)) ? rollDiscrMax : pitchDiscrMax;
			/**
			 *> ������� ����� checkJump(SensorFaultsSet snsPriorityModelFaults, SensorPanel snsNotPriority, SensorPanel snsPriority, 
			 *> String param, Double discrepancy, VRS_Faults vf)
			 *> ��� �������� ������� ������ �������� ��� ��������� ��������� Jump.
			 */
			checkJump(snsPriorityModelFaults, snsNotPriority, snsPriority, param, discrepancy, VRS_Faults.Jump);
			tosSNS.returnPriority(snsPriority);
		}

		vrsFault = VRS_Faults.Jump.name();  
			
		/// ������������� ������� ��������� Jump Ar=10, Ap=-6.18
		snsPriorityModelFaults.setValue(vrsFault, AR, JUMP_AR); 
		snsPriorityModelFaults.setValue(vrsFault, AP, JUMP_AP);
		snsPriorityModelFaults.activate(vrsFault); /*------------------< ���������� Jump >------------------*/
		
		for(String indicator : Arrays.asList(ROLL, PITCH)){	
			Do.check4actionTO(snsNotPriority.dataIndicators.get(indicator), 
					"Deviation of not Priority - Priority sensors not equal for indicator " + indicator, 
						new MethodSignature("isEqualInRangeToIndValue", 
								snsPriority.dataIndicators.get(indicator), 0.2, ((ROLL.equals(indicator) ? -JUMP_AR : -JUMP_AP)), 1, 0));
		}
			
		/// ����� �����: VRS Discrepancy - ����������.
		tosAlarm.mustBeLastPresentWithReqStatus("VRS_SENSOR_DISCREPANCY", "(?iu)" + Sensors.VRS + "\\s+discrepancy", true, false);

		/// ��������� �������� ��� ��������� Jump, ����� ����������� �������� ������ ����������.
		double pitchMax = RoleIDs.appVisual1.getIniFilesParameters().getSensorsIniVRSPitchMax(snsPriority.getNum());
		snsPriorityModelFaults.setValue(vrsFault, AP, pitchMax + DEV);
			
		/// �������, ��� ����� ����������� � Visual ��� ���������� ���������� ��������� ���� ������� �����.
		snsPriority.modeMustBe(SensorREFModes.FaultSelect);
		
		/// ����������������� ��������� ������ �� �������������� ����� ������������� ������� � ���������� ���������� ��������.
		tosAlarm.mustBeLastPresentWithReqStatus("CHANGE_MAIN_VRS", "Main\\s+VRS\\s+Changed:\\s+" + snsPriority.button.getCaption() + 
			"\\s+\\->\\s+" + snsNotPriority.button.getCaption() + ":\\s+Auto$", false, false);	
		
		/// ����� �����: VRS�: no good data for a long time - ����������.
		tosAlarm.mustBeNotLastPresentWithReqStatus(snsPriority.getNameForAlarmType() + "_NO_DATA",
				"(?iu)" + snsPriority.button.getCaption() + ":\\s+no\\s+good\\s+data\\s+for\\s+a\\s+long\\s+time", true, false);
		
		snsPriorityModelFaults.deactivate(vrsFault); /*------------------< ������������ Jump >------------------*/
	
		tosSNS.snsNB.btAll.pressVW(); // ��������������, ��� ������� ���� All.
		
		tosSNS.returnPriority(snsPriority); // ��������������� ��������������
				
		for(String param : Arrays.asList(WX, WY)){
			/**
			 * ���������� Drift Wx = 1 ���� Wy = 1 ������������� �������
			 * > ������� ����� checkDrift(SensorFaultsSet snsPriorityModelFaults, SensorPanel snsPriority, String param, Double val, VRS_Faults vf)
			 * > ��� �������� ������� ������ �������� ��� ������� ������ Drift.
			 */
			checkDrift(snsPriorityModelFaults, snsPriority, param, ((WX.equals(param)) ? DRIFT_WX : DRIFT_WY), VRS_Faults.Drift);
		}
		
		openCorrectionWindow();
					
		/// VRS Parameters: Correction - ��������� �������� ������������� �������.
		paramPnVRS.getDialogEditParameter(PITCH_VRS+snsPriority.getNum()).setCurVal(THREE);
		paramPnVRS.getDialogEditParameter(ROLL_VRS+snsPriority.getNum()).setCurVal(TWO);
		paramPnVRS.pressApply();
		Do.debug("Set Pitch = "+THREE+" and Roll = "+TWO+" (Corrections of priority sensor).");
		
		/// �������� Raw/Used Data � ����� ���� 15,16.
		String[] state2switch = {"RawData", "UsedData"};
		
		for (String state : state2switch){
			/**
			 * > ������� ����� checkRawUsedData(SensorPanel snsPriority, List<String> indicators, String state)
			 * > ��� �������� �������� ������ ������������� ������� VRS � ������� Raw/Used Data.
			 */
			checkRawUsedData(snsPriority, Arrays.asList(PITCH, ROLL), state); Do.debug("state : " + state);
		}
		
		tosSNS.snsNB.btAll.pressVW(); // ������� �� ����� �������� ������ ��������.(All).
		
		/**
		 * Sensor MRU - ���������� ������������ � ������ �������, ����� ���������� ������������� �������(�� MRU).
		 * ������� ����� areThereSensorsWithTypeMRU(SensorPanel[] availableSensPanels)
		 * ��� �������� ������� ������ �������� VRS �� ������� �������/�������� ���� MRU � �������� �������������� � MRU
		 */
		SensorPanel snsMRU = areThereSensorsWithTypeMRU(availableSensPanels);
		
		/// ������� ���� ��������� ������� VRS.
		openCorrectionWindow();
		
		/**
		 * ������� ����� setCorrectionsToZero(ParamPanel paramPnVRS)
		 * ��� ��������� ���� ����� ������ ��������� ������� VRS � ����.
		 */
		setCorrectionsToZero(paramPnVRS);
		
		tosSNS.snsNB.btAll.pressVW(); // ������� �� ����� ������ All.
		
		/**
		 * ������� ����� checkSignPitchRoll(SensorPanel snsPriority)
		 * ��� �������� �������� ������ ������������� ������� VRS � ������� Raw/Used Data.
		 */
		checkSignPitchRoll(snsPriority);
		
		if(null!=snsMRU){
			snsMRU.setMode(SensorREFModes.NotUse);
			tosModel.switchSensorOrRefOFF(snsMRU.sensorGroup, snsMRU.getNum());
			snsMRU.modeMustBe(SensorREFModes.NotUse);
			Do.debug("Switch snsMRU to mode NotUse");
			snsMRU.setMode(SensorREFModes.Select);
			snsMRU.modeMustBe(SensorREFModes.FaultSelect);	
			// ����� ���������� 7-8 ������� MRU ��������� ������, ��������� ���������� � ���������, ����� ����� �� ���������.
			tosAlarm.mustBeNotLastPresentWithReqStatus("VRS_SENSOR_DISCREPANCY", "(?iu)" + Sensors.VRS + "\\s+discrepancy", true, false);
		} else {
			this.addWarn2report(Sensors.VRS + " with type MRU not found. Check Sensors.ini");
		}
		
		/// ������������ ������ � ������.
		tosModel.btMotionSignals.pressVW();
		tosModel.motionSignals.deactivateGeneralDisturbance();
	}
	
	/**
	 * ������� ���� ��������� ������� VRS, ��������� ���������� ������������ ������� Param -> Graph.
	 */
	private void openCorrectionWindow(){
		///> ������� ���� ��������� ���������� �������� VRS.
		tosSNS.snsNB.btAll.pressVW();
		tosSNS.snsNB.btVRS.pressVW();
			
		///> ���������� ���� VRS Parameters(Correction). ���������, ��� ������������ Param(�� ���������) ����� Graph
		Do.dpAssert(tosSNS.snsNB.btParamGraph.getCaption().equalsIgnoreCase("param"), "Must be activated button Param");
		tosSNS.snsNB.btParamGraph.pressVW();
		Do.dpAssert(tosSNS.snsNB.btParamGraph.getCaption().equalsIgnoreCase("graph"), "Must be activated button Graph");
	}
	
	/**
	 * ����� ��� �������� ������� ������ �������� VRS �� ������� �������/�������� ���� MRU.
	 * @param availableSensPanels ������ ��������(������), ������� ���������� � ����� Select.
	 */
	private SensorPanel areThereSensorsWithTypeMRU(SensorPanel[] availableSensPanels){
		int count = 0; // ���������� �������� VRS � Physical Type = MRU
		boolean exist = false; // ������ ��� ���� MRU ����������.(���� ������ ���� �����)
		
		SensorPanel snsMRU = null;
		SensorPanel snsNotMRU = null;
		
		for(int i=0; i<availableSensPanels.length; i++){
			int num = availableSensPanels[i].getNum();
			String type = RoleIDs.appVisual1.getIniFilesParameters().getSensorsIniVRSPhysicalType(num);
			
			if("MRU".equals(type)){
				count++; snsMRU = availableSensPanels[i];
				Do.debug("Current sensor is " + snsMRU.button.getCaption() + " with type MRU.");
			} else {
				if(exist) continue;
				snsNotMRU = availableSensPanels[i];
				exist = true;
			}
		}
		
		/// ���� �������� MRU ��� ��� ������ ������, �� �������� ����������� (��� ������ � ������ ������).
		if(count != 1) return null;
		
		/// ���� ������ � ����� MRU - ������������, �� ���������� �������������� ��������.
		if(snsMRU.isPriority()) tosSNS.returnPriority(snsNotMRU);
		
		tosModel.switchSensorOrRefOFF(snsNotMRU.sensorGroup, snsNotMRU.getNum());
		snsNotMRU.modeMustBe(SensorREFModes.FaultSelect);
		
		snsMRU.mustBePriority();
	
		tosModel.switchSensorOrRefON(snsNotMRU.sensorGroup, snsNotMRU.getNum());
		snsNotMRU.setMode(SensorREFModes.Select);
		snsNotMRU.modeMustBe(SensorREFModes.Select);
		
		return snsMRU;
	}
	
	/**
	 * ����� ��� ��������� ���� ����� ������ ��������� ������� VRS � ����.
	 * @param paramPnVRS ������ ����������(Correction) ������� VRS.
	 */
	private void setCorrectionsToZero(ParamPanel paramPnVRS){
		///> �������� �������� ��������� ��� ������� VRS.
		for(String title : paramPnVRS.getDialogEditParametersKeySet()) paramPnVRS.getDialogEditParameter(title).setCurVal(ZERO);
		paramPnVRS.pressApply();
		Do.debug("Aligned all fields of VRS Correction to zero.");
	}
	
	/**
	 * ����� ��� �������� ������� ������ �������� ��� ������� ������ Drift
	 * @param snsPriorityModelFaults ����� ���������� ��� ������ � �������� ������������� �������.
	 * @param snsPriority ������������ ������(������ SensorPanel)
	 * @param param �������� ������ (Wx/Wy)
	 * @param valDrift �������� ������ (Wx/Wy)
	 * @param vf �������� ������, ���������� � VRS_Faults
	 */
	private void checkDrift(SensorFaultsSet snsPriorityModelFaults,
							SensorPanel snsPriority,
			   				String param, 
			   				Double valDrift,
			   				VRS_Faults vf)
	{
		// ���������� �������� Drift, �������� ����������� ��������.
		snsPriorityModelFaults.setValue(vf.name(), param, valDrift);
		snsPriorityModelFaults.activate(vf.name());
		
		double max = 0; // ��������, ��� ���������� �������� ������ ����� ����������� � FaultSelect
		String indTitle = ""; // ����-��������� ��� ��������� ���������� ��� �������.
		
		if(WX.equals(param)){ max = RoleIDs.appVisual1.getIniFilesParameters().getSensorsIniVRSRollMax(snsPriority.getNum()); indTitle = ROLL; }
		else if(WY.equals(param)) { max = RoleIDs.appVisual1.getIniFilesParameters().getSensorsIniVRSPitchMax(snsPriority.getNum()); indTitle = PITCH; }
		else { Do.stop("Unrecognized param : " + param); }
		
		///> ���, ���� Roll/Pitch ������ ������ Max. �� ����� ��������� ������������� ������� � ������ ����������� ���������.
		while( snsPriority.dataIndicators.get(indTitle).getNumberVal() < max - 0.5){
			Do.debug("NumInd = " + snsPriority.dataIndicators.get(indTitle).getNumberVal() + "; Max = " + max);
			Do.check4actionTO(snsPriority.dataIndicators.get(indTitle), 
				"Failed for Drift. NumIndPriority [" + snsPriority.dataIndicators.get(indTitle).getNumberVal() + "]" +
					" is Not Equal NumIndProcessedData [" + tosSNS.pnResAll.get(Sensors.VRS).processedData.get(indTitle).getNumberVal() + "].", 
						new MethodSignature("isEqualInRangeToIndValue", tosSNS.pnResAll.get(Sensors.VRS).processedData.get(indTitle), 0.5, 0.0, 1, 0));
		}
		
		///> �������, ��� ����� ����������� � Visual ��� ���������� ���������� ��������� ���� �������� �����.
		snsPriority.modeMustBe(SensorREFModes.FaultSelect);
					
		///> ����� �����: VRS�: No good data for a long time.
		tosAlarm.mustBeNotLastPresentWithReqStatus(snsPriority.getNameForAlarmType() + "_NO_DATA",
				"(?iu)" + snsPriority.button.getCaption() + ":\\s+no\\s+good\\s+data\\s+for\\s+a\\s+long\\s+time", true, false);
					
		snsPriorityModelFaults.deactivate(vf.name());
		
		///> ���������� �������������� �������.
		tosSNS.returnPriority(snsPriority);
	}
	
	/**
	 * ����� ��� �������� ���������� ������ �������, ����� ����������� ������ Jump,
	 * � ��� �� ���������/������ ������������ ������ ��� ����������� �������� ���� ������ ��� ������ �-��� ����� VRS discrepancy.
	 * @param snsPriorityModelFaults ����� ���������� ��� ������ � �������� ��������������� �������.
	 * @param snsNotPriority �������������� ������ (������ SensorPanel).
	 * @param snsPriority ������������ ������ (������ SensorPanel).
	 * @param param �������� ������(�-�: Ap, Ar).
	 * @param discrepancy ��������, ������� ���������� ���������� � ��������� �����������.
	 * @param vf �������� ������, ���������� � VRS_Faults
	 */
	private void checkJump(SensorFaultsSet snsPriorityModelFaults, 
						   SensorPanel snsNotPriority, 
						   SensorPanel snsPriority,
						   String param, 
						   Double discrepancy,
						   VRS_Faults vf){
		snsPriorityModelFaults.deactivateSensorError();
		snsPriorityModelFaults.setValue(vf.name(), param, discrepancy);
		snsPriorityModelFaults.activate(vf.name());
		
		String indTitle = (AR.equals(param)) ? ROLL : PITCH; // �������� ����� �������(�������� ���������).
		
		CompareDeviations cmpDev = new CompareDeviations(snsNotPriority.dataIndicators.get(indTitle), 
														 snsPriority.dataIndicators.get(indTitle),
														 0, // ��������� ���
														 1); // ��������
		double deviation = cmpDev.calculateDeviation(); // ������� ����� �����. � �������. �-�� = rollDiscrMax
		
		///> ��������� ���������� ������������� � ��������������� ��������.
		Do.check4actionTO(cmpDev, 
			"Deviation of not Priority - Priority sensors not equal " + deviation,
				new MethodSignature("isEqualInRangeToValue", -(discrepancy+DEV), 0.2, 0.0));
		Do.debug("Deviation of not Priority - Priority sensors equal " + deviation);
		
		///> ����� �����: VRS Discrepancy - ����������.
		tosAlarm.mustBeLastPresentWithReqStatus("VRS_SENSOR_DISCREPANCY", "(?iu)" + Sensors.VRS + "\\s+discrepancy", true, false);
		
		///> ���������� Jump Ap/Ar = 4.9
		snsPriorityModelFaults.setValue(vf.name(), param, discrepancy - 2 * DEV);
		
		///> ����� �����: VRS Discrepancy - ������������.
		tosAlarm.mustBeLastPresentWithReqStatus("VRS_SENSOR_DISCREPANCY", "(?iu)" + Sensors.VRS + "\\s+discrepancy", false, false);
		
		snsPriorityModelFaults.deactivate(vf.name());
	}
	
	/**
	 * ����� ��� �������� ������ ������ Single. ��������� ������ �������� �������.
	 * @param snsPriority ������������ ������ (������ SensorPanel)
	 */
	private void checkSingleData(SensorPanel snsPriority){
		///> ���, ���� � ������ ������������� ������� �� �������� ������ �� 2 �������. �-�: VRS Pitch 2.0; VRS Roll 2.0
		for(String ind : Arrays.asList(PITCH, ROLL)){
			Do.check4actionTO(snsPriority.dataIndicators.get(ind), new MethodSignature("isEqualInRangeToValue", 
					((snsPriority.dataIndicators.get(ind).isPositive()) ? SINGLE_A : -SINGLE_A), 0.1, 0.0, 1, 0));
		}
		
		///> ��������� 5 ���... ��������� ������ ������ �������� �������� ������� Single.
		for(int i=0; i!=5; i++){
			for(String ind : Arrays.asList(PITCH, ROLL)){
				Do.check4actionTO(snsPriority.dataIndicators.get(ind), new MethodSignature("isEqualInRangeToValue", 
						((snsPriority.dataIndicators.get(ind).isPositive()) ? SINGLE_A : -SINGLE_A), 0.1, 0.0, 1, 0));
			}
		}	
	}
	
	/**
	 * ����� ��� �������� �������� ������ ������������� ������� VRS � ������� Raw/Used Data.
	 * @param snsPriority ������������ ������ (������ SensorPanel)
	 * @param indicators ������ ������, ��� ��������� ����������� ��� ��������.
	 * @param state ����� Raw/Used Data
	 */
	private void checkRawUsedData(SensorPanel snsPriority, List<String> indicators, String state){
		///> �������� ��������� ����� ����������� ������
		if ("RawData".equals(state) && "Used data".equals(tosSNS.getLblRawUsedDataTitle().getVal())) tosSNS.switchRawUsedData();
		else if ("UsedData".equals(state) && "Raw data".equals(tosSNS.getLblRawUsedDataTitle().getVal())) tosSNS.switchRawUsedData();
		///> � ����������� �� ������(������������ ������) ��������� �������� ����������.
		if("Raw Data".equals(state)) checkRawData(snsPriority, indicators);
		else { areEquals(snsPriority, indicators); checkUsedData(snsPriority); }
	}
	
	/**
	 * ����� ��� �������� �� ��������� �������� ������������� ������� VRS � ������ �����������.
	 * @param snsPriority ������������ ������ (������ SensorPanel)
	 * @param indicators ������ ������, ��� ��������� ����������� ��� ��������.
	 */
	private void areEquals(SensorPanel snsPriority, List<String> indicators){
		///> ���������, ��� �������� ����������� ������������� ������� � ������ ����������� ���������.
		for(String ind : indicators){		
			Do.check4actionTO(snsPriority.dataIndicators.get(ind), 
				"Data of priority sensor and result panel is equals : value of Priority sensor " +
						snsPriority.dataIndicators.get(ind).getNumberVal() + " : value of pnResult : " + 
							tosSNS.pnResAll.get(Sensors.VRS).processedData.get(ind).getNumberVal(), 
			new MethodSignature("isEqualInRangeToValue", 
					tosSNS.pnResAll.get(Sensors.VRS).processedData.get(ind).getNumberVal(), 0.5, 0.0, 1, 0));
		}
	}
	
	/**
	 * ����� ��� �������� Raw Data ������������� ������� VRS �� ������� �������� ��������.
	 * @param snsPriority ������������ ������ (������ SensorPanel)
	 * @param indicators ������ ������, ��� ��������� ����������� ��� ��������.
	 */
	private void checkRawData(SensorPanel snsPriority, List<String> indicators){
		///> �������� ����������� � ����������� ��������� �������� ����� ��������� ��������� ��� ������ RawData Data.
		for(String ind : indicators){
			Do.check4actionTO(snsPriority.dataIndicators.get(ind), 
				"Invalid num for " + ind + ". It is not equal zero value : " + 
					"Data results of " + snsPriority.button.getCaption() + " is " + snsPriority.dataIndicators.get(ind).getNumberVal(),
						new MethodSignature("isEqualInRangeToValue", 0.0, 0.5, 0.0, 1, 0));
		}
	}
	
	/**
	 * ����� ��� �������� Used Data ������������� ������� VRS �� ������� �������� (PITCH - 3.0 | ROLL - 2.0)
	 * @param snsPriority ������������ ������ (������ SensorPanel)
	 */
	private void checkUsedData(SensorPanel snsPriority){
		///> �������� ����������� � ����������� ��������� �������� ��� ������ Used Data.
		Do.check4actionTO(snsPriority.dataIndicators.get(PITCH), 
			"Invalid num for Pitch. It is not equal " + THREE + " : " + snsPriority.dataIndicators.get(PITCH).getNumberVal(),
				new MethodSignature("isEqualInRangeToValue", Double.parseDouble(THREE), 0.1, 0.0, 1, 0));
		Do.debug("UsedData For Pitch is verified.");
		Do.check4actionTO(snsPriority.dataIndicators.get(ROLL), 
				"Invalid num for Roll. It is not equal " + TWO + " : " + snsPriority.dataIndicators.get(ROLL).getNumberVal(),
					new MethodSignature("isEqualInRangeToValue", Double.parseDouble(TWO), 0.1, 0.0, 1, 0));
		Do.debug("UsedData For Roll is verified.");
		
	}
	
	/** ����� ��� �������� Roll/Title ������������� ������� �� ����� ����� � ������������ Up/Down; STBD/Port; +-
	 * @param snsPriority ������������ ������ (������ SensorPanel)
	 */
	private void checkSignPitchRoll(SensorPanel snsPriority){
		/// ��������� Pitch/Roll �� ����� ����� � ������������ Up/Down; STBD/Port; +-.
		for(String ind : Arrays.asList(PITCH, ROLL)){
			///> ������� � ������ ���� MotionSignals.
			tosModel.btMotionSignals.pressAny();

			///> ��������� ����� 3 � � ����������� 90/180 ��������(��������������� ���� ���� �����).
			tosModel.motionSignals.setWave(3.0, Do.to_0_360(tosMap.getIndCurHeading().getNumberVal()) + ((ind.equals(PITCH)) ? 0.0 : 90.0));
			
			NumberWithIndependentSignDataInd signDataInd = // �������� ��������� � ��������� Up/Down - Pitch ; PORT/STBD - Roll.
					(NumberWithIndependentSignDataInd)(tosSNS.pnResAll.get(Sensors.VRS).processedData.get(ind));
			
			double angle = (ind.equals(PITCH)) ? 1.0 : 7.0;
			
			///> ��� ������� ������������� ��������, � ����� ��������� �����.
			Do.check4actionTO(signDataInd, new MethodSignature(true, "isPositive"));
			///> ���������� ������ ��������� 7..8 �������� ��� Roll � 0..1 ��� Pitch.
			Do.check4actionTO(snsPriority.dataIndicators.get(ind), "Data of " + snsPriority.dataIndicators.get(ind).getCaption() + 
				" VRS sensor is not Positive. Value is "+snsPriority.dataIndicators.get(ind).getNumberVal(),
						new MethodSignature("isEqualInRangeToValue", angle, 0.5, 0.0, 1, 0));
			///> ��� ������������� ��������, � ����� ��������� �����.
			Do.check4actionTO(signDataInd, new MethodSignature(true, "isNegative"));
			///> ���������� ������ ��������� -7..-8 �������� ��� Roll � -0..-1 ��� Pitch
			Do.check4actionTO(snsPriority.dataIndicators.get(ind), "Data of " + snsPriority.dataIndicators.get(ind).getCaption() + 
				" VRS sensor is not Negative. Value is " + snsPriority.dataIndicators.get(ind).getNumberVal(),
						new MethodSignature("isEqualInRangeToValue", -angle, 0.5, 0.0, 1, 0));
			
			///> ������������ ������� ���������� � ������.
			tosModel.motionSignals.deactivateGeneralDisturbance();
			
			///> �������� �������� � ReinitDialog ����� ��������� ���������� �����.
			ReinitParametersDialog reinit = tosModel.reinitParamsDialog;
			
			tosModel.btReinit.pressVW(); // ������� ������ ReinitParameters.
			reinit.mustBeOpen();
			
			NumberWithIndependentUnitsModelEditField[] numInds = // ������ ����� � ��������� ���������.
									{ reinit.getXg(), reinit.getYg(), reinit.getCourse(), reinit.getVelocity() };

			for(NumberWithIndependentUnitsModelEditField numInd : numInds)
			{
				reinit.setValueWithoutApplyChanges(numInd, 0.0);
			}
			
			reinit.pressOk(); // ������������ �������� ���������.
			reinit.mustBeClosed(); // ������ ������.
		}
	}
	
	/// ���������� DP � �������� ���������
	@Override
	public void postconditions() throws Exception {
		/// � ������ �������������� ��� ������� ���������� � ������.
		tosModel.btMotionSignals.pressVW();
		tosModel.motionSignals.deactivateGeneralDisturbance();
		tosModel.btSensorFaults.pressVW();
		tosModel.deactivateAllSensorsOrREFsGroupErrors(Sensors.VRS);
		
		/// � ������ �������� ������� VRS.
		tosModel.switchSensorsOrREFsGroupON(Sensors.VRS);
		
		/// � Visual �������� ������� ��� ������� VRS � Select.
		tosSNS.setWholeGroupFast(Sensors.VRS, SensorREFModes.Select, false);
		Do.debug("Set All " + Sensors.VRS + " Sensors to Select after test.");
	}
}