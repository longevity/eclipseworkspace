package dpat.tests.functional.sensors;

import java.util.ArrayList;

import dpat.tests.RunTest;
import dpat.tests.functional.alarm.TOset.Alarm;
import dpat.coreLib.common.config.Sensors;
import dpat.coreLib.common.constants.ApplicationTypes;
import dpat.coreLib.common.constants.RoleIDs;
import dpat.coreLib.common.constants.SensorREFModes;
import dpat.coreLib.common.Do;
import dpat.coreLib._model.sensorFaults.LOG_Faults;
import dpat.coreLib._model.sensorFaults.SensorFaultsSet;
import dpat.coreLib.panels.PanelSwitcher;
import dpat.coreLib.panels.SensorPanel;

/**
 * ��������� � Visual �� ����������� ��� ������� ������� ��� ��������� � ����������� ������ ��� ������� ������:
 * ����������� ����������� ��������� ������, �������� �, ����� ������ ������ ������������ ��� ������� ������� �� �����������.
 * - SNS_NEG_LogSpecificFormatErrors.java - ����������� ������ ������� LOG(OutOfLimit/FaultStatus).
 * - SNS_NEG_CommonFormatErrors.java - ��������� ���� �� ��������� ������ ��� �������� LOG.
 * - SNS_NEG_GyroSpecificFormatErrors.java - ����������� ������ ������� Gyro(NoCS/RepeatMsg/OutOfLimit).
 * 
 * @author v.kulikov
 */
public class SNS_NEG_LogSpecificFormatErrors extends RunTest {
	
	public dpat.tests.functional.sensors.TOset tosSNS;
	public dpat.tests.functional.alarm.TOset tosAlarm;
	
	@Override
	public void initTOsets() throws Exception {
		/*  ������������� ��������	 */
		fillAllowedAppList( RoleIDs.appVisual1, new ApplicationTypes[] {ApplicationTypes.Visual} );
		this.tosSNS = new dpat.tests.functional.sensors.TOset(RoleIDs.appVisual1);
		this.tosAlarm = new dpat.tests.functional.alarm.TOset(RoleIDs.appVisual1);
	}
	
	@Override
	public void preconditions() throws Exception {
		/// ����� DP ������ ���� ������� ��� ���������� ���� �������� ������ ����� - RunTest.
		
		Do.debug("Check that Sensors Gyro and LOG are in this configuration");
		Do.dpAssert(Sensors.Gyro.totalELTnum() > 0 && Sensors.LOG.totalELTnum() > 0, 
													"Sensors Gyro and LOG are not in this configuration");
		/// � ������ �������� ��� �������
		tosModel.switchAllSensorsAndREFsON();
		
		/// ���� Gyro ������ ���� ����������� �������.
		tosSNS.setNsensorsInGroup(Sensors.Gyro, SensorREFModes.Select, 1, false, true);
		
		tosModel.deactivateAllSensorsOrREFsGroupErrors(Sensors.LOG);
		
		/** �� ����� (�����) ������ ���������� ������������� ����� �.�. ������� Sensors; 
		  * �� ������ (������) ������� ���� Visual � DP ����������� Alarm
		  */
		ArrayList<PanelSwitcher> pSW = PanelSwitcher.createAllpsws(getAllowedAppList().keySet());
		for( PanelSwitcher each : pSW ) each.bringPanelRight("Alarm");
	}

	@Override
	public void scenario() throws Exception {							
		/// ���������� ������� ������� ������ � ����� Select(�������������, ��� ������������� ����������).
		ArrayList<Integer> logSel = tosSNS.setWholeGroupFast(Sensors.LOG, SensorREFModes.Select, false);
		Do.debug("Switch sensors LOG to mode Select");
				
		/** ����������, ���� �� ��������� LOG (���� �� 1).
		  * ���� ��������� LOG ��� ������, ���-�� ����� ������������ ������� ������, ���� ��������� ��� �� ������.
		  * ��������� ����������� ����� ������������ ��� skipped � ��������� � ������ ��������.
		  */
		if(logSel.size() < 1) Do.skip("Less than 1 sensor " + Sensors.LOG + " selected . Check mode of this sensor.");
		
		/// ������� ������� �� ������, ��������� ������� OutOfLimit/FaultStatus.
		for (int i=0; i<logSel.size(); i++){
			///> �������� ��������� �������(������), ����� �������� � ���.
			SensorPanel sensor = tosSNS.snsesAll.get(Sensors.LOG)[i];
			Do.debug("Check "+sensor.button.getCaption());
			
			///> ���������� ������������� ��������� ������� �������. (������)
			tosSNS.bringSensorPanel(sensor);
			
			///> �������� ��������� ��������� VDVHW/VDVBW
			LOG_Faults[] protocols = { LOG_Faults.VDVBW, LOG_Faults.VDVHW };
			for(LOG_Faults proto :  protocols)
			{
				/// > ��������� ������ �� ������� ����������� ��������� ������.
				for(LOG_Faults logFault : LOG_Faults.values()){
					if(LOG_Faults.OutOfLimit.equals(logFault) || LOG_Faults.FaultStatus.equals(logFault)){
						/**
	    				 * - ������� ����� checkLOGSpecificFormatError(SensorPanel sensor, LOG_Faults logFault)
	    				 * - ��� �������� ��������� �������� ������� LOG ����� ����������� OutOfLimit, FaultStatus.
	    				 */
						checkLOGSpecificFormatError(sensor, logFault, proto);
						Do.pause(5);
					}
				}
			}
		}
	}
	
	/** ����� ��� �������� ��������� ������� {������, ��������� �������, ������, ����������� ������}
	*@param sensor ������� ������(������).
	*@param logFault ��� ������ (�-�: OutOfLimit, FaultStatus).
	*@param proto ��� ��������� (�-�: VDVHW, VDVBW...).
	**/
	private void checkLOGSpecificFormatError(SensorPanel sensor, LOG_Faults logFault, LOG_Faults proto) {
		Do.debug("Check Fault : " + logFault.name());

		// �������� ����� ���������� ��� ������ � �������� � ������.
		SensorFaultsSet sensFaultSet = tosModel.sensorsFaults.getParamSet(sensor.sensorGroup, sensor.getNum());
		
		///> ���������� ������ OutOfLimit/FaultStatus.
		sensFaultSet.activate(logFault.name());
		Do.debug("Check Specific Format Error: " + logFault.name()); 

		///> ���������� ������ �������� VDVBW/VDVHW.
		sensFaultSet.activateProtocol(sensor.sensorGroup, proto);
		
		/**
		 * > ������� ����� checkSensor(SensorPanel sensor, LOG_Faults logFault, boolean state)
		 * > ��� �������� ��������� �������� ������� LOG.
		 */
		checkSensor(sensor, logFault, proto, false);
	
		///> ������������ ������ OutOfLimit/FaultStatus.
		sensFaultSet.deactivate(logFault.name());
		
		///> ������������� ������ � ����� - Select.
		sensor.setMode(SensorREFModes.Select);
		sensor.modeMustBe(SensorREFModes.Select);
	}
	
	/** ����� ��� �������� ������� {������, ��������� �������, ������, ������ ������������/�����������}
	*@param sensor ������� ������(������).
	*@param logFault ��� ������ (�-�: OutOfLimit, FaultStatus...).
	*@param proto ��� ��������� (�-�: VDVHW, VDVBW...).
	*@param state (Present/Absent - ������ ������������/�����������).
	**/
	private void checkSensor(SensorPanel sensor, LOG_Faults logFault, LOG_Faults proto, boolean state){
		///> ���� ���� FaultStatus - ��������� ������� Select (LOG). ������, ����������, ��� - �������� ��������.
		if(LOG_Faults.FaultStatus.equals(logFault) && LOG_Faults.VDVHW.equals(proto))
		{
			sensor.modeMustBe(SensorREFModes.Select);
			sensor.allDataIndicatorsMustBe(true);
			return;
		}
		
		///> ��������� ������� FaultSelect (LOG).
		sensor.modeMustBe(SensorREFModes.FaultSelect);
		Do.debug("Filter of sensor " + sensor.button.getCaption() + " isRed.");

		///> Alarm: Sensor{Num}: No Data Received + ���������� + ���������������.
		Alarm alarm = tosAlarm.mustBeLastPresentWithReqStatus(sensor.getNameForAlarmType() + "_NO_DATA",
				"(?iu)" + sensor.button.getCaption().replaceAll("\\^", "-") + ":" + "\\s+no\\s+data\\s+received$", true, false);			
		Do.debug(sensor.button.getCaption().replaceAll("\\^", "-")+" No Data Received : " + alarm.getTimeStart() + "| actual + not confirmed | => OK");
		
		///> Alarm: Sensor{Num}: Invalid Data + ���������� + ���������������.
		Do.debug("Check alarm of " + sensor.button.getCaption().replaceAll("\\^", "-"));
		
		alarm = tosAlarm.mustBeLastPresentWithReqStatus(sensor.getNameForAlarmType()  + "_FAULT",
				"(?iu)" + sensor.button.getCaption().replaceAll("\\^", "-") + ":" + "\\s+invalid\\s+data$", true, false);		
		Do.debug(sensor.button.getCaption().replaceAll("\\^", "-")+" Invalid Data " + alarm.getTimeStart() + "| actual + not confirmed | => OK");
			
		///> Alarm: Sensor{Num}: No Data Received + ������������ + ���������������.
		Do.debug("Check alarm of " + sensor.button.getCaption().replaceAll("\\^", "-"));
	
		alarm = tosAlarm.mustBeLastPresentWithReqStatus(sensor.getNameForAlarmType() + "_NO_DATA",
				"(?iu)" + sensor.button.getCaption().replaceAll("\\^", "-") + ":" + "\\s+no\\s+data\\s+received$", false, false);			
		Do.debug(sensor.sensorGroup+" No Data Received: " + alarm.getTimeStart() + "| not actual + not confirmed | => OK");
		
		///> ������, ����������, ��� - ��������.
		sensor.allDataIndicatorsMustBe(state);
		
		///> ������ ����������� - �������� ������.
		tosSNS.pnResAll.get(Sensors.LOG).dataProcInd.mustBeGreen();
		tosSNS.pnResAll.get(Sensors.LOG).mustBeDataPresentOnPanel();
	}
	
	/// ���������� DP � �������� ���������
	@Override
	public void postconditions() throws Exception {
		/// � ������ �������� ��� �������.
		tosModel.switchSensorsOrREFsGroupON(Sensors.LOG);
		
		/// � Visual �������� ������� ��� ������� LOG � Select.
		Do.debug("Set All Sensors LOG to Select after test.");

		tosModel.deactivateAllSensorsOrREFsGroupErrors(Sensors.LOG);
		tosSNS.setWholeGroupFast(Sensors.LOG, SensorREFModes.Select, false);
	}
}