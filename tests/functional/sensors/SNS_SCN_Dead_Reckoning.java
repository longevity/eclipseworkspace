package dpat.tests.functional.sensors;

import java.util.ArrayList;

import dpat.coreLib.common.Do;
import dpat.coreLib.common.MethodSignature;
import dpat.coreLib.common.config.REFs;
import dpat.coreLib.common.config.Sensors;
import dpat.coreLib.common.constants.ApplicationTypes;
import dpat.coreLib.common.constants.Modes.PosModes;
import dpat.coreLib.common.constants.RoleIDs;
import dpat.coreLib.common.constants.SensorREFModes;
import dpat.coreLib.common.constants.Modes.HdgModes;
import dpat.coreLib.common.constants.Time;
import dpat.coreLib.indicators.ProcessingFilterIndicator;
import dpat.coreLib.panels.PanelSwitcher;
import dpat.coreLib.panels.REFPanel;
import dpat.coreLib.panels.SensorPanel;
import dpat.coreLib.popUp.SetCrabAngleSystemDialog;
import dpat.coreLib.popUp.SetHeadingSystemDialog;
import dpat.coreLib.popUp.SetTrackPositionSystemDialog;
import dpat.tests.RunTest;

/**
 * �������� ��������� ������� Heading dead reckoning � Heading jump detected
 * ��������� ������������� ������� : Button, Indicators, Results
 * ����� ���������� � ������.
 * @author v.kulikov
 */
public class SNS_SCN_Dead_Reckoning extends RunTest {
	public dpat.tests.functional.alarm.TOset tosAlarm;
	public dpat.tests.functional.sensors.TOset tosSNS;
	public dpat.tests.functional.REF.TOset tosREF;
	public dpat.tests.functional.Map.TOset tosMAP;
	public dpat.tests.functional.MFpanel.TOset tosMFP;
	public dpat.tests.functional.parameters.TOset tosParam;
	
	/* ������������ ��� ��������� ����������� ��� �������� */
	private final String HDG = "HDG.Title";
	private final String BIAS = "Bias.Title";
	
	ArrayList<PanelSwitcher> pSW;

	@Override
	public void initTOsets() throws Exception {
		/*  ������������� ��������	 */
		fillAllowedAppList( RoleIDs.appVisual1, new ApplicationTypes[] {ApplicationTypes.Visual} );
		tosSNS = new dpat.tests.functional.sensors.TOset(RoleIDs.appVisual1);
		tosREF = new dpat.tests.functional.REF.TOset(RoleIDs.appVisual1);
		tosMAP = new dpat.tests.functional.Map.TOset(RoleIDs.appVisual1);
		tosMFP = new dpat.tests.functional.MFpanel.TOset(RoleIDs.appVisual1);
		tosAlarm = new dpat.tests.functional.alarm.TOset(RoleIDs.appVisual1);
		tosParam = new dpat.tests.functional.parameters.TOset(RoleIDs.appVisual1);
	}
	
	@Override
	public void preconditions() throws Exception {
		/// ����� DP ������ ���� ������� ��� ���������� ���� �������� ������ ����� - RunTest.
		
		/// Dead Reckoning - �����, ����� ������� �������������� ���������������.
		int deadReck = RoleIDs.appVisual1.getIniFilesParameters().getSensorsIniFilterGyroDeadReckoningTimeout();
		if (deadReck!=1){
			//RoleIDs.appVisual1.getIniFilesParameters().setSensorsIniFilterGyroDeadReckoningTimeout(1);
			//RoleIDs.appVisual1.restartSystem();
		}
		
		tosModel.switchSensorsOrREFsGroupON(Sensors.Gyro); /// �������� ������� � ������.
				
		/** �� ����� (�����) ������ ���������� ������������� ����� �.�. ������� Sensors 
		  * �� ������ (������) ������� ���� Visual � DP ����������� Map
		  */
		pSW = PanelSwitcher.createAllpsws(getAllowedAppList().keySet());
		for( PanelSwitcher each : pSW ) each.bringPanelRight("Map");		
	}

	@Override
	public void scenario() throws Exception {
		/// ���������� ������� Gyro � ����� Select(�������������, ��� ������������� ����������).
		ArrayList<Integer> gyroSel = tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false);
		Do.debug("Switch sensors to mode Select");
		
		/**> ����������, ���� �� ��������� Gyro (���� �� 1).
		  *> ���� ��������� Gyro ���, ���-�� ����� ������������ ������� ������, ���� ��������� ��� �� ������.
		  *> ��������� ����������� ����� ������������ ��� skipped � ��������� � ������ ��������.
		  */	
		if(gyroSel.size() < 1) Do.skip("Less than 1 sensor " + Sensors.Gyro + " selected . Check mode of this sensor.");
		
		tosModel.btReinit.pressVW(); /// ���������� ���������� ���� ����� � ������ � �������.
		tosModel.reinitParamsDialog.setValueWithApplyChanges(tosModel.reinitParamsDialog.getCourse(), 
					Do.to_0_360(tosMAP.getIndCurHeading().getNumberVal()));
		
		tosSNS.snsNB.btAll.pressVW(); // ����������� ������� �� ������ All, ��� ����������� ������ Details ��� ��������.
		
		SensorPanel[] availableSensPanels = new SensorPanel[gyroSel.size()];
		
		for(int i=0; i<gyroSel.size(); i++) availableSensPanels[i] = tosSNS.snsesAll.get(Sensors.Gyro)[i]; 
		
		/// ���������� ������������ ������ ��� ������������ ���������� � ��������� � ������� �����������.
		SensorPanel snsPriority = Do.sureFirstObject(availableSensPanels, 
						new MethodSignature(true, "modeFast", SensorREFModes.Select),  
						new MethodSignature(true, "isPriority"));
		snsPriority.mustBePriority(); Do.debug("Current priority sensor is " + snsPriority.button.getCaption());
		
		double[] angles = { 30.0, -30.0 }; /// ��������� ���������� ���� �� 30 �������� ������/������ �������� �����.
		
		for(double angle : angles){
			if(isSemiAutomatic){ // ���� ���� ������������������, �� �������� ���������� ��� ����� TRACK.
				Do.debug("Check steps 22 .. 38 for part with Track.");
				
				/// ��������� Track ��� ����������� �������� � �������� �������� ����������.
				for( PanelSwitcher each : pSW ) each.bringPanelRight("Track");
				
				Do.debug("Add Route -> Edit Route -> Add Point 2 times - Edit Point. Set the value in the upper right button REL\n" + 
						 "Set Surge 500 meters -> Ok -> Apply -> Ok -> Set Active.", Do.Level.Request);
				
				/// Set POS and HDG LS Track Active
				tosMFP.setPosMode(PosModes.POS_TRACK);
				tosMFP.mustBeActivePosMode(PosModes.POS_TRACK);
				
				tosMFP.setHdgMode(HdgModes.HDG_TRACK);
				tosMFP.mustBeActiveHdgMode(HdgModes.HDG_TRACK);
			}
			
			/// ��������� ��� ����������� � ������
			tosModel.btSensorFaults.pressAny(); // step : 4
			tosModel.switchSensorsOrREFsGroupOFF(Sensors.Gyro);
			
			snsPriority.modeMustBe(SensorREFModes.FaultSelect);
			
			/// ��������� ��������� ������ Heading dead reckoning.
			tosAlarm.mustBeLastPresentWithReqStatus("GYRO_FILTER_DEAD", "(?iu)heading\\s+dead\\s+reckoning", true, false);
			
			/// �������� ���� ����� �� 30� ������ ���� ������ ��������.
			tosModel.btReinit.pressVW();
			double course = Do.to_0_360(tosMAP.getIndCurHeading().getNumberVal() + angle);
			tosModel.reinitParamsDialog.setValueWithApplyChanges(tosModel.reinitParamsDialog.getCourse(), course);
			
			/// �������� ������������ ������ � Model � Visual.
			tosModel.switchSensorOrRefON(Sensors.Gyro, snsPriority.getNum());
			
			// ���������, ��� ��� ��������� ��������� ������������� �-�� �������������� ������ ����������� �� �������� angle
			ProcessingFilterIndicator procFilterInd = // ���� ��������� �����, �.�. ����� Dead Reckoning, �� ��. ����
					tosSNS.pnResAll.get(snsPriority.sensorGroup).dataProcInd;
			if(!procFilterInd.isGrayVisible()){
				Do.dpAssert(snsPriority.dataIndicators.get(HDG).isEqualInRangeToIndValue(
						tosSNS.pnResAll.get(snsPriority.sensorGroup).processedData.get(HDG), 0.5, angle, 1, 360),
						"Data of priority Sensor is not equal data of pnResult with added angle");
			} else { this.addWarn2report("Check key Dead Reckoning Timeout in file Sensors.ini"); }
			
			/// ��������� ��������� ��������� ������: ������, ���������, ������� - Select.
			snsPriority.setMode(SensorREFModes.Select); snsPriority.modeMustBe(SensorREFModes.Select);
		
			/// ��������� ��������� ������ Heading jump detected.
			tosAlarm.mustBeLastPresentWithReqStatus("HDG_JUMP", "(?iu)^heading\\s+jump\\s+detected:\\s+" + "(\\d{2}.\\d||\\d{2})" + "\u00B0\\s+" +
						((course - tosMAP.getIndCurHeading().getNumberVal()) >= 0.0 ? "STBD" : "PORT") + "$", false, true);
			
			/// ��������� ������� Gyro ������ ��������� ����� � ������ ����������� �� ������������ �������� angle.
			Do.dpAssert(snsPriority.dataIndicators.get(HDG).isEqualInRangeToIndValue(
					tosSNS.pnResAll.get(snsPriority.sensorGroup).processedData.get(HDG), 0.5, 
				   -tosSNS.pnResAll.get(snsPriority.sensorGroup).processedData.get(BIAS).getNumberVal(), 1, 360),
					"Data of priority Sensor is not equal data of pnResult with added bias");
			
			Do.dpAssert(snsPriority.dataIndicators.get(HDG).isEqualInRangeToIndValue(
					tosMAP.getIndCurHeading(), 0.5, 
				   -tosSNS.pnResAll.get(snsPriority.sensorGroup).processedData.get(BIAS).getNumberVal(), 1, 360),
					"Data of priority Sensor is not equal current course with added bias");
			
			/// �������� �������
			SetHeadingSystemDialog setHeadingDialog = tosSNS.pnResAll.get(Sensors.Gyro).getDlgSystemSetHeading();
			SetCrabAngleSystemDialog setCrabAngleSystemDialog = tosSNS.pnResAll.get(Sensors.Gyro).getDlgSystemSetCrabAngle();
			SetTrackPositionSystemDialog setPosSysDlgTrack = tosREF.pnREFProcResults.getDlgSystemSetTrackPosition();	
			
			if(isSemiAutomatic) tosMAP.getIndCurHeading().rmbVal();
			
			/// �������� ��������� ������� Do you want to hold current heading or use previous � ����������� ��������.
			if(setHeadingDialog.isOpen() || setCrabAngleSystemDialog.isOpen()) {
				if(angle == 30.0) // ���� ����������� �� 30�, �� �������� HOLD(����� ��� �����)
					if(isSemiAutomatic) setCrabAngleSystemDialog.pressButton2Cancel(); // Previous
					else setHeadingDialog.pressButton1OK();		
				else if(angle == -30.0) // ���� ��������� �� 30�, �� �������� Previous(����� ��� �����)
					if(isSemiAutomatic) setCrabAngleSystemDialog.pressButton1OK(); // HOLD
					else setHeadingDialog.pressButton2Cancel();	
			}
			
			/**
			 * ������� ����� checkCrabAngle(double angle) TODO: ������ ������ ��������� �������� 
			 */
			checkCrabAngle(angle);
			
			tosSNS.pnResAll.get(snsPriority.sensorGroup).getBtRemoveBias().pressVW();
			tosSNS.pnResAll.get(Sensors.Gyro).getDlgRemoveBias().pressButton1OK();
			
			/// �������� ��������� ������� Do you want to hold current heading or use previous � ����������� ��������.
			if(setHeadingDialog.isOpen()) setHeadingDialog.pressButton1OK();

			/// Error �������� Bias = 0, ����������  ����� ������������� ������� ��������� � ������� �����������. ��� 14.
			tosSNS.pnResAll.get(snsPriority.sensorGroup).processedData.get(BIAS).mustBeValue(0.0);
			
			Do.dpAssert(snsPriority.dataIndicators.get(HDG).isEqualInRangeToIndValue(
					tosSNS.pnResAll.get(snsPriority.sensorGroup).processedData.get(HDG), 0.5, 0.0, 1, 360),
					"Data of priority Sensor is not equal data of pnResult with added bias");
			
			Do.dpAssert(snsPriority.dataIndicators.get(HDG).isEqualInRangeToIndValue(
					tosMAP.getIndCurHeading(), 0.5, 0.0, 1, 360),
					"Data of priority Sensor is not equal data of current course with added bias");
			
			ArrayList<PanelSwitcher> pSW = PanelSwitcher.createAllpsws(getAllowedAppList().keySet());
			for( PanelSwitcher each : pSW ) each.bringPanelRight("Ref");
			
			/// ������� �������� REF-��������. ����� ��� ���������� ������ � ����-��������� ��������
			ArrayList<Integer> gpsSel = tosREF.checkGroup(REFs.GPS, SensorREFModes.FaultSelect, 1, true, false, false, true);
			
			if(!gpsSel.isEmpty()){
				REFPanel refPanel = Do.sureFirstObject(tosREF.refAll.get(REFs.GPS), 
						new MethodSignature(true, "modeFast", SensorREFModes.FaultSelect));	
				/// �������� (longPress) �� ��������� ������.
				refPanel.button.longPressVW();
				/// �� ����������� ������� �������� �� No
				tosREF.reSelAll.pressButton1OK();
			}
			
			for( PanelSwitcher each : pSW ) each.bringPanelRight("Map");
			
			/// Step 15. ������� AutoHDG
			tosMFP.btHdgAUTO.pressVW();
			tosMFP.setHdgMode(HdgModes.HDG_AUTO);
			tosMFP.mustBeActiveHdgMode(HdgModes.HDG_AUTO);
			
			tosMAP.getIndCurHeading().rmbVal();
			Do.pause(Time.POP_UP_ON_SCREEN);
			if(angle == 30.0) Do.dpAssert(tosMAP.getIndCurHeading().cmpVal(), "Current heading is changed");
			else if(angle == -30.0) Do.dpAssert(!tosMAP.getIndCurHeading().cmpVal(), "Current heading is not changed");
		}
	}
	
	/**
	 * ����� ��� �������� �������� Crab Angle � ����������� �� ������� HOLD / PREVIOUS 
	 * � ����������� ���� SetCrabAngleSystemDialog.
	 * @param angle ���� ���� > 0 (30) �� ��������������� �������� ����� ������� PREVIOUS, ����� ����� ������� HOLD
	 */
	private void checkCrabAngle(double angle){
		if(isSemiAutomatic){ // TODO: ���������� ������ ��������		
			tosMFP.btHdgTrack.pressVW(); /// ������ HDG_TRACK. ��������� TODO: CrabAngle = 0 + �������� ������� (Cancel).
			int crabAngle = Integer.parseInt(tosMFP.dlgHdgTrack.field.getCurrentValue("Text"));
			tosMFP.dlgHdgTrack.checkCloseOnButton2Cancel();
			
			boolean is = (angle > 0) ? (crabAngle == 0) : (crabAngle != 0);
			boolean isEqual = (angle > 0) ? tosMAP.getIndCurHeading().cmpVal() : !tosMAP.getIndCurHeading().cmpVal();
			String msg = (angle > 0) ? "not" : "";
			
			Do.dpAssert(is, "Crab Angle is " + msg + " equal zero : " + crabAngle);
			
			Do.dpAssert(isEqual, "Course is " + msg + " changed : MemVal = " + 
					tosMAP.getIndCurHeading().getMemVal() + " ; curVal + " + tosMAP.getIndCurHeading().getNumberVal());
		}
	}

	@Override
	public void postconditions() throws Exception {
		tosModel.switchAllSensorsAndREFsON();
		tosSNS.setWholeGroupFast(Sensors.Gyro, SensorREFModes.Select, false);
		tosSNS.setWholeGroupFast(Sensors.Wind, SensorREFModes.Select, false);
	}
}