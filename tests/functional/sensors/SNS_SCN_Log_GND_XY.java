package dpat.tests.functional.sensors;

import java.util.ArrayList;

import dpat.tests.RunTest;
import dpat.NvWidget.NvWidget;
import dpat.coreLib.common.config.IniFilesParameters;
import dpat.coreLib.common.config.Sensors;
import dpat.coreLib.common.constants.ApplicationTypes;
import dpat.coreLib.common.constants.RoleIDs;
import dpat.coreLib.common.constants.SensorREFModes;
import dpat.coreLib.common.Do;
import dpat.coreLib.common.MethodSignature;
import dpat.coreLib.indicators.StringMultiStateInd;
import dpat.coreLib._model.sensorFaults.LOG_Faults;
import dpat.coreLib._model.sensorFaults.SensorFaultsSet;
import dpat.coreLib.panels.PanelSwitcher;
import dpat.coreLib.panels.SensResults;
import dpat.coreLib.panels.SensorPanel;

/**
 * ��������� � Visual �� ����������� ��� ������� ������� ��� ��������� � ����������� ������ ��� ������ ����� ����������:
 * PhysicalType=GND; Type=XY,X
 * ����������� ���������, �������� �, ����� ������ ������������ ��� ������� ������� �� �����������.
 * - SNS_NEG_LogSpecificFormatErrors.java - ����������� ������ ������� LOG(OutOfLimit/FaultStatus).
 * - SNS_NEG_CommonFormatErrors.java - ��������� ���� �� ��������� ������ ��� �������� LOG.
 * 
 * @author v.kulikov
 */
public class SNS_SCN_Log_GND_XY extends RunTest {
	
	/** �������� ������� ��� ��������, �������, ����� � �������. **/
	public dpat.tests.functional.sensors.TOset tosSNS;
	public dpat.tests.functional.alarm.TOset tosAlarm;
	public dpat.tests.functional.Map.TOset tosMap;
	
	/** �������� ��� ����� Physical Type � ����� Simul.ini **/
	private String GND = "GND";
	
	/** �������� ��� ����� Type � ����� Simul.ini **/
	private String XY = "XY";
	
	@Override
	public void initTOsets() throws Exception {
		/*  ������������� ��������	 */
		fillAllowedAppList( RoleIDs.appVisual1, new ApplicationTypes[] {ApplicationTypes.Visual} );
		this.tosSNS = new dpat.tests.functional.sensors.TOset(RoleIDs.appVisual1);
		this.tosAlarm = new dpat.tests.functional.alarm.TOset(RoleIDs.appVisual1);
		this.tosMap = new dpat.tests.functional.Map.TOset(RoleIDs.appVisual1);
	}
	
	@Override
	public void preconditions() throws Exception {
		/// ����� DP ������ ���� ������� ��� ���������� ���� �������� ������ ����� - RunTest.
		
		/// �������� ������ ��� ������ � ini-������ ������ �������.
		IniFilesParameters fsp = RoleIDs.appVisual1.getIniFilesParameters();
		fsp.setSensorsIniLOGPhysicalType(1, GND);
		fsp.setSensorsIniLOGType(1, XY);
		RoleIDs.appVisual1.restartSystem();
		
		/// � ������ �������� ��� �������
		tosModel.switchAllSensorsAndREFsON();
		
		/// ���� Gyro ������ ���� ����������� �������.
		tosSNS.setNsensorsInGroup(Sensors.Gyro, SensorREFModes.Select, 1, false, true);
		
		/** �� ����� (�����) ������ ���������� ������������� ����� �.�. ������� Sensors; 
		  * �� ������ (������) ������� ���� Visual � DP ����������� Alarm
		  */
		ArrayList<PanelSwitcher> pSW = PanelSwitcher.createAllpsws(getAllowedAppList().keySet());
		for( PanelSwitcher each : pSW ) each.bringPanelRight("Alarm");
	}

	@Override
	public void scenario() throws Exception {
		/// ���������� ������� ������� ������ � ����� Select(�������������, ��� ������������� ����������).
		ArrayList<SensorPanel> logPanels = tosSNS.getAllSensorsInGroup(Sensors.LOG);
		Do.debug("Get sensors " + Sensors.LOG + " in different modes.");
		
		/** ����������, ���� �� LOG ������� (���� �� 1).
		  * ���� LOG �������� ��� ������, ���-�� ����� ������������ ������� ������, ���� ��������� ��� �� ������.
		  * ��������� ����������� ����� ������������ ��� skipped � ��������� � ������ ��������.
		  */
		if(logPanels.size() < 1) Do.skip("Less than 1 sensor " + Sensors.LOG + " selected . Check mode of this sensor.");
		
		/// ������� ������ ������ ���������� � ��������� ��������� �������.
		for (SensorPanel logPanel : logPanels){
			///> �������� ��������� �������(������), ����� �������� � ���.
			SensorPanel logSensor = logPanel;
			Do.debug("Check "+logSensor.button.getCaption());
			
			///> ���������� ������������� ��������� ������� �������. (������)
			tosSNS.bringSensorPanel(logSensor);
			
			///> ��������� ����� ��� ������ Auto Thr (���� ����� ReInitDialog).
			tosModel.btReinit.pressVW();
			tosModel.reinitParamsDialog.setValueWithApplyChanges(tosModel.reinitParamsDialog.getVelocity(), 0.5);
			
			///> ���, ���� SOG �� ���������� �� ������� ������� - 0.25 ����.
			Do.check4actionTO(tosMap.getIndSOG(), 
					"Speed is not increased!!", 
						new MethodSignature("isEqualInRangeToValue", 0.25, 0.1, 0.0, 1, 0));
			
			// �������� ����� ���������� ��� ������ � �������� � ������.
			SensorFaultsSet logModelFaults = tosModel.sensorsFaults.getParamSet(Sensors.LOG, logSensor.getNum());
			
			///> ���������� ������ �������� VDVBW
			logModelFaults.activateProtocol(Sensors.LOG, LOG_Faults.VDVBW);
			
			checkSensor(logSensor, Modes.BW, false); // ��������� ����������� �������.
			
			///> ���������� ��������� �� ���������.
			logModelFaults.activate(LOG_Faults.GNDLOG.name());
			logModelFaults.activate(LOG_Faults.XYLOG.name());
			
			///> �������� ��������� �������������� ��������� VDVHW/VDVBW
			for(LOG_Faults proto :  new LOG_Faults[]{ LOG_Faults.VDVBW, LOG_Faults.VDVHW })
			{
				logModelFaults.activate(proto.name());
				
				Modes mode = (LOG_Faults.VDVBW.equals(proto)) ? Modes.BWGNDXY : Modes.HW; Do.pause(2);
				
				checkSensor(logSensor, mode, true); // ��������� ����������� �������.
				
				logModelFaults.deactivate(proto.name());
			}
			
			///> ����������� ������� GNDLOG � XYLOG � ���������� VDVBW
			logModelFaults.activateProtocol(Sensors.LOG, LOG_Faults.VDVBW);
		}
	}
	
	/**> ����� ��� �������� ������� {������, ��������� �������, ������, ������ ������������/�����������}
	*@param sensor ������� ������(������).
	*@param mode - ������� �������.
	*@param state (Present/Absent - ������ ������������/�����������).
	**/
	private void checkSensor(SensorPanel logSensor, Modes mode, boolean state)
	{
		logSensor.setMode(SensorREFModes.Select);
		///- ��������� ������ - ������ ��� �������, � ����������� �� ��������.
		String commonState = ((((Modes.BW.equals(mode) || Modes.HW.equals(mode)) ? state : true)) ? "isLightGreen" : "isRed");
		Do.check4actionTO(logSensor.button, new MethodSignature(commonState)); 
		Do.debug("Button " + logSensor.button.getCaption() + " " + commonState);
		
		///- ��������� ������� ������� ������ ��� �������, � ����������� �� ��������.
		String stateDataFilter = ((((Modes.BW.equals(mode) || Modes.BWGNDXY.equals(mode)) ? false : true)) ? "isGreenAny" : "isRedAny");
		Do.check4actionTO(logSensor.dataFilter, new MethodSignature(stateDataFilter)); 
		Do.debug("Filter of sensor " + logSensor.button.getCaption() + " " + stateDataFilter); Do.pause(2);
		
		///- ������, ����������, ��� -> ������ ������������ ��� �����������.
		if(Modes.BW.equals(mode)){
			logSensor.allDataIndicatorsMustBe(false);
		} else if(Modes.BWGNDXY.equals(mode)){
			logSensor.allDataIndicatorsMustBe(true);
		} else if(Modes.HW.equals(mode)){
			try {
				logSensor.allDataIndicatorsMustBe(false);
			} catch(NvWidget.NvExceptionFatal nf){
				Do.debug(nf.getMessage());
				logSensor.dataIndicators.get("Vx.Title").mustBeDataPresent();	
			}
		}
		
		///- �������. ��������� ����� � ����.
		StringMultiStateInd statusLOG = ("Raw data".equals(tosSNS.getLblRawUsedDataTitle().getVal())) ? logSensor.statusLOGRaw : logSensor.statusLOG;
		String caption = statusLOG.getCaption();
		Do.dpAssert(caption.equalsIgnoreCase((Modes.HW.equals(mode)) ? "Through Water" : "Ground"), "Caption of statusLOG != " + caption);
		logSensor.statusLOG.mustBeRed();

		///- ��������� ������ �����������
		SensResults pnLogResult = tosSNS.pnResAll.get(Sensors.LOG);
		
		///- ��������� ������ ��� �������
		Do.check4actionTO(pnLogResult.dataProcInd, new MethodSignature("isGreenAny")); 
		
		///- �������� ������������ ��� �����������.
		pnLogResult.processedData.forEach((key,value) -> { value.mustBeDataPresent(); } );
		
		// TODO: �������.
	}
	
	private enum Modes { BW, HW, BWGNDXY }; // ����������� ������������ ������������ ����������.

	/// ���������� DP � �������� ���������
	@Override
	public void postconditions() throws Exception {
		///> � Visual �������� ������� ��� ������� LOG � Select.
		tosSNS.setWholeGroupFast(Sensors.LOG, SensorREFModes.Select, false);
		Do.debug("Set All Sensors " + Sensors.LOG + " to Select after test.");
	}
}