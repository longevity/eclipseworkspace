package dpat.tests.functional.alarm;

import dpat.NvWidget.NvWidget;
import dpat.NvWidget.NvWidget.NvExceptionFatal;
import dpat.coreLib.buttons.Button;
import dpat.coreLib.buttons.Button2state;
import dpat.coreLib.common.Do;
import dpat.coreLib.common.TOgeneral;
import dpat.coreLib.common.config.IniFilesParameters.NvExceptionFatalNoValue;
import dpat.coreLib.common.constants.RoleIDs;
import dpat.coreLib.panels.AlarmLine;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import org.json.simple.parser.JSONParser;

/**
 * ���� TOset - ��� Alarm
 * (��������� � ������ tests.functional.alarm)
 * @author v.kulikov
 *
 */
public class TOset extends TOgeneral {

	///> ����������� ����� ��� ���� ������ Alarm �������
	/* ������   */
	public Button btArrUP;
	public Button btArrDN;
	public Button2state btACK;
	public Button2state btAllACK;
	public Button2state btAll;
	public Button2state btActive;
	public Button2state btDynamicView;
	public Button2state btStaticView;
	public Button2state btDetails;
	public Button2state btMonitor;

	/* ��������� ������ ������   */
	public AlarmLine alarmLineMessage;

	/**
	 * �����������
	 * */
	public TOset(RoleIDs currAI) {
		// ��������������� �������� �������������� ����������
		super(currAI);
		/// > ����������� ������� ����� ��� ���� ������ Alarm �������:
		Do.debug("Common objects for Alarm functional tests will be created");

		// ������� � ������ 1998 �������������� �������� � ���� ������� �������� (��. DP-14740)
		btArrUP = new Button(this.V,"wndAlarm:bMoveUp");
		btArrDN = new Button(this.V,"wndAlarm:bMoveDown");
		btACK = new Button2state(this.V,"wndAlarm:btAck");
		btAllACK = new Button2state(this.V,"wndAlarm:btAckAll");
		btAll = new Button2state(this.V,"wndAlarm:btAll");
		btActive = new Button2state(this.V,"wndAlarm:btActual");
		btDynamicView = new Button2state(this.V,"wndAlarm:btDynamicView");
		btStaticView = new Button2state(this.V,"wndAlarm:btStaticView");
		btDetails = new Button2state(this.V,"wndAlarm:btShowDetails");
		btMonitor = new Button2state(this.V,"wndAlarm:btShowMonitor");
		//message = new AlarmString(this.V, "wndAlarm:sgMessages");
		alarmLineMessage = new AlarmLine(this.V);
	}



	/**
	 * ����� ��� ��������, �� ����� �� ������ ������ ������.
	 */
	private boolean alarmSearchPeriodNotExpired(long startSearch, long searchPeriod){
		return (System.currentTimeMillis() - startSearch) < searchPeriod * 1000;
	}

	/**
	 * ����� ������ ���� �������������� � ������, ��������� �������.
	 * @param alarmType - ��� ������, ���������� ������������ ��� ����� � ���� Event
	 * @param msgPattern ���������� ���������, �������������� ��������� ������ � ���� Event ��� ������������� ������
	 * @param isActual TRUE - ���������� �����; FALSE - ������������ �����
 	 * @param isAcknowledged TRUE - �������������� �����; FALSE - ���������������� �����
 	 * @param alarmID - ���������� ������������� ������, ������� ��������� ����� (null, ���� ��������� ����� ��������� �����)
	 * @return ����� (��������� ������ Alarm) ��� ����������� ������������ ��������� � ����
	 */
	private Alarm mustBePresentAlarm(String alarmType, String msgPattern, boolean isActual, boolean isAcknowledged, Alarm alarm) {
		//Alarm alarm = regAlarm;
		boolean isPresent= false; // ������� ����, ��� ������ ����� � ������ Event (�.�. ���������� ����)
		boolean isReqStatusFound = false; // ������� ����, ��� ������ ����� � ������ ��������
		boolean isIdKnown = null != alarm; // �������, ��� �������� ������������� ������

		String alarmText = isIdKnown? alarm.getAlarmText(): ""; // ����� ������, ������������ � ����� Event

		final long SEARCH_PERIOD = 20; // sec., ������������ �����, ��������� �� ����� ������� ������ (�� ������, ���� ����� �������� ����� �������� � ��������)
		long startSearch = System.currentTimeMillis(); // msec, �������� ����� ������ ������ ������ (������, �� �������� ������������� searchPeriod)

		do{
			///> ������������� ��������� �������� ������ ����� ����������� ������
			isPresent = isIdKnown; // ������� ����, ��� ������ ����� � ������ Event (�.�. ���������� ����)
			isReqStatusFound = false; // ������� ����, ��� ������ ����� � ������ ��������

			///> ������� � ������ ����� ������� ����
			if (!isIdKnown) {
				try{
					//if (!isIdKnown) alarm = new Alarm( initRole, alarmType); // ���� �� ����� ID, ���� ��������� �����
					alarm = new Alarm( initRole, alarmType); // ���� �� ����� ID, ���� ��������� �����
					//else alarm = new Alarm( initRole, alarmType, alarmID); // ���� ����� ID, ���� ����� � ���� ID

					///> ���������, ��� ����� ������ ������������� ��������� ����������� ���������
					Do.debug("Alarm with specified type: \"" + alarmType + "\" found. Check text of alarm...");
					//String msgDetail = alarm.getMsgDetail();
					//String alarmText = alarm.getMsgDisplay() + (msgDetail.isEmpty()? "": (": " + msgDetail));
					alarmText = alarm.getAlarmText();
					if (!alarmText.matches(msgPattern)) { // ���� ����� ������ �� ������������� ��������� ����������� ���������
					//if(!msgPattern.isEmpty() && !alarmText.matches(msgPattern)) {// !!!!! ��� ��������	!!!!!
						Do.debug("...text of alarm is not correct"); continue;
					}
					isPresent = true;
					Do.debug("...text of alarm is correct");
				}catch (NvExceptionFatalNoRegAlarm e){}
			}

			if( isPresent ){ // ���� ���� ������� ���� � � ������ ������� � ���� Event �����
				Do.debug("Alarm with specified type: \"" + alarmType + "\" and text: \"" + alarmText + "\" is present in list");
				Do.debug("Check status of alarm...");
				boolean isCurAlarmActual = alarm.isActual(); // ������� ������������ �������� ������
				Do.debug("Current alarm is actual: " + isCurAlarmActual);

				boolean isCurAlarmAcknowledged = alarm.IsAcknowledged(); // ������� ���������������� �������� ������
				Do.debug("Current alarm is acknowledged: " + isCurAlarmAcknowledged);
				//this.stamp = alarm.getAlarmID(); // ���������� ������������� ������

				///> ����������, ��� ����� �� ��������� ���, ��� ���������
				if (isActual && !isAcknowledged) isReqStatusFound = isCurAlarmActual && !isCurAlarmAcknowledged; //���������� � ����������������
				else if (isActual && isAcknowledged) isReqStatusFound = isCurAlarmActual && isCurAlarmAcknowledged; //���������� � ��������������
				else if (!isActual && !isAcknowledged) isReqStatusFound = !isCurAlarmActual && !isCurAlarmAcknowledged; //������������ � ����������������
				else if (!isActual && isAcknowledged) isReqStatusFound = !isCurAlarmActual && isCurAlarmAcknowledged;//������������ � ��������������
				Do.debug("...alarm with specified status found: " + isReqStatusFound + ", State: " + alarm.alarmState);
			}
		// ��� ������� ������ �������� ������������ ��� ������-�� �� ������������ �������
		} while (!(isPresent && isReqStatusFound) && this.alarmSearchPeriodNotExpired(startSearch, SEARCH_PERIOD));
		// �� ������ �� ����� ���������, ��� ����� � ������� ���������� ������ ����� ������������
		// ���� ��� - ������ ���������� � �������� ��������� ����.
		Do.dpAssert((isPresent && isReqStatusFound), "No required last alarm: " + alarmType +"/ present: "+
				isPresent + "/ reqStatusFound: " + isReqStatusFound, NvExceptionFatalNoRegAlarm.class);
		///> ���������� ����� (��������� ������ Alarm) ��� ����������� ������������� ������ �� ������� ������ ������������/�������������
		return alarm;
	}

	/**
	 * ����� ������ ���� ���������, �������������� � ������, ��������� �������.
	 * @param alarmType - ��� ������, ���������� ������������ ��� ����� � ���� Event
	 * @param msgPattern ���������� ���������, �������������� ��������� ������ � ���� Event ��� ������������� ������
	 * @param isActual TRUE - ���������� �����; FALSE - ������������ �����
 	 * @param isAcknowledged TRUE - �������������� �����; FALSE - ���������������� �����
	 * @return ����� (��������� ������ Alarm) ��� ����������� ������������ ��������� � ����
	 */
	public Alarm mustBeLastPresentWithReqStatus(String alarmType, String msgPattern, boolean isActual, boolean isAcknowledged) {
		return mustBePresentAlarm( alarmType, msgPattern, isActual,  isAcknowledged,  null);
	}

	/**
	 * ����� � �������� ��������������� ������ ���� �������������� � ������ � ����� ��������� ������.
	 * @param alarm - ����� (��������� ������ Alarm)
	 * @param msgPattern ���������� ���������, �������������� ��������� ������ � ���� Event ��� ������������� ������
	 * @param isActual TRUE - ���������� �����; FALSE - ������������ �����
 	 * @param isAcknowledged TRUE - �������������� �����; FALSE - ���������������� �����
	 */
	public void mustBePresentWithReqStatus(Alarm alarm, String msgPattern, boolean isActual, boolean isAcknowledged) {
		//mustBePresentAlarm( alarm.alarmType, msgPattern,  isActual,  isAcknowledged,  alarm.alarmID);
		mustBePresentAlarm( alarm.alarmType, msgPattern,  isActual,  isAcknowledged,  alarm);

	}

	/**
	 * ����� �� ������ ���� ���������, �������, ��������� �������
	 * @param msgPattern ���������� ���������, �������������� ������ � ���� Event ��� �������������
	 * @param actual TRUE - ���������� �����; FALSE - ������������ �����
 	 * @param acknowledged TRUE - �������������� �����; FALSE - ���������������� �����
	 */
	public void mustBeNotLastPresentWithReqStatus(String alarmType, String msgPattern, boolean isActual, boolean isAcknowledged){

		boolean exist = false;
		try{
			mustBeLastPresentWithReqStatus(alarmType, msgPattern, isActual, isAcknowledged);
		}catch(NvExceptionFatalNoRegAlarm nv){
			exist = true;
		}
		Do.dpAssert(exist, "Required last visible alarm of type: "+alarmType+" was found.");
	}

	/**
	 * �������� ������ ������ ���������� ���� � �������� ���������������.
	 * @param alarmType ��� ������, ������������ ��� ����� � ����� Event
	 * @param alarmID ���������� ������������� ������ (�������� � Messages*.dat)
	 */
	public Alarm getAlarm ( String alarmType, String alarmID){
		return new Alarm(this.getInitRole(), alarmType, alarmID);
	}

	/**
	 * �������� ������ ������ ���������� ���� ��� ���������� ������ � ������.
	 * @param msgPattern ����, ������������ ������������� ����������
	 * @param alarmType ��� ������, ������������ ��� ����� � ����� Event
	 */
	public  Alarm getAlarm ( String alarmType){
		return new Alarm(this.getInitRole(), alarmType);
	}

	//����� ��� �������������� ������ � ����
	protected Date strToDate(String strDate) {
		//String s="05.09.2013";
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");

		try {
			date = format.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block - �������� ������ �����, ���� �� �����������
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * �����, �������������� ����� �������� ���������� ������ ��������� ���� ��� ������������� �����.
	 * @author v.kulikov
	 *
	 */
	public static class Alarm {

		private RoleIDs currAI; // ����, ������������ ������������� ����������
		private String alarmType; // ��� ������, ������������ ��� ����� � ����� Event
		private String alarmID; // ���������� ������������� ������ (�������� � Messages*.dat)
		private JSONObject alarmState; // ���������, ���������� ���������� � ��������� ���������� ������ ������� ����

		private boolean isActCheckNeded = true; // ��������� ��������� �������� �� ����� (��������� ����� ��������, ����� ����� ���������� ����������)
		private boolean isAckCheckNeded = true; // ��������� ��������� ����������� �� ����� (��������� ����� ��������, ����� ����� ���������� �����������)


		/**
		 * �����������, ��������� ������ ������ ���������� ���� � �������� ���������������.
		 * @param msgPattern ����, ������������ ������������� ����������
		 * @param alarmType ��� ������, ������������ ��� ����� � ����� Event
	 	 * @param alarmID ���������� ������������� ������ (�������� � Messages*.dat)
		 */
		public Alarm(RoleIDs currAI, String alarmType, String alarmID){
			this.currAI = currAI;
			this.alarmType = alarmType;
			this.alarmID = alarmID;
			alarmState = askAboutAlarm(currAI, alarmType, alarmID);
		}

		/**
		 * �����������, ��������� ������ ������ ���������� ���� ��� ���������� ������ � ������.
		 * @param msgPattern ����, ������������ ������������� ����������
		 * @param alarmType ��� ������, ������������ ��� ����� � ����� Event
		 */
		public Alarm(RoleIDs currAI, String alarmType){
			this.currAI = currAI;
			this.alarmType = alarmType;
			alarmState = askAboutLastAlarm(currAI, alarmType);
			//this.alarmID = getStringValueForKey(AlarmProperties.ID);
			this.alarmID = getStringValueForKeyFromKnownState(AlarmProperties.ID, alarmState);
		}

		/**
		 * ������ ���������� �� ���� ������� ������� ����.
		 * @return ��� ���������� �� ������ ������� ���� � ������� JSONArray.
		 * ������� ��������������� � ������� ������ ������������� ���� ������ ������.
		 * @exception ���� ���������� �� ������� ������� ���� �� ����� �������.
		 */
		protected static JSONArray alarmInfo(RoleIDs role, String alarmType) {
			Do.debug("Try to get info about alarm with type: " + alarmType);
			JSONArray alarmArray = NvWidget.getAlarm(role.realID(), alarmType);
			Do.dpAssert(null != alarmArray && !alarmArray.isEmpty(), "Info about alarm with type: " + alarmType + " is not found", NvExceptionFatalNoRegAlarm.class);
			return alarmArray;
		}

		/**
		 * ������ ���������� �� ���������� ������ ������� ����.
		 *
		 * @return ���������� �� ���������� ���������������� ������ ������� ���� � ������� JSONObject.
		 */
		protected static JSONObject askAboutLastAlarm(RoleIDs role, String alarmType) {
			JSONArray alarmArray = alarmInfo(role, alarmType);
			Do.dpAssert(alarmArray.get(alarmArray.size() - 1).getClass().equals(JSONObject.class), "Wrong type of info about alarm with type: " + alarmType);
			return (JSONObject) alarmArray.get(alarmArray.size() - 1);
		}

		/**
		 * ������ ���������� �� ������������� ������ ������� ���� � ��������� ���������������.
		 *
		 * @return ���������� �� ������ ������� ���� � �������� ��������������� � ������� JSONObject.
		 * @throws ���� ����� � ��������� ��������������� �� ����� ������.
		 */
		protected static JSONObject askAboutAlarm(RoleIDs role, String alarmType, String alarmID) {
			JSONArray alarmArray = alarmInfo(role, alarmType);
			int length = alarmArray.size();
			for (int i = 0; i < length; i++) {
				Do.dpAssert(alarmArray.get(i).getClass().equals(JSONObject.class), "Wrong type of info about alarm with type: " + alarmType);
				JSONObject tmpAlarm = (JSONObject) alarmArray.get(i);
				Do.dpAssert(tmpAlarm.containsKey("Offset"), "Info about identificator for alarm with type: " + alarmType + " is not found");

				if ((alarmID.equals(tmpAlarm.get("Offset")))) {
					return tmpAlarm;
				}
			}
			//return null;
			throw new NvExceptionFatalNoRegAlarm("Alarm with type: " + alarmType + " and ID: " + alarmID + " is not found");
		}

		/**
		 * ����� ��� ���������� ��������� ������� ������.
		 */
		private void updateAlarmState(){
			alarmState = askAboutAlarm(currAI, alarmType, alarmID);
		}

		/**
		 * ����� ��� ���������� ���������� �������� �� ������� ����� �� ���������� ��������� ������� ������.
		 * @param key �������� ������
		 * @param alarmState ��������� ������� ������
		 * @return �������� �������� � ���� ������.
		 */
		private String getStringValueForKeyFromKnownState(AlarmProperties key, JSONObject alarmState){
			String strKey = key.getPropName();
			Do.dpAssert(alarmState.containsKey(strKey), "Info about '"+strKey+"' for alarm with type: "+alarmType +" is not found",NvExceptionFatalNoValue.class);
			Do.dpAssert( alarmState.get(strKey).getClass().equals(String.class), "Wrong type of value for "+strKey);
			String retval = (String) alarmState.get(strKey);
			Do.dpAssert(null!= retval, "Invalid value: " +retval +" for key '"+strKey+"' for alarm with type: "+ alarmType);
			return retval;
		}

		/**
		 * ����� ��� ���������� ���������� �������� �� ����������� ����� �� ��������� ������� ������.
		 * �������������� ������������ ���������� ���������.
		 * @param key �������� ������
		 * @return �������� �������� � ���� ������.
		 */
		private String getStringValueForKey(AlarmProperties key){
			/*String strKey = key.getPropName();
			updateAlarmState();
			Do.dpAssert(alarmState.containsKey(strKey), "Info about '"+strKey+"' for alarm with type: "+alarmType +" is not found",NvExceptionFatalNoValue.class);
			Do.dpAssert( alarmState.get(strKey).getClass().equals(String.class), "Wrong type of value for "+strKey);
			String retval = (String) alarmState.get(strKey);
			Do.dpAssert(null!= retval, "Invalid value: " +retval +" for key '"+strKey+"' for alarm with type: "+ alarmType);
			return retval;*/
			updateAlarmState();
			return getStringValueForKeyFromKnownState(key, alarmState);
		}

		/**
		 * ����� ��� ����������  �������� � ������� Long �� ������� ����� �� ���������� ��������� ������� ������.
		 * @param key �������� ������
		 * @param alarmState ��������� ������� ������
		 * @return �������� �������� � ������� Long.
		 */
		private Long getLongValueForKeyFromKnownState (AlarmProperties key, JSONObject alarmState){
			String strKey = key.getPropName();
			Do.dpAssert(alarmState.containsKey(strKey), "Info about '"+strKey+"' for alarm with type: "+alarmType +" is not found", NvExceptionFatalNoValue.class);
			Do.dpAssert( alarmState.get(strKey).getClass().equals(Long.class), "Wrong type of value for "+strKey);
			Long retval = (Long) alarmState.get(strKey);
			Do.dpAssert(null!= retval, "Invalid value: " +retval +" for key '"+strKey+"' for alarm with type: " + alarmType);
			return retval;
		}

		/**
		 * ����� ��� ����������  �������� � ������� Long �� ����������� ����� �� ��������� ������� ������.
		 * �������������� ������������ ���������� ���������.
		 * @param key �������� ������
		 * @return �������� �������� � ������� Long.
		 */
		private Long getLongValueForKey(AlarmProperties key){
			/*String strKey = key.getPropName();
			updateAlarmState();
			Do.dpAssert(alarmState.containsKey(strKey), "Info about '"+strKey+"' for alarm with type: "+alarmType +" is not found");
			Do.dpAssert( alarmState.get(strKey).getClass().equals(Long.class), "Wrong type of value for "+strKey);
			Long retval = (Long) alarmState.get(strKey);
			Do.dpAssert(null!= retval, "Invalid value: " +retval +" for key '"+strKey+"' for alarm with type: "+ alarmType);
			return retval;*/
			updateAlarmState();
			return getLongValueForKeyFromKnownState( key,  alarmState);
		}

		/**
		 * ����� ��� �������������� ���������� �������� (0 ��� 1) � �������  boolean.
		 * @param stringValue ������, � ������� ������������ ���������� ��������
		 * @return ���������� ��������, ��������������� ���������� ������.
		 */
		private boolean getBooleanFromString(String stringValue){
			//if(null == intValue) return null;
			Integer intValue = null;
			try{
				intValue = Integer.parseInt(stringValue);
			}
			catch (NumberFormatException e){
				e.printStackTrace();
				throw new NvExceptionFatal("Failed to parse integer number for alarm with type: "+alarmType +  " from "+ stringValue);
			}
			Do.dpAssert(intValue == 0 || intValue == 1, "Failed to parse boolean value for alarm with type: "+alarmType +  " from "+ intValue);
			return intValue == 1;
		}

		/**
		 * @return  ����
		 */
		public RoleIDs getRole() {
			return currAI;
		}

		/**
		 * @return  ��� ������
		 */
		public String getAlarmType() {
			return alarmType;
		}

		/**
		 * @return  ���������� ������������� ������ (�������� � Messages*.dat)
		 */
		public String getAlarmID() {
			return alarmID;
		}

		/**
		 * @return  �������� ������ (A, B ��� AB)
		 */
		public String getAlarmSource(){
			return getStringValueForKeyFromKnownState(AlarmProperties.SOURCE, alarmState);
		}

		/**
		 * @return ��� ������ �������������� ������
		 */
		public String getAlarmLevel(){
			return getStringValueForKeyFromKnownState(AlarmProperties.LEVEL, alarmState);
		}

		/**
		 * @return ������������ ����� ������, ������������ ������������
		 */
		public String getMsgDisplay(){
			return getStringValueForKeyFromKnownState(AlarmProperties.MSG_DISPLAY, alarmState);
		}

		/**
		 * @return ���������� ����� ������ ������, ������������� ������������
		 */
		public String getMsgDetail(){
			return getStringValueForKeyFromKnownState(AlarmProperties.MSG_DETAIL, alarmState);
		}

		/**
		 * @return ������ ����� ������, ������������ ������������ � ������� Event
		 */
		public String getAlarmText(){
			String msgDetail = getMsgDetail();
			String alarmText = getMsgDisplay() + (msgDetail.isEmpty() ? "" : (": " + msgDetail));
			return alarmText;
		}

		/**
		 * ����� ��� ��������� ������� �������� �� ������
		 *
		 * @return ������ ��������
		 */
		public ArrayList<Double> getAngularValueFromAlarm() {
			ArrayList<Double> listOfValues = new ArrayList<>();
			Pattern patternDetail = Pattern.compile("([0-9]+|[0-9]+\\.[0-9]+)\u00B0\\s*(STBD|PORT)");
			Matcher matcherDetail = patternDetail.matcher(getMsgDetail());
			while (matcherDetail.find()) {
				double d = Double.parseDouble(matcherDetail.group(1));
				if (matcherDetail.group(2).equals("PORT")) {
					listOfValues.add(-d);
				} else listOfValues.add(d);
			}
			return listOfValues;
		}

		/**
		 * @return ������� ������������ ������.
		 */
		public boolean isActual(){
			boolean retVal = false ;// ������� �� ��������� - ����� ����������
			if (isActCheckNeded){// ���� ��������� ��������� ������������
				String stringValue = Integer.toString((int)(long)getLongValueForKey(AlarmProperties.ACT));
				retVal = getBooleanFromString(stringValue);
				isActCheckNeded  = retVal;//������������ � ���������� ��������� ���������, ���� ����� ��� ��������
			}
			return retVal;
		}

		/**
		 * @return ������� ���������������� ������.
		 */
		public boolean IsAcknowledged(){
			boolean retVal = true ;// ������� �� ��������� - ����� �����������
			if(isAckCheckNeded){ // ���� ��������� ��������� ����������������
				String stringValue = Integer.toString((int)(long)getLongValueForKey(AlarmProperties.ACK));
				retVal = getBooleanFromString(stringValue);
				isAckCheckNeded  = !retVal;//���������������� � ���������� ��������� ���������, ���� ����� �������������
			}
			return retVal;

		}

		/**
		 * @return ����� ������������� ������
		 */
		public String getTimeStart(){
			return getStringValueForKeyFromKnownState(AlarmProperties.TIME_START, alarmState);
		}

		/**
		 * @return ����� ������ ���������� ������.
		 * ���� ���������� � ��������� � ������ ��������� ���, ������ ����� ��� �������, ���������� ����� ������.
		 */
		public String getTimeStop(){
			String retVal = "";
			if (isActCheckNeded){// ���� ��������� ��������� ����������
				try{
					retVal = getStringValueForKey(AlarmProperties.TIME_STOP);
					isActCheckNeded = false;// ���� ���������� � ��������� � ������� ������ ���������� ����, ������ ��� ��� �� �������� - ������ ����
					return retVal;
				} catch (NvExceptionFatalNoValue e){
					return "";
				}
			}
			return getStringValueForKeyFromKnownState(AlarmProperties.TIME_STOP, alarmState);
		}

		/**
		 * @return ����� ������������� ������.
		 * ���� ���������� � ��������� � ������ ��������� ���, ������ ����� ��� �� �����������, ���������� ����� ������.
		 */
		public String getTimeAcknowledge(){
			String retVal = "";
			if(isAckCheckNeded){ // ���� ��������� ��������� ����������������
				try {
					retVal = getStringValueForKey(AlarmProperties.TIME_ACK);
					isAckCheckNeded = false;// ���� ���������� � ��������� � ������� ������������� ����, ������ ��� ��� �� �������� - ������ ����
					return retVal;
				} catch (NvExceptionFatalNoValue e){
					return "";
				}
			}
			return getStringValueForKeyFromKnownState(AlarmProperties.TIME_ACK, alarmState);
		}

		/**
		 * @return ������������� �����, �� ������� ����������� ����� (A ��� B).
		 * ���� ���������� � ��������� � ������ ��������� ���, ������ ����� ��� �� �����������, ���������� ����� ������.
		 */
		public String getHostAcknowledge(){
			String retVal = "";
			if(isAckCheckNeded){ // ���� ��������� ��������� ����������������
				try{
					String stringValue = Integer.toString((int)(long)getLongValueForKey(AlarmProperties.ACK_ON_HOST));
					retVal = getBooleanFromString(stringValue) ? "B" : "A";
					isAckCheckNeded = false;// ���� ���������� � ��������� � ����� ������������� ����, ������ ��� ��� �� �������c� - ������ ����
					return retVal;
				} catch (NvExceptionFatalNoValue e){
					return "";
				}
			}
			return getBooleanFromString(Integer.toString((int)(long)getLongValueForKeyFromKnownState(AlarmProperties.ACK_ON_HOST, alarmState))) ? "B" : "A";
		}

		/**
		 * �������� ��������� ������� ������
		 */
		public enum AlarmProperties {
			ID("Offset"),                     //���������� ������������� ������ (�������� � Messages*.dat)
			SOURCE("Source"),                 //�������� ������ (A, B ��� AB)
			LEVEL("Priority"),                //��� ������ �������������� ������ ( ��������� ��������: "invisible", "info", "warning", "error", "critical")
			MSG_DISPLAY("MsgDisplay"),        //������������ ����� ������, ������������ ������������
			MSG_DETAIL("MsgDetail"),          //���������� ����� ������ ������
			ACT("IsActive"),                  //������� ������������ ������
			ACK("IsAcknowledged"),              //������� ���������������� ������
			TIME_START("TimeStart"),          //����� ������������� ������
			TIME_STOP("TimeStop"),            //����� ������ ���������� ������
			ACK_ON_HOST("AcknowledgedOnHost"),//������������� �����, �� ������� ����� ��� ����������� (A ��� B)
			TIME_ACK("TimeAcknowledge");      //����� ������������� ������

			private String propName; //�������� ������������ �������� � ��������� ���������� �� ������

			/** ����������� */
			AlarmProperties(String propName) {
				this.propName = propName;
			}

			/**
			 * �������� �������� ������������ �������� � ��������� ���������� �� ������
			 * @return �������� ������������ �������� � ���������
			 */
			public String getPropName() {
				return propName;
			}

		}


	}


//------------------------------------------------------------------------------------------------------------------------------

	/** ����� ���������� � ������ ���������� ���������� ������
	*/
	@SuppressWarnings("serial")
	static public class NvExceptionFatalNoRegAlarm extends NvExceptionFatal {
        /**
         *  �����������.
		 *  @param message - ��������� � ��������� ��������
		 */
		public NvExceptionFatalNoRegAlarm(String message){
            super(message);
        }
    }
	
	/*
	 * ����� ������ ���� �������������� � ������, ��������� �������.
	 * @param alarmType - ��� ������, ���������� ������������ ��� ����� � ���� Event
	 * @param isActual TRUE - ���������� �����; FALSE - ������������ �����
 	 * @param isAcknowledged TRUE - �������������� �����; FALSE - ���������������� �����
 	 * @param alarmID - ���������� ������������� ������, ������� ��������� ����� (null, ���� ��������� ����� ��������� �����)
	 * @return ����� (��������� ������ Alarm) ��� ����������� ������������ ��������� � ����
	 */
	/*private Alarm mustBePresentAlarm(String alarmType, String msgPattern, boolean isActual, boolean isAcknowledged, String alarmID){				
		Alarm alarm = null;
		boolean isPresent= false; // ������� ����, ��� ������ ����� � ������ Event (�.�. ���������� ����)
		boolean isReqStatusFound = false; // ������� ����, ��� ������ ����� � ������ ��������
		boolean isIdKnown = null != alarmID; // �������, ��� �������� ������������� ������
		
		final long SEARCH_PERIOD = 20; // sec., ������������ �����, ��������� �� ����� ������� ������ (�� ������, ���� ����� �������� ����� �������� � ��������)
		long startSearch = System.currentTimeMillis(); // msec, �������� ����� ������ ������ ������ (������, �� �������� ������������� searchPeriod)
		
		do{
			///> ������������� ��������� �������� ������ ����� ����������� ������
			isPresent = false; // ������� ����, ��� ������ ����� � ������ Event (�.�. ���������� ����)
			isReqStatusFound = false; // ������� ����, ��� ������ ����� � ������ ��������
			
			///> ������� � ������ ����� ������� ����
			try{
				if (!isIdKnown) alarm = new Alarm( initRole, alarmType); // ���� �� ����� ID, ���� ��������� �����
				else alarm = new Alarm( initRole, alarmType, alarmID); // ���� ����� ID, ���� ����� � ���� ID
				String alarmText = alarm.getMsgDisplay() + ": " + alarm.getMsgDetail();
				//if(!alarmText.matches(msgPattern)) continue; // ���� ����� ������ �� ������������� ��������� ����������� ���������	
				if(!msgPattern.isEmpty() && !alarmText.matches(msgPattern)) continue; // !!!!! ��� ��������	!!!!!
				isPresent = true;
			}catch (NvExceptionFatalNoRegAlarm e){}
			
			if( isPresent ){ // ���� ���� ������� ���� (� ������ ������� � ���� Event) �����
				Do.debug("Alarm with specified type: \"" + alarmType + "\" is present in list");
				Do.debug("Current alarm is active: " + alarm.isActive());
				Do.debug("Current alarm is acknowledged: " + alarm.IsAcknowledged());
				//this.stamp = alarm.getAlarmID(); // ���������� ������������� ������

				///> ����������, ��� ����� �� ��������� ���, ��� ���������
				if (isActual && !isAcknowledged) isReqStatusFound = alarm.isActive() && !alarm.IsAcknowledged(); //���������� � ����������������
				else if (isActual && isAcknowledged) isReqStatusFound = alarm.isActive() && alarm.IsAcknowledged(); //���������� � ��������������
				else if (!isActual && !isAcknowledged) isReqStatusFound = !alarm.isActive() && !alarm.IsAcknowledged(); //������������ � ����������������
				else if (!isActual && isAcknowledged) isReqStatusFound = !alarm.isActive() && alarm.IsAcknowledged();//������������ � ��������������
				Do.debug("Alarm with specified status found: " + isReqStatusFound);
			}
		// ��� ������� ������ �������� ������������ ��� ������-�� �� ������������ �������
		} while( !(isPresent && isReqStatusFound) &&   this.alarmSearchPeriodNotExpired(startSearch, SEARCH_PERIOD) ); 
		// �� ������ �� ����� ���������, ��� ����� � ������� ���������� ������ ����� ������������
		// ���� ��� - ������ ���������� � �������� ��������� ����.
		Do.dpAssert((isPresent && isReqStatusFound), "No required last alarm: " + alarmType +"/"+
				isPresent+"/"+isReqStatusFound+"/", NvExceptionFatalNoRegAlarm.class);				
		///> ���������� ����� (��������� ������ Alarm) ��� ����������� ������������� ������ �� ������� ������ ������������/�������������
		return alarm;
	}*/


	// ������ ��� ������ ������ � ������
	//private boolean isPresent; // ������� ����, ��� ������ ����� � ������ Event (�.�. ���������� ����)
	//private boolean isReqStatusFound; // ������� ����, ��� ������ ����� � ������ ��������
	//private String stamp; //  ���������� ������������� ������
	//private long searchPeriod; // ��������� ������, � ������� �������� ��������� ���������� ������ (�� ������, ���� ����� �������� ����� �������� � ��������)
	//private long startSearch; // ������, �� �������� ������������� searchPeriod
	
	/*/*
	 * ����� ��� ������������ ��������� ��������� ������ ������
	 */
	/*private void actualize(long currentMoment){
		//this.stamp = ""; // ������������� �������������� ������
		this.searchPeriod = 20; //2; // sec., ����. �����, ��������� �� ����� ������� ������
		if( currentMoment>0 ) this.startSearch = currentMoment; // �������� ����� ������ ������ ������ - ���� ���� �������� ������������� �������� �������
																// � ��������� ������ startSearch �������� �������, 
																// �.�. �������, ��� ���������� ������ � ��� �� ��������� ������� 
	}*/
	
	/*
	 * ����� ��� ������������ ��������� ��������� ������ ������ ��� ��������� ������� ������ ������
	 */
	/*private void actualize(){ // ������������� ����� ������� ������ ������, �� ��������� ����� ������ ������ ������� 
		this.actualize(-1);
	}*/
	
	/*
	 * ����� ��� ������������ ��������� ��������� ������
	 */
	/*private void noReqAlarmEvidences(){// ��� ��������� �������� ������
		this.isPresent = false;	
		this.isReqStatusFound = false;
		if( this.alarmSearchPeriodNotExpired() ){
			this.actualize(); // ���� �� ����� ������ ����� � ������� ����������� ������� ������ - ��������� ������ 
		}
	}*/
	
	/*
	 * ������ ���������� �� ������ ������� ����.
	 * @return ��� ���������� �� ������ ������� ���� � ������� JSONArray. 
	 * ������� ��������������� � ������� ������ ������������� ���� ������ ������.
	 */
	/*private JSONArray alarmInfo( String alarmType) {
		return Alarm.alarmInfo(this.getInitRole(), alarmType);
	}*/
		
	/*
	 * ������ ���������� �� ���������� ������ ������� ����.
	 * @return ���������� �� ���������� ���������������� ������ ������� ���� � ������� JSONObject. 
	 */
	/*private  JSONObject askAboutLastAlarm(String alarmType) {
		return Alarm.askAboutLastAlarm(this.getInitRole(), alarmType);
	}*/
	
	/*
	 * ������ ���������� �� ������������� ������ ������� ����.
	 * @return ���������� �� ������ ������� ���� � �������� ��������������� � ������� JSONObject. 
	 * null - ���� ����� � ��������� ��������������� �� ����� ������.
	 */
	/*private JSONObject askAboutAlarm(String alarmType, String alarmID) {
		return Alarm.askAboutAlarm(this.getInitRole(), alarmType, alarmID);
	}*/
	
	/*
	 * �������� ������ ������ ���������� ���� � �������� ���������������.
	 * @param msgPattern ����, ������������ ������������� ����������
	 * @param alarmType ��� ������, ������������ ��� ����� � ����� Event
	 * 	 	 * @param alarmID ���������� ������������� ������ (�������� � Messages*.dat)
	 */		
	/*public static Alarm getAlarm (RoleIDs currAI, String alarmType, String alarmID){
		return new Alarm(currAI, alarmType, alarmID);
	}*/
	
	/*
	 * �������� ������ ������ ���������� ���� ��� ���������� ������ � ������.
	 * @param msgPattern ����, ������������ ������������� ����������
	 * @param alarmType ��� ������, ������������ ��� ����� � ����� Event
	 */		
	/*public static Alarm getAlarm (RoleIDs currAI, String alarmType){
		return new Alarm(currAI, alarmType);
	}*/
	
	/*
	* @return �������� ����� ������ ������ � ������ ������� � ���� Alarm 
	*/
	/*private int lastRow(){
		return Integer.parseInt(this.message.getCurrentValue("RowCount"));
	}	*/
	

	
	/*
	 * ����� �� ������ ���� ���������, �������, ��������� �������
	 * @param msgPattern ���������� ���������, �������������� ������ � ���� Event ��� �������������
	 * @param actual TRUE - ���������� �����; FALSE - ������������ �����
 	 * @param acknowledged TRUE - �������������� �����; FALSE - ���������������� �����
	 * @return ����� ������� �������� ��� ����������� �������������
	 */
	/*public void mustBeNotLastVisibleReqStatus(String msgPattern, boolean actual, boolean acknowledged){
		
		boolean exist = false;
		try{
			mustBeLastVisibleReqStatus(msgPattern, actual, acknowledged);
		}catch(NvExceptionFatalNoRegAlarm nv){
			exist = true;
		}
		Do.dpAssert(exist, "Required last visible alarm: "+msgPattern+" was found.");
	}*/
	
	
	/*
	 * ����� ������ ���� ���������, �������, ��������� �������
	 * @param msgPattern ���������� ���������, �������������� ������ � ���� Event ��� �������������
	 * @param actual TRUE - ���������� �����; FALSE - ������������ �����
 	 * @param acknowledged TRUE - �������������� �����; FALSE - ���������������� �����
	 * @return ����� ������� �������� ��� ����������� �������������
	 */
	/*public String mustBeLastVisibleReqStatus(String msgPattern, boolean actual, boolean acknowledged ){				
		//private String[] possibleAlarmStatus = new String[]();
		//ArrayList<String> possibleAlarmStatus = new ArrayList<String>(Arrays.asList("A-NEW", "A", "NEW", ""));
		this.actualize(System.currentTimeMillis()); // ������������� �����, ������� � �������� ����� ������ ����� �� time stamp � ������� 20� (������ �������� ����������������)
													// ���� �� ����� ����� �� ��� �����, �������, ��� �� �� ������, � ��� ������
		do{
			noReqAlarmEvidences(); // ������������� �������� ������ - ��������� �� ������� � ����� ���������� ���� Event, �������, �����  
			///> ������� ������ � ����� ����� � ������ ����������
			//this.message.setRow(i);	// ��������������� �� ��������� ������ � ������� Alarm List - �.�. �� �������
			this.stamp = this.message.getTime(); // ������� ����� � time stamp ������
			this.isPresent = this.message.getEvent().matches(msgPattern); // ���� ������ ������	

			if( this.isPresent ){ // ���� ������ ������ � ���� Event �����
				Do.debug("Alarm with specified event: \"" + this.message.getEvent() + "\" is present in list, row: " +i);
				Do.debug("Current alarm status: " + this.message.getACT());

				///> ����������, ��� ����� �� ��������� ���, ��� ��� �����
				Do.dpAssert(possibleAlarmStatus.contains(this.message.getACT().trim()), "Unknown alarm status: " + this.message.getACT());
				if (actual && !acknowledged) isReqStatusFound = this.message.getACT().trim().equals("A-NEW"); //���������� � ����������������
				else if (actual && acknowledged) isReqStatusFound = this.message.getACT().trim().equals("A"); //���������� � ��������������
				else if (!actual && !acknowledged) isReqStatusFound = this.message.getACT().trim().equals("NEW"); //������������ � ����������������
				else if (!actual && acknowledged) isReqStatusFound = this.message.getACT().trim().equals("");//������������ � ��������������
				//else Do.stop("Unknown alarm status");
				Do.debug("Alarm with specified status found: " + this.isReqStatusFound+", "+i);
			}
			this.i--; // �������������� �������
		// ��� ������� ������ �������� ������������ ��� ������-�� �� ������������ �������
		} while( !(this.isPresent && this.isReqStatusFound) && 
				( i>=0 || this.alarmSearchPeriodNotExpired() )); 
		// �� ������ �� ����� ���������, ��� ����� � ������� ���������� ������ ����� ������������
		// ���� ��� - ������ ���������� � �������� ��������� ����.
		Do.dpAssert((this.isPresent && this.isReqStatusFound), "No required last visible alarm: " + msgPattern +"/"+
				this.isPresent+"/"+this.isReqStatusFound+"/"+i, NvExceptionFatalNoRegAlarm.class);				
		///> ���������� ����� ������� ��� ����������� ������������� ������ �� ������� ������ ������������/�������������
		return this.stamp;
	}*/

	//---------------------------------- ������ ������--------------------------------------------------------
	/*
	 * ����� ������ ���� ���������, ���������� � �������
	 * @param msgSubStr1,2,3 ��������� � ���� Event ��� ������������� 
	 * @return ����� ������� �������� ��� ����������� �������������
	 */
	/*public String mustBeLastNewVis(String msgSubStr1,String msgSubStr2, String msgSubStr3){				
		this.actualize(System.currentTimeMillis()); // ������������� �����, ������� � �������� ����� ������ ����� �� time stamp � ������� 20� (������ �������� ����������������)
													// ���� �� ����� ����� �� ��� �����, �������, ��� �� �� ������, � ��� ������
		do{
			noReqAlarmEvidences(); // ������������� �������� ������ - ��������� �� ������� � ����� ���������� ���� Event, �������, �����  
			///> ������� ������ � ����� ����� � ������ ����������
			this.message.setRow(i);	// ��������������� �� ��������� ������ � ������� Alarm List - �.�. �� �������
			this.stamp = this.message.getTime(); // ������� ����� � time stamp ������
			this.a_present = this.message.getEvent().contains(msgSubStr1) &&
							 this.message.getEvent().contains(msgSubStr2) &&
							 this.message.getEvent().contains(msgSubStr3); // ���� ������ ���������	
			if( this.a_present ){ // ���� ������ ��������� � ���� Event �����
				Do.debug("present: "+this.a_present+", "+i);
				///> ����������, ��� ����� ���������� ���������������� - �.�. ���, ��� ��� �����
				// a_act = this.message.getACT().contains("A_NEW"); // � ������ ���� cr15267
				this.a_act = this.message.getACT().contains("NEW"); // � ������ ��� cr15267
				Do.debug("act: "+this.a_act+", "+i);
				//!!!!!!!!!!!!! A ???????
			}
			this.i--; // �������������� �������
		// ��� ������� ������ �������� ������������ ��� ������-�� �� ������������ �������
		} while( !(this.a_present && this.a_act) && 
				( i>=0 || this.alarmSearchPeriodNotExpired() )); 
		// �� ������ �� ����� ���������, ��� ����� � ������� ���������� ������ ����� ������������
		// ���� ��� - ������ ���������� � �������� ��������� ����.
		Do.dpAssert((this.a_present && this.a_act), "No required last actual visible alarm! "+
				this.a_present+"/"+this.a_act+"/"+i);				
		///> ���������� ����� ������� ��� ����������� ������������� ������ �� ������� ������ ������������/�������������
		return this.stamp;
	}
	// �� �� �����, ������ � ����� ����������� ��� �������������
	public String mustBeLastNewVis(String msgSubStr1, String msgSubStr2){
		return mustBeLastNewVis(msgSubStr1, msgSubStr2, "");
	}
	// �� �� �����, ������ � ����� ���������� ��� �������������
	public String mustBeLastNewVis(String msgSubStr1){
		return mustBeLastNewVis(msgSubStr1, "", "");
	}/*
	
	/*
	 * �����, ������ ����������, � ��������� ������ ������ ���� ������������
	 * @param msgSubString1,2,3 ��������� � ���� Event ��� �������������  
	 * @param stamp ����� ������, ������� ��������� (����� ��������)
	 */
	/*public void mustBecomeInact(String msgSubStr1, String msgSubStr2, String tStamp){		
		this.actualize(System.currentTimeMillis());
		do{
			noReqAlarmEvidences();			
			///> ������� ������ � ����� ����� � ������ ���������� � �������� 
			this.message.setRow(i);	// ��������������� �� ��������� ������ � ������� Alarm List - �.�. �� �������					
			this.a_present = this.message.getEvent().contains(msgSubStr1) && 
							 this.message.getEvent().contains(msgSubStr2) && // ���� ������ ��������� 
							 this.message.getTime().contains(tStamp); 	     // ���� ������ �����
			if( this.a_present ){
				///> ����������, ��� ����� ������������ (�������������� ��� ����������������)				
				// a_active = this.message.getACT().contains("A_NEW"); // � ������ ���� cr15267
				//a_active = this.message.getACT().contains("NEW"); // � ������ ��� cr15267
				this.a_act = this.message.getACT().contains("NEW") || // ������������ ����������������
						this.message.getACT().contains("");      // ������������ ��������������
			}
			this.i--; // �������������� �������
		// ��� ������� ������ �������� ������������ ��� ������-�� �� ������������ �������
		} while( !(this.a_present && this.a_act) && 
				( this.i>=0 || this.alarmSearchPeriodNotExpired() )
				); 
		// �� ������ �� ����� ���������, ��� ����� � ������� ���������� ������ ����� ������������
		// ���� ��� - ������ ���������� � �������� ��������� ����.
		Do.dpAssert((this.a_present && this.a_act), "No required non-actual alarm!");
	}
	// �� �� �����, �� � 1 ����������
	public void mustBecomeInact(String msgSubStr1, String tStamp){
		mustBecomeInact(msgSubStr1, "", tStamp);
	}*/
	
	
		
	/*
	 * �������� �� ����� � ���� ������
	 * @param row ������ � ���������� (�� ��������) �������, � ������� ��������� �����
	 * @return ������� - TRUE
	 *
	public boolean isACT(int row){
		
		return false;
	}
	*/
		
	
	/*public void findSubMsg(String msg2find){				
		Do.debug("\r\n findSubMsg");
		int ii=0;
		//for( int i=0;i<lineNumDMoff;i++ ){
		for( int i=0;i<lastRow();i++ ){
			ii=i;
			this.message.setRow(i);
			if( this.message.getEvent().contains(msg2find) ){
				Do.debug("ACT:      "+this.message.getACT());	
				Do.debug("Event:    "+this.message.getEvent());
				Do.debug("Time:     "+this.message.getTime());
				Do.debug("This row: "+(i+1));				
			}			
		}
		Do.debug("Row on method finish: "+(ii+1));
		Do.debug("Last vis. row: "+this.message.getCurrentValue("VisibleRowCount"));
		Do.debug("Last abs. row: "+this.lastRow());		
		this.message.setRow(ii);
		Do.debug("Event on row finish:  "+this.message.getEvent());
		Do.debug("Time on row finish :  "+this.message.getTime());
	}
	
	public void findLine(int lNum){
		Do.debug("\r\n findLine");
		this.message.setRow(lNum);
		Do.debug("ACT:      "+this.message.getACT());
		Do.debug("Time:     "+this.message.getTime());
		Do.debug("Event:    "+this.message.getEvent());		
		Do.debug("Last row: "+this.lastRow());
				
	}*/

}
