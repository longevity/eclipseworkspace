package dpat.coreLib.plotters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import dpat.coreLib.buttons.Button;
import dpat.coreLib.buttons.Button2state;
import dpat.coreLib.common.Do;
import dpat.coreLib.common.TO2Implement;
import dpat.coreLib.common.config.IniFilesParameters.NvExceptionFatalNoValue;
import dpat.coreLib.indicators.ParamValue;
import dpat.coreLib.parameters.ParameterWithBreakingArrowsEditor;
import dpat.coreLib.parameters.ParameterWithChangeEditor;
import dpat.coreLib.plotters.GraphPanelConfig.GraphPanelClasses;

/**
 * ����� ����� ��������� ������ ��������:
 * ������ Ok, Cancel, ClearItems; 
 * ���������� DataType 
 * 	Class - ��� ����������� ������ ����� ��������;
 * 	Group - ��� ����������� ������ ����� ��������;
 * 	Items - � ������������ ������ ��������.
 * ��� Items ���������� ��������� �� �������� ������ ������������ ������� ���� ����������.
 * @author v.kulikov
 */
public class GenericGraphPanel {
	protected String commonIDpart; // ����� ����� ��� ������������ �������.
	protected String groupName; // ������.
	protected String graphPanelClass; // ��� ����������� ���� ������ (Class) Sensors, Power, Forces...
	private int num; // ����� ������� (��� ������������ �������������� �������).
	
	protected TO2Implement graf; // ������ ��� ������������ �������(�������(Sensors/REF); Power; 
	
	private Button btItems; // ������ ��� ������� � ������ ������ items.
	
	private Button btClearItems; // ������ ClearItems - ��� ������� ������� �� ��������� items.
	private Button btOk; // ������ �� - ��� ������������� ���������.
	protected Button btCancel; // ������ Cancel - ��� ������ ���������.
	
	private ParameterWithChangeEditor modeYaxis; // �������� Yaxis
	private ParameterWithChangeEditor mode; // �������� ��� ����� ������ ����������� ��������(Actual/History). ����� ������ Actual.
	
	private ParameterWithBreakingArrowsEditor changeTime; // ��� ��������� �������.

	private HashMap<String, Button2state> possibleItems; // ������� ��������� ��� ������� �������.
	
	private HashMap<String, ParamValue> dataTypeIndicators; // ������ ����������� ������ Data Type
	
	/** ����������� ������� ��������� �������� ��� ����������� ������ ������.  
	 * @param appID - ������������� ����������.
	 * @param classGraphPanel -  ��� ������(������ ���� ����������), ��������� � �������.
	 * @param group - �������� ������, ��� ������� �������� ����������� ������ ��� ������ �� ��������.
	 */
	public GenericGraphPanel(String appID, 
							 GraphPanelClasses classGraphPanel, 
							 String group)
	{
		graphPanelClass = classGraphPanel.toString(); // ��������� ���������� ������ �� GraphPanelClasses
		num = getPlotterNum(graphPanelClass, group); // �������� ����� ��� ������������ ������ �� ������.
		
		// ����� ����� ��� ������������ �������
		commonIDpart = "wnd" + graphPanelClass + ":pn" + getWgtId(classGraphPanel) + "Graph/fmGraphParamsDlg_" + num;
		
		dataTypeIndicators = new HashMap<String, ParamValue>();
		
		// ������
		graf = new TO2Implement(appID, "wnd" + graphPanelClass + ":pn" + group + "Graph/panel/plotter");
		
		// �������� ��������� ������� ��� ������� ������ ������.
		possibleItems = getPossibleItems(appID, group);
		
		// ����������� ������ ������ 
		btItems  = new Button(appID, commonIDpart + "/nvPanelDataType/nvSimBtnTypes");
		
		// ������ ClearItems - ��� ������� ��������.
		btClearItems = new Button(appID, commonIDpart + "/nvPanelButtons/nvSimBtnOk");
		
		// ������ Ok - ��� ������������� ���������.
		btOk = new Button(appID, commonIDpart + "/nvPanelButtons/nvSimBtnOk");
		
		// ������ Cancel - ��� ������ ���������.
		btCancel = new Button(appID, commonIDpart + "/nvPanelButtons/nvSimBtnCancel");
		
		// ��������� Class, Group, Items ������ Data Type. ��� �������� ����������.
		dataTypeIndicators.put("Class", new ParamValue(appID, "Class", commonIDpart + "/nvPanelDataType/nvParamCtrlClass"));
		dataTypeIndicators.put("Group", new ParamValue(appID, "Group", commonIDpart + "/nvPanelDataType/nvSimBtnGroup"));
		dataTypeIndicators.put("Items", new ParamValue(appID, "Items", commonIDpart + "/nvPanelDataType/nvParamCtrlTypes"));
		
		/** -----------------------------<History>----------------------------- **/
		
		changeTime = new ParameterWithBreakingArrowsEditor(
				appID, "Time axis", 
				commonIDpart + "/nvPanelTimeAxis/nvLabelTimeAxis",
				commonIDpart + "/nvPanelTimeAxis/nvParamCtrlTimeSpan",
				commonIDpart + "/nvPanelTimeAxis/nvParamCtrlTimeSpan",
				commonIDpart + "/nvPanelTimeAxis/nvSimBtnDecreaseTimeSpan"
		); // ��������� � ����� ��������� ��� ��������� �������.
	}
	
	/** -------------------------------<�������� ������>------------------------------- **/
	
	/**
	 * ��������� ����������� ������ ��� ����������� �� ������� (��������� ������).
	 * @param items - ����������� �������� ��� ����������� �� �������.
	 */
	public void switchItemsOn(ArrayList<String> items){
		for(String item : items)
		{
			Do.dpAssert(!possibleItems.containsKey(item), "Item " + item + " is not exist");
			if(possibleItems.get(item).isActive()) {
				Do.debug("Item " + item + " is already Active");
			} else {
				btItems.pressVW();	
				possibleItems.get(item).pressVW();
			}
			Do.debug("Switched to item : " + item);
		}
		pressOk();
		graf.mustBeUserVisible();
	}

	/**
	 * ����� ��� ������ �� �������� �������(�������). ������ ������� �������.
	 */
	public void makeGraphPanelVisible(){
		if(null == graf ) Do.debug("Plotter is null. Failed make.");
		if(!graf.isUserVisible()){
			if(btCancel.isUserVisible()) pressCancel();
			if(!graf.isUserVisible()){ pressCancel(); }
			else { Do.dpAssert(graf.isUserVisible(), "Plotter is InVisible. Check mode of button Cancel."); }
		}
		Do.debug("Plotter For Group " + groupName + " is UserVisisble.");
	}

	/** ����� ��� ��������� ������� � ���������� ������� ������� ������ ��������. 
	 *  ������ �� ������, ������ ���� �� ����� �� ������. �������� �� 0.5 � �� ��������� ���������� ���������  */
	public void pressPlotterVW(){
		Do.dpAssert(graf.isUserVisible(), "Plotter is InVisible.");
		graf.pressVW();
	}
	
	/** -----------------------------------<�������>------------------------------------ **/
	
	/**
	 * �������� �������� ��������� ������� ��� ������� ������ ��������.
	 * @return ������ �� ��������� ��� ��������� �������.
	 */
	public ParameterWithBreakingArrowsEditor getChangeTime(){
		Do.dpAssert(null != changeTime, "changeTime is null.");
		return changeTime;
	}
	
	/** �������� ����� ������ ��� ����������� ������ Data Type.*/
	public Set<String> getDataTypeIndicatorsKeySet() {
		return dataTypeIndicators.keySet();
	}
	
	/** 
	 * �������� DataIndicatir(ParamValue) ������ Data Type.
	 * @param name - ����, �� �������� ������� ������ ���������.
	 **/
	public ParamValue getDataIndicator(String name) {
		// �������� ������������� ���������� ������ � �������� ������.
		Do.dpAssert(dataTypeIndicators.containsKey(name), "Data Indicator "+name+" is not supported", NvExceptionFatalNoValue.class);
		Do.debug("get Data Indicator : " + name);
		return dataTypeIndicators.get(name);
	}
	
	/** ����������� ��������� � ����������� ������ �� ��������. **/
	public void pressOk() { btOk.pressVW(); }
	
	/** �������� ��������� � ����������� ������ �� ��������. ����� ��� ������������ ��� ������ � ��������. **/
	public void pressCancel() { btCancel.pressVW(); }
	
	/** ������ ��� �������� � �������. **/
	public void pressClearItems() { btClearItems.pressVW(); pressOk(); }
	
	/** 
	 * ��������� ����� ����������� ��� Y axis.
	 * @param axis - ����������� �����.
	 **/
	public void setDefaultYaxis(String axis) { 
		modeYaxis.setCurVal(axis);
	}
	
	/** 
	 * ��������� ����� �������.
	 * @param modeTime - ����������� �����.
	 **/
	public void setDefaultTime(String modeTime) {
		mode.setCurVal(modeTime); 
	}
	
	/** -------------------------------<��������� ������ - DONE>------------------------------- **/
	
	private HashMap<String, Button2state> getPossibleItems(String appID,
			String group) {
		return null;
	}
	
	// TODO ������� ���������, ���� � ������� ���������� ���������  classPlotter, plotterClass  ???
	/** �������� id ������� � ����������� �� ������ ������ **/
	private String getWgtId(GraphPanelClasses classPlotter){
		if(GraphPanelClasses.Ref.equals(classPlotter)) return "Sns/pn";
		else if(GraphPanelClasses.Power.equals(classPlotter)) return "Main/graphParent";
		else return groupName;
	}
	
	/**
	 * �������� ����� �������� � ����������� �� ���� � ���� �������, ���� ������������ ����������(��������).
	 * @param plotterClass - ���������, ��� ������� ��������� ������.
	 * @param group - ��� �������.
	 * @return ����� ������� ��� ������������ �������������� �������.
	 */
	private int getPlotterNum(String plotterClass, String group){
		if("Ref".equals(plotterClass)){ 
			num = 1;
		} else if("TrendView".equals(plotterClass) && "Top".equals(group)){
			num = 2;
		} else if("TrendView".equals(plotterClass) && "Middle".equals(group)){
			num = 3;
		} else if("TrendView".equals(plotterClass) && "Bottom".equals(group)){
			num = 4;
		} else if("Sensors".equals(plotterClass)){ // ��������� ������ ������� � ����������� �� ���� �������.
			if("Gyro".equals(group)) num = 5; 
			else if("Wind".equals(group)) num = 6;
			else if("VRS".equals(group)) num = 7; 
			else if("Draft".equals(group)) num = 8; 
			else if("LOG".equals(group)) num = 9; 
			else if("TS".equals(group)) num = 10;
		} else if("Power".equals(plotterClass)){
			num = 21;
		} else if("Target".equals(plotterClass)){
			num = 22;
		}
		return num;
	}
}