package dpat.coreLib.plotters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dpat.coreLib.buttons.Button;
import dpat.coreLib.buttons.Button2state;
import dpat.coreLib.common.Do;
import dpat.coreLib.plotters.GraphPanelConfig.GraphPanelClasses;

public class GraphPanelDataGroup extends GenericGraphPanel {
	
	private Button btGroup; // ������ ��� ������ ������ ��� ����������� �� ��������.
	private Button btCancelGroupItems; // ������ ��� ������ ��������� � ������ ������ ��� ����������� �� ��������.
	
	private HashMap<String, Button2state> possibleGroups; // ��������� ������ ��� �����������.

	/** ����������� ������� ��������� �������� ��� ������������ ���������� ������.  
	 * @param appID - ������������� ����������.
	 * @param classPlotter -  ��� ������, ��������� � �������.
	 * @param group - ��� ������
	 */
	public GraphPanelDataGroup(String appID, 
			GraphPanelClasses classPlotter, 
			String group) {
		super(appID, classPlotter, group);
		
		graphPanelClass = classPlotter.toString(); // ��������� ���������� ��� ������� �����.
		
		// ����������� ������ ������ �� �������
		btGroup  = new Button(appID, commonIDpart + "/nvPanelDataType/nvSimBtnGroup");
		
		// ������ CancelGroupItems - ��� ������ ��������� � ������� ������ ������ Group/Items.
		btCancelGroupItems = new Button(appID, "wnd" + graphPanelClass + ":nvSimBtnCancel");
		
		// ��� ������� ������ �������� �������� ��������� ������ ������������ �������.
		possibleGroups = getPossibleGroupModes(appID, graphPanelClass, group);
	}
	
	
	/** -------------------------< �������� ������>-------------------------------------- **/
	
	/**
	 * ������ ��������� ��� ��� ������� ��������.
	 * @param appID - ������� ������������� ����������.
	 * @param classPlotter - ������� �����.
	 * @param group - ������� ������ ��������.
	 * @return ������ ��������� �������(���� - �������� ������, �������� - ���� ������).
	 */
	private HashMap<String, Button2state> getPossibleGroupModes(String appID, String plotterClass, String group){
		String nameClass = plotterClass;
		if("All".equals(nameClass)){
			for(GraphPanelClasses pc : GraphPanelClasses.values()){
				@SuppressWarnings("static-access")
				ArrayList<String[]> groups = pc.valueOf(nameClass).getDataIndAssociate();
				possibleGroups.putAll(getModes(appID, groups));
			}	
		} else {
			ArrayList<String[]> groups = GraphPanelClasses.valueOf(nameClass).getDataIndAssociate();
			
			ArrayList<String[]> current = new ArrayList<String[]>();
			
			for(String[] list : groups)
			{
				if(group.equalsIgnoreCase(list[2]) && list[3] == "GROUP"){
					current.add(list);
					possibleGroups.putAll(getModes(appID, current));
				}
			}
		}
		return possibleGroups;
	}
	
	/**
	 * �������� ������ ������ Items
	 * @param appID - ������� ������������� ����������.
	 * @param groups - ����� ���������� ���������������.
	 * @return ������ ��������� ������ (Items)
	 */
	private Map<String, Button2state> getModes(String appID, ArrayList<String[]> groups){
		Map<String, Button2state> possibleModes = new HashMap<>();

		for(String key[] : groups)
		{
			String title = key[0]; String address = key[1];
			possibleModes.put(title, new Button2state(appID, "wndSensors:nvPanelSelect/" + address));
		}	
		
		return possibleModes;
	}
	
	/** 
	 * ����� ��� ������������ ����� ������� ������� ������ ��������. 
	 * plotterGroupOrItem - ���������� �������, ����������� ��� ����������� �� ��������.
	 **/
	public void switchGraphGroup(Enum<?> plotterGroup){
		pressPlotterVW();
		String fullClassName = plotterGroup.getDeclaringClass().getDeclaringClass().getCanonicalName();
		String name = fullClassName.substring(fullClassName.lastIndexOf('.')+1, fullClassName.length());
		switchGroupOn(name);
	}

	/**
	 * ��������� ����������� ������ ��� ����������� �� ��������.
	 * @param group - ����������� ������ ��� ���������.
	 */
	public void switchGroupOn(String group){
		btGroup.pressVW();
		possibleGroups.get(group).pressVW();	
		
		Do.debug("Switched to group : " + group);
		pressOk();
		graf.mustBeUserVisible();
	}
	
	/**
	 * ����� ��� ����������� �������� �� �������. ����������� ����� �� �������� ��������. 
	 */
	@Override
	public void makeGraphPanelVisible(){
		if(null == graf ) Do.debug("Plotter is null. Failed make.");
		if(!graf.isUserVisible()){
			if(btCancelGroupItems.isUserVisible()){
				btCancelGroupItems.pressVW();
				pressCancel();
			} else {
				if(btCancel.isUserVisible()) pressCancel();
			}
			Do.dpAssert(graf.isUserVisible(), "Plotter is InVisible. Check mode of button Cancel.");
		}
		Do.debug("Plotter For Group " + groupName + " is UserVisisble.");
	}
}