package dpat.coreLib.plotters;

import java.util.ArrayList;

import dpat.coreLib.common.Do;
import dpat.coreLib.common.config.CommonConfigNew;

/**
 * ����� ������, ���������� �������������� ����� � ������ ��� ���� ��������..
 * @author v.kulikov
 */
public class GraphPanelConfig {
	/**
	 * ������ ������� ��� ����������� ����������� ������ ������� ������.
	 * @author v.kulikov
	 */
	public static enum GraphPanelClasses implements CommonConfigNew{
		Sensors {
			@Override
			public void setDataIndAssociate() {
				/*
				 * 0 - ��� ������(������).
				 * 1 - ������������� ���� � �������.
				 * 2 - ����� �������(�����)
				 * 3 - ����� �������
				 * 4 - ������ ������ ������
				 * 5 - ������ ��� ����.
				 */
				/** -------------------------------------------<GROUP>------------------------------------------- **/
				dataIndAssociate.add(new String[] {"Gyro Heading", "HdtGroup_GyroHead", "GYRO", "GROUP"});
				dataIndAssociate.add(new String[] {"Gyro Yaw Rate", "HdtGroup_GyroYawRate", "GYRO", "GROUP"});
				dataIndAssociate.add(new String[] {"Wind Direction", "HdtGroup_WindDir", "WIND", "GROUP"});
				dataIndAssociate.add(new String[] {"Wind Speed", "HdtGroup_WindSpeed", "WIND", "GROUP"});
				dataIndAssociate.add(new String[] {"VRS Pitch", "HdtGroup_VrsPitch", "VRS", "GROUP"});
				dataIndAssociate.add(new String[] {"VRS Roll", "HdtGroup_VrsRoll", "VRS", "GROUP"});
				dataIndAssociate.add(new String[] {"LS Displacement", "HdtGroup_LsDisplacement", "DRAFT", "GROUP"});
				dataIndAssociate.add(new String[] {"LS Loading", "HdtGroup_LsLoading", "DRAFT", "GROUP"});
				dataIndAssociate.add(new String[] {"LS Displacement", "HdtGroup_LsDisplacement", "DRAFT", "GROUP"});
				dataIndAssociate.add(new String[] {"LS Loading", "HdtGroup_LsLoading", "DRAFT", "GROUP"});
				dataIndAssociate.add(new String[] {"Log", "HdtGroup_LogXY", "Button", "LOG", "GROUP"});
				dataIndAssociate.add(new String[] {"TS Elevation", "HdtGroup_TsEle", "TS", "GROUP"});
				dataIndAssociate.add(new String[] {"TS Force", "HdtGroup_TsForce", "TS", "GROUP"});
				dataIndAssociate.add(new String[] {"TS Azimuth", "HdtGroup_TsAzi", "TS", "GROUP"});
				
				/** -------------------------------------------<ITEMS>------------------------------------------- **/
				// TODO: ���������� �� ���������� ��������.
			}
		}, 
		Ref {
			@Override
			public void setDataIndAssociate() {
				/*
				 * 0 - ��� ������(������).
				 * 1 - ������������� ���� � �������.
				 * 2 - ����� �������(�����)
				 * 3 - ����� �������
				 * 4 - ������ ������ ������
				 * 5 - ������ ��� ����.
				 */
				dataIndAssociate.add(new String[] {"DGPS LAT", "HdtGroup_GpsLat", "GPS", "GROUP"});
				dataIndAssociate.add(new String[] {"DGPS LON", "HdtGroup_GpsLon", "GPS", "GROUP"});
				dataIndAssociate.add(new String[] {"HPR X", "HdtGroup_HprX", "HPR", "GROUP"});
				dataIndAssociate.add(new String[] {"HPR Y", "HdtGroup_HprY", "HPR", "GROUP"});
				dataIndAssociate.add(new String[] {"LR Bearing", "HdtGroup_LrBearing", "LR", "GROUP"});
				dataIndAssociate.add(new String[] {"LR Range", "HdtGroup_LrRange", "LR", "GROUP"});
				dataIndAssociate.add(new String[] {"REF X", "HdtGroup_RefX", "REF", "GROUP"});
				dataIndAssociate.add(new String[] {"REF Y", "HdtGroup_RefY", "REF", "GROUP"});
				dataIndAssociate.add(new String[] {"TW Beta", "HdtGroup_TwBeta", "TW", "GROUP"});
				dataIndAssociate.add(new String[] {"TW Length", "HdtGroup_TwLength", "TW", "GROUP"});
				dataIndAssociate.add(new String[] {"TW Alpha", "HdtGroup_TwAlpha", "TW", "GROUP"});
			}
		}, 
		Forces {
			@Override
			public void setDataIndAssociate() {
				/*
				 * 0 - ��� ������(������).
				 * 1 - ������������� ���� � �������.
				 * 2 - ����� �������(�����)
				 * 3 - ����� �������
				 * 4 - ������ ������ ������
				 * 5 - ������ ��� ����.
				 */
				dataIndAssociate.add(new String[] {"%-Force", "HdtGroup_RelativeForces", "FORCE", "GROUP"});
				dataIndAssociate.add(new String[] {"X-Force", "HdtGroup_XForce", "FORCE", "GROUP"});
				dataIndAssociate.add(new String[] {"Y-Force", "HdtGroup_YForce", "FORCE", "GROUP"});
				dataIndAssociate.add(new String[] {"Z-Moment", "HdtGroup_ZMoment", "FORCE", "GROUP"});
			}
		}, 
		Power {
			@Override
			public void setDataIndAssociate() {
				/*
				 * 0 - ��� ������(������).
				 * 1 - ������������� ���� � �������.
				 * 2 - ����� �������(�����)
				 * 3 - ����� �������
				 * 4 - ������ ������ ������
				 * 5 - ������ ��� ����.
				 */
				dataIndAssociate.add(new String[] {"Bus Breakers", "HdtGroup_BusTieBreak", "POWER", "GROUP"});
				dataIndAssociate.add(new String[] {"Generator Powers", "HdtGroup_GenPow", "POWER", "GROUP"});
				dataIndAssociate.add(new String[] {"Generator Breakers", "HdtGroup_GenTieBreak", "POWER", "GROUP"});
				dataIndAssociate.add(new String[] {"Propeller Powers", "HdtGroup_PropPow", "POWER", "GROUP"});
				dataIndAssociate.add(new String[] {"Propeller Breakers", "HdtGroup_PropTieBreak", "POWER", "GROUP"});
				dataIndAssociate.add(new String[] {"Thruster Powers", "HdtGroup_ThrPow", "POWER", "GROUP"});
				dataIndAssociate.add(new String[] {"Thruster Breakers", "HdtGroup_ThrTieBreak", "POWER", "GROUP"});
			}
		}, 
		Thr {
			@Override
			public void setDataIndAssociate() {
				/*
				 * 0 - ��� ������(������).
				 * 1 - ������������� ���� � �������.
				 * 2 - ����� �������(�����)
				 * 3 - ����� �������
				 * 4 - ������ ������ ������
				 * 5 - ������ ��� ����.
				 */
				dataIndAssociate.add(new String[] {"Angle", "HdtGroup_ThrDirCmdFeed", "THR", "GROUP"});
				dataIndAssociate.add(new String[] {"Thrust", "HdtGroup_ThrLoadCmdFeed", "THR", "GROUP"});
				dataIndAssociate.add(new String[] {"RPM", "HdtGroup_ThrRpmCmdFeed", "THR", "GROUP"});
			}
		}, TrendView {
			@Override
			public void setDataIndAssociate() {
				/*
				 * 0 - ��� ������(������).
				 * 1 - ������������� ���� � �������.
				 * 2 - ����� �������(�����)
				 * 3 - ����� �������
				 * 4 - ������ ������ ������
				 * 5 - ������ ��� ����.
				 */
				dataIndAssociate.add(new String[] {null, "Top", "THR", "GROUP"});
				dataIndAssociate.add(new String[] {null, "Middle", "THR", "GROUP"});
				dataIndAssociate.add(new String[] {null, "Bottom", "THR", "GROUP"});
			}
		};
	
		// ��������� ��� �������� �������� �������� ��������� ����������� ������
		protected ArrayList<String[]> dataIndAssociate = new ArrayList<String[]>();
		
		/**
		 * ��������� �������� �������� ��������� ����������� ������.
		 */
		abstract void setDataIndAssociate();
		
		private GraphPanelClasses(){
			setDataIndAssociate();		
		}
		
		/**
		 * ��������� �������� �������� ��������� ����������� ������
		 * @return ��������� ��� �������� �������� �������� ��������� ����������� ������
		 */
		@Override
		public ArrayList<String[]> getDataIndAssociate(){
			Do.dpAssert(!this.dataIndAssociate.isEmpty(), "dataIndAssociate is Empty For current class : "+this.name());
			return this.dataIndAssociate; 
		}
	}
}