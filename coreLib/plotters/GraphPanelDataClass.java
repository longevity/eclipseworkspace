package dpat.coreLib.plotters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dpat.coreLib.buttons.Button2state;
import dpat.coreLib.plotters.GraphPanelConfig.GraphPanelClasses;

public class GraphPanelDataClass extends GraphPanelDataGroup {
	
	private Button2state btClass; // ������ Class ��� ������ ������ ������� ������.
	
	private HashMap<String, Button2state> possibleClasses;

	public GraphPanelDataClass(String appID, GraphPanelClasses classPlotter,
			String group) {
		super(appID, classPlotter, group);
		
		btClass = new Button2state(appID, commonIDpart + "/nvPanelDataType/nvSimBtnClass");
		
		// ��� ������� ������ �������� �������� ��������� ������ ������������ �������.
		possibleClasses = getPossibleClassModes(appID, group);
	}
	
	public Button2state getBtClass(){
		return btClass;
	}
	
	public HashMap<String, Button2state> getPossibleClasses()
	{
		return possibleClasses;
	}

	/**
	 * ������ ��������� ��� ��� ������� ��������.
	 * @param appID - ������� ������������� ����������.
	 * @param group - ������� ������ ��������.
	 * @return ������ ��������� �������(���� - �������� ������, �������� - ���� ������).
	 */
	private HashMap<String, Button2state> getPossibleClassModes(String appID, String group){
		HashMap<String, Button2state> map = new HashMap<>();
		
		if("All".equals(group)){
			for(GraphPanelClasses pc : GraphPanelClasses.values()){
				@SuppressWarnings("static-access")
				ArrayList<String[]> groups = pc.valueOf(group.toUpperCase()).getDataIndAssociate();
				map.putAll(getModes(appID, groups));
			}	
		} else {
			ArrayList<String[]> groups = GraphPanelClasses.valueOf(group.toUpperCase()).getDataIndAssociate();
			map.putAll(getModes(appID, groups));
		}
		return map;
	}
	
	private Map<String, Button2state> getModes(String appID, ArrayList<String[]> groups){
		Map<String, Button2state> possibleModes = new HashMap<>();

		for(String key[] : groups)
		{
			String title = key[0]; String address = key[1];
			possibleModes.put(title, new Button2state(appID, address));
		}	
		
		return possibleModes;
	}
}
