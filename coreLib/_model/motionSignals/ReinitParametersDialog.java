package dpat.coreLib._model.motionSignals;

import dpat.coreLib.common.Do;
import dpat.coreLib.common.TO2Implement;
import dpat.coreLib.indicators.NumberWithIndependentUnitsModelEditField;
import dpat.coreLib.indicators.StringInd;
import dpat.coreLib.indicators.NumberWithUnitsDataInd.UnitsTypes;
import dpat.coreLib.popUp.BaseConfirmationDialog;
import dpat.coreLib.popUp.GenericPopUp;

/**
 * ������ ReinitParametersDialog � ������, ��� ��������� ��������� ���������, ��������, ����� �����.
 * @author v.kulikov
 */
public class ReinitParametersDialog extends BaseConfirmationDialog {
	private static String commonIDpart = "OKRightDlg:"; // ������� ��� ������������� ������������ �������
	
	private NumberWithIndependentUnitsModelEditField lat_degree;
	private NumberWithIndependentUnitsModelEditField lat_minute;
	private NumberWithIndependentUnitsModelEditField lon_degree;
	private NumberWithIndependentUnitsModelEditField lon_minute; // ���������� ��������� �����(������, �������) [�, '].
	private NumberWithIndependentUnitsModelEditField xg;
	private NumberWithIndependentUnitsModelEditField yg; // xg, yg - ���������� ��������� �����.
	private NumberWithIndependentUnitsModelEditField velocity; // ��������� ��������
	private NumberWithIndependentUnitsModelEditField course; // ��������� ����
	
	/**
	 * ����������� ������� ������ �������������� ��������� ���������, ��������, ����� ����� � ������.
	 * @param appID ������������� ����������. (Model)
	 * @param bt1OkID ������� ������ ��.
	 * @param bt2CancelID ������� ������ Cancel.
	 * @param titleID ������� ���������.
	 * @param textOfTitle ����� ���������.
	 */
	public ReinitParametersDialog(String appID, 
								  String bt1OkID,
								  String bt2CancelID, 
								  String titleID, 
								  String textOfTitle)
	{
		super(appID, commonIDpart+bt1OkID, commonIDpart+bt2CancelID, commonIDpart+titleID, textOfTitle);
		this.lat_degree = new NumberWithIndependentUnitsModelEditField(appID, new StringInd(appID, commonIDpart+"Label22").getVal() + " " + new StringInd(appID, commonIDpart+"Label24").getVal(), commonIDpart+"La_g", commonIDpart+"Label24", UnitsTypes.ANGLE);
		this.lat_minute = new NumberWithIndependentUnitsModelEditField(appID, new StringInd(appID, commonIDpart+"Label22").getVal() + " " + new StringInd(appID, commonIDpart+"Label26").getVal(), commonIDpart+"La_m", commonIDpart+"Label26", UnitsTypes.ANGLE);
		this.lon_degree = new NumberWithIndependentUnitsModelEditField(appID, new StringInd(appID, commonIDpart+"Label23").getVal() + " " + new StringInd(appID, commonIDpart+"Label25").getVal(), commonIDpart+"Lo_g", commonIDpart+"Label25", UnitsTypes.ANGLE);
		this.lon_minute = new NumberWithIndependentUnitsModelEditField(appID, new StringInd(appID, commonIDpart+"Label23").getVal() + " " + new StringInd(appID, commonIDpart+"Label27").getVal(), commonIDpart+"Lo_m", commonIDpart+"Label27", UnitsTypes.ANGLE);
		this.xg = new NumberWithIndependentUnitsModelEditField(appID, new StringInd(appID, commonIDpart+"Label30").getVal(), commonIDpart+"edStartXg",  commonIDpart+"Label30", UnitsTypes.LONG_DISTANCE);
		this.yg = new NumberWithIndependentUnitsModelEditField(appID, new StringInd(appID, commonIDpart+"Label31").getVal(), commonIDpart+"edStartYg", commonIDpart+"Label31", UnitsTypes.LONG_DISTANCE);
		this.velocity = new NumberWithIndependentUnitsModelEditField(appID, new StringInd(appID, commonIDpart+"Label36").getVal(), commonIDpart+"edStartVelociry", commonIDpart+"Label36", UnitsTypes.VELOCITY);
		this.course = new NumberWithIndependentUnitsModelEditField(appID, new StringInd(appID, commonIDpart+"Label37").getVal(), commonIDpart+"edStartCourse", commonIDpart+"Label37", UnitsTypes.ANGLE);
	}
	
	/** -------------------------------------<�������� ������>---------------------------------- **/
	
	/**
	 * ��������� � ������� ������� ��������� ���������� �������� (��� �������������). 
	 * ������ ����������� ���������:
	 * 	30.00.0106N; - ��� ��������� �������� ������ � ����� ����. ������ �������� ��������� ���, ��� ���������� � �������.
	 * 	090.00.0000W  - ��� ��������� �������� ������ �� ������ ����. ������ �������� ��������� ���, ��� ���������� � �������.
	 * ������������� ����������� �������� ������� �� ���������� ���������.
	 */
	public void setBaseLatLonCoordsWithoutApplyChanges(String value){
		this.mustBeOpen();
		
		boolean isLat = (value.endsWith("N") || value.endsWith("S")) ? true : false;
		
		String[] tokens;
		String degrees; //�������
		String minutes; //������
		String deciminutes; //������� �����
	
        if(isLat){
        	tokens = GenericPopUp.validateGeog(value, true);
        	degrees =tokens[0]; minutes =tokens[1]; deciminutes =tokens[2].substring(0, tokens[2].length() - 1);
        	this.lat_degree.setValue(Double.parseDouble(degrees));
			this.lat_minute.setValue(Double.parseDouble(minutes+"."+deciminutes));
        } else {
        	tokens = GenericPopUp.validateGeog(value, false);
        	degrees =tokens[0]; minutes =tokens[1]; deciminutes =tokens[2].substring(0, tokens[2].length() - 1);
        	this.lon_degree.setValue(Double.parseDouble(degrees));
			this.lon_minute.setValue(Double.parseDouble(minutes+"."+deciminutes));
        }
        
        String sign = tokens[2].substring(tokens[2].length()- 1, tokens[2].length()); //����
		        
		selectCombo(CardinalDirection.valueOf(sign));
	}
	
	/**
	 * ��������� � ������� ������� ��������� ���������� �������� (� �������������� �������� ��������). 
	 * ������ ����������� ���������:
	 * 	30.00.0106N; - ��� ��������� �������� ������ � ����� ����. ������ �������� ��������� ���, ��� ���������� � �������.
	 * 	090.00.0000W  - ��� ��������� �������� ������ �� ������ ����. ������ �������� ��������� ���, ��� ���������� � �������.
	 * ������������� ����������� �������� ������� �� ���������� ��������� � �������� ����� ����������.
	 */
	public void setBaseLatLonCoordsWithApplyChanges(String value){
		setBaseLatLonCoordsWithoutApplyChanges(value);
		pressOk();
		this.mustBeClosed();
	}
	
	/** ��������� �������� ��� ������������ ���������� ������� Reinit parameters ��� ������������� ��� �������� **/
	public void setValueWithoutApplyChanges(NumberWithIndependentUnitsModelEditField numInd, double value){
		this.mustBeOpen();
		if(this.velocity.equals(numInd)) Do.dpAssert(Math.abs(value) < 40.0, "Speed must be between 0 and 40 knots by absolute value");
		if(this.course.equals(numInd)) Do.dpAssert(Math.abs(value) < 360, "Invalid value! Start course must be between 0� and 360�");
		numInd.setValue(value);
		Do.debug(numInd.getTitle()+" : "+numInd.getNumberVal()+" ["+numInd.getUnits()+"];");
	}
	
	/** ��������� �������� ��� ������������ ���������� ������� Reinit parameters � �������������� ��� �������� **/
	public void setValueWithApplyChanges(NumberWithIndependentUnitsModelEditField numInd, double value){
		setValueWithoutApplyChanges(numInd, value);
		pressOk();
		this.mustBeClosed();
	}
	
	/** ����������� ��������, ��� ����������� ������ � ������ ������ ������������ �� ������ **/
	@Override
	public void mustBeOpen(){
		this.lat_degree.mustBeUserVisible();
		this.lat_minute.mustBeUserVisible();
	
		this.lon_degree.mustBeUserVisible();
		this.lon_minute.mustBeUserVisible();
		
		this.xg.mustBeUserVisible();
		this.yg.mustBeUserVisible();
		
		this.velocity.mustBeUserVisible();
		this.course.mustBeUserVisible();
		
		this.bt1OK.mustBeUserVisible();
		this.bt2Cancel.mustBeUserVisible();
	}
	
	/** ����������� ��������, ��� ����������� ������ � ������ ������ �� ������������ �� ������ **/
	@Override
	public void mustBeClosed(){
		this.lat_degree.mustBeUserInvisible();
		this.lat_minute.mustBeUserInvisible();
	
		this.lon_degree.mustBeUserInvisible();
		this.lon_minute.mustBeUserInvisible();
		
		this.xg.mustBeUserInvisible();
		this.yg.mustBeUserInvisible();
		
		this.velocity.mustBeUserInvisible();
		this.course.mustBeUserInvisible();
		
		this.bt1OK.mustBeUserInvisible();
		this.bt2Cancel.mustBeUserInvisible();
	}
	
	/** ����������� �������� ��������� **/
	public void pressOk(){
		bt1OK.pressVW();
	}
	
	/** ---------------------------------------<�������>--------------------------------------- **/

	/** �������� ��������� LatDegree **/
	public NumberWithIndependentUnitsModelEditField getLatDegree(){
		Do.dpAssert(null != this.lat_degree, "Lat degree is not exists in ReinitParametersDialog.");
		return this.lat_degree;
	}
	
	/** �������� ��������� LatMinute **/
	public NumberWithIndependentUnitsModelEditField getLatMinute(){
		Do.dpAssert(null != this.lat_minute, "Lat minute is not exists in ReinitParametersDialog.");
		return this.lat_minute;
	}
	
	/** �������� ��������� LonDegree **/
	public NumberWithIndependentUnitsModelEditField getLonDegree(){
		Do.dpAssert(null != this.lon_degree, "Lon degree is not exists in ReinitParametersDialog.");
		return this.lon_degree;
	}
	
	/** �������� ��������� LonMinute **/
	public NumberWithIndependentUnitsModelEditField getLonMinute(){
		Do.dpAssert(null != this.lon_minute, "Lon minute is not exists in ReinitParametersDialog.");
		return this.lon_minute;
	}
	
	/** �������� ��������� Xg **/
	public NumberWithIndependentUnitsModelEditField getXg(){
		Do.dpAssert(null != this.xg, "Xg is not exists in ReinitParametersDialog.");
		return this.xg;
	}
	
	/** �������� ��������� Xy **/
	public NumberWithIndependentUnitsModelEditField getYg(){
		Do.dpAssert(null != this.yg, "Yg is not exists in ReinitParametersDialog.");
		return this.yg;
	}
	
	/** �������� ��������� velocity **/
	public NumberWithIndependentUnitsModelEditField getVelocity(){
		Do.dpAssert(null != this.velocity, "velocity is not exists in ReinitParametersDialog.");
		return this.velocity;
	}
	
	/** �������� ��������� course **/
	public NumberWithIndependentUnitsModelEditField getCourse(){
		Do.dpAssert(null != this.course, "course is not exists in ReinitParametersDialog.");
		return this.course;
	}
	
	/** -----------------------------------<��������� ������>--------------------------------- **/
	
	/**
	 * �������� �������� ���������� ������������ ������� �� ������ � ���������� �������� ������������.  
	 */
	@Override
	public void checkTimeout(){
		this.checkTextOfTitle();
		this.timeout();
		this.mustBeOpen(); // ��������� ������� ������ ������������
		Do.debug("Opening of \"" + this.textOfTitle + "\" dialog on timeout done");
	}
	
	/**
	 * ����� ��� ������ �������� �� ����������� ������
	 * @param dir ��������, ������� ���� ���������� [N S W E]
	 */
	private void selectCombo(CardinalDirection dir){
		String address = null;
		if("N_S".contains(dir.name())) address = "cbLatNS";
		else if("W_E".contains(dir.name())) address = "cbLonEW";
		else Do.stop("Invalid dir : "+dir.name());
		TO2Implement to2i = new TO2Implement("Model", commonIDpart+address );
		to2i.setCombo(dir.name());
	} 
	
	private enum CardinalDirection { N, S, W, E }
}