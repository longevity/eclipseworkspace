package dpat.coreLib._model.motionSignals;

import java.util.ArrayList;
import java.util.Arrays;

import dpat.coreLib.buttons.Button;
import dpat.coreLib.common.Do;
import dpat.coreLib.common.config.CommonConfigNew;

/**
 * ������ ������� ������ MotionSignals (������).
 * @author v.kulikov
 */
public enum Tabs implements CommonConfigNew {
	INFO{ // ������� Info.
		/**
		 * ������������ ��������� � ���������� �������� ��������� ����������� ������
		 */
		@Override
		public void setDataIndAssociate() {
			/*
			 * 0 - ��� ���������.
			 * 1 - ����� �������� (��������� � ������� ������ ����������)
			 * 2 - ����� �����������(��������� � ������� ��������� - title)
			 * 3 - ����� ����������
			 * 4 - ��� �����������
			 */                        
			dataIndAssociate.add(new String[] {null, null, "lShipName", "StringInd", ""}); // �������� �����
			dataIndAssociate.add(new String[] {"Displasment", "lDispl", "lDispl1", "NumberWithIndependentUnitsDataInd", "FORCE"}); // ������� �������������
			dataIndAssociate.add(new String[] {"Engine Power", "lEngPower", "lEngPower1", "NumberWithIndependentUnitsDataInd", "POWER"}); // �������� �������������� ���������
			dataIndAssociate.add(new String[] {"Service Speed", "lSpeed", "lSpeed1", "NumberWithIndependentUnitsDataInd", "VELOCITY"}); // ���������������� ��������.
			dataIndAssociate.add(new String[] {"Length o.a.", "lLoa", "lLoa1", "NumberWithIndependentUnitsDataInd", "SHORT_DISTANCE"}); // Loa - ������������ �����(����� ������������ �������).
			dataIndAssociate.add(new String[] {"Breadth", "lBoa", "lBoa1", "NumberWithIndependentUnitsDataInd", "SHORT_DISTANCE"}); // Boa - ������������ ������(����� ������������ �������).
			dataIndAssociate.add(new String[] {"Draft forward", "lTf", "lTf1", "NumberWithIndependentUnitsDataInd", "SHORT_DISTANCE"}); // ������ � ����.
			dataIndAssociate.add(new String[] {"Draft after", "lTa", "lTa1", "NumberWithIndependentUnitsDataInd", "SHORT_DISTANCE"}); // ������ � �����.
		}
	}, 
	ENV{ // ������� ENV, ��������� �� ���� ����������.
		@Override
		public void setDataIndAssociate() {
			this.btOk = new Button("Model", "fmMotionAnalysis:btOk");
			subTubsAssociate.addAll(Arrays.asList(SubTabs.EnvSubTabs.values() ));
		}
	}, 
	MOTION{ // ������� Motion.
		@Override
		public void setDataIndAssociate() {	
			dataIndAssociate.add(new String[] {"Xg", "lXg", "Label7", "NumberWithIndependentUnitsDataInd", "LONG_DISTANCE"}); // ������ DP-15229
			dataIndAssociate.add(new String[] {"Yg", "lYg", "Label8", "NumberWithIndependentUnitsDataInd", "LONG_DISTANCE"}); // xg, yg - ���������� ��������� �����.

			// TODO: ���� �� ��������, �.�. 0.00; Vgx=0.00 �������� ����� �������, � ����� getNumberVal ��������� ���� �����
			// dataIndAssociate.add(new String[] {"lVx1", "lVx", null, "NumberWithUnitsIndTitle"}); // vx - - ������ �������� �� ��� ��� [����].
			// dataIndAssociate.add(new String[] {"lVy1", "lVy", null, "NumberWithUnitsIndTitle"}); // vy - ������ �������� �� ��� ����� [����].

			dataIndAssociate.add(new String[] {"V", "lV_", "lV_1", "NumberWithIndependentUnitsDataInd", "VELOCITY"}); // v - �������������� ������ �������� [����].
			dataIndAssociate.add(new String[] {"Heading", "lCourse", "Label9", "NumberWithIndependentUnitsDataInd", "ANGLE"}); // heading - �������� ����.
			dataIndAssociate.add(new String[] {"Wz", "lWz", "Label11", "NumberWithIndependentUnitsDataInd", "RATE_OF_TURN"}); // wz - ������� ��������.
			dataIndAssociate.add(new String[] {"Roll", "lRoll", "Label19", "NumberWithIndependentUnitsDataInd", "ANGLE"}); // roll - ���� �����.
			dataIndAssociate.add(new String[] {"Pitch", "lPitch", "Label20", "NumberWithIndependentUnitsDataInd", "ANGLE"}); // pitch - ���������.
		}
	};
	
	// ��������� ��� �������� �������� �������� ��������� ����������� ������
	protected ArrayList<String[]> dataIndAssociate = new ArrayList<String[]>();
	
	// ��������� ��� �������� ���������� ������� (�������� �����).
	protected ArrayList<CommonConfigNew> subTubsAssociate = new ArrayList<>();
	
	protected Button btOk; // ������ ������������� ��������� ��������(������� Env).
	
	private Tabs(){
		setDataIndAssociate(); // ��������� ��� ������ ������� �������� � �������������� �����������.	
	}
	
	/**
	 * ��������� �������� �������� ��������� ����������� ������. 
	 * ����������� ������������� ��� ������ ������ (���������� enum) ����������.
	 */
	abstract void setDataIndAssociate();
	
	/**
	 * ��������� �������� �������� ��������� ����������� ������
	 * @return ��������� ��� �������� �������� �������� ��������� ����������� ������
	 */
	public ArrayList<String[]> getDataIndAssociate() { return this.dataIndAssociate; }
	
	/**
	 * ��������� ���������� �������(���������) ����������� �������, ��� ���������� - ���������� ������ ������ �������.
	 * @return ������ �������(���������) subTubsAssociate
	 **/
	public ArrayList<CommonConfigNew> getSubTabs() { return subTubsAssociate; }
	
	/** �������� ������ Ok �������. 
	 * @return ������ ��.
	 **/
	public void pressOk() {
		Do.dpAssert(null != this.btOk, "Button Ok is not exist for current Tab"+this.name());
		this.btOk.press(); 
	}
}