package dpat.coreLib._model.motionSignals;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import dpat.coreLib._model.widgets.CheckBox;
import dpat.coreLib.common.Do;
import dpat.coreLib.common.config.CommonConfigNew;
import dpat.coreLib.common.config.IniFilesParameters.NvExceptionFatalNoValue;
import dpat.coreLib.indicators.NumberWithIndependentUnitsDataInd;
import dpat.coreLib.indicators.NumberWithIndependentUnitsModelEditField;
import dpat.coreLib.indicators.NumberWithUnitsDataInd.UnitsTypes;

/**
 * ������(�������), ��������������� ����������� ������� ������ MotionSignals (������).
 * @author v.kulikov
 */
public class Tab {
	// ������ ������� NumberWithIndependentUnitsDataInd(��� ���������������� ��������� ����������, ����� ����������� ������������� � ������� ������ � ���������� ����������).
	private Map<String, NumberWithIndependentUnitsDataInd> numWithIndependentUnitsDataInd = new HashMap<>();
	// ������ ������������� ���������� � ��������� ������������.(NumberWithIndependentUnitsModelEditField).
	private Map<String, NumberWithIndependentUnitsModelEditField> numWithIndependentUnitsModelEditField = new HashMap<>();
	// ������ �������� ���������� �������..
	private Map<String, CheckBox> checkBoxes = new HashMap<>();
	
	/**
	 * ����������� ������� ������� ������ motionSignals, ������ ���������� ��� enum Tabs.
	 * @param appID ������������� ����������. ("Model")
	 * @param commonIDpart ������� ��� ������������� ������ Motion Analysis (fmMotionAnalysis:) 
	 * @param tab ���������, ������������ �������� ���������� �������.
	 */
	public Tab(String appID, 
			   String commonIDpart,
			   CommonConfigNew tab)
	{
		for(String[] dt : tab.getDataIndAssociate()){
			String title = dt[0]; // ����� ������ ���������-����, �� �������� ����� �������� ������ ���������
			
			// ������ ���������� ��������� �����				
			switch(dt[3]){
			case "NumberWithIndependentUnitsDataInd" : // ��������� numIndTabData ���������������� ��������� ������������.
				UnitsTypes unnitsType = UnitsTypes.valueOf(dt[4]); 
				NumberWithIndependentUnitsDataInd nwiudi = 
							new NumberWithIndependentUnitsDataInd(appID, title, commonIDpart+dt[1], commonIDpart+dt[2], unnitsType);
				this.numWithIndependentUnitsDataInd.put(title, nwiudi);
				break;
			case "NumberWithIndependentUnitsModelEditField" : // ��������� editIndependentUnitsIndTabData �������������� ��������� ������������, ����������� �������� �� ���������.
				UnitsTypes unnitType = UnitsTypes.valueOf(dt[4]); 
				NumberWithIndependentUnitsModelEditField nwiumef = 
							new NumberWithIndependentUnitsModelEditField(appID, title, commonIDpart+dt[1], commonIDpart+dt[2], unnitType);
				this.numWithIndependentUnitsModelEditField.put(title, nwiumef);
				break;
			case "CheckBox" :
				this.checkBoxes.put(title, new CheckBox(appID, commonIDpart+dt[2]));
				break;
			default:
			}
		}
	}
	
	/** -------------------------------------<�������>------------------------------------- **/
	
	/** �������� ����� ������ ��� ����������� numWithIndependentUnitsDataInd */
	public Set<String> getNumWithIndependentUnitsDataIndKeySet() {
		return this.numWithIndependentUnitsDataInd.keySet();
	}
	
	/** �������� ����� ������ ��� ����������� numWithIndependentUnitsModelEditField */
	public Set<String> getNumWithIndependentUnitsModelEditFieldKeySet() {
		return this.numWithIndependentUnitsModelEditField.keySet();
	}
	
	/** �������� ����� ������ ��� ��������� checkBoxes */
	public Set<String> getCheckBoxesKeySet() {
		return this.checkBoxes.keySet();
	}
	
	/** �������� ��������������� �������� ��������� �� ��� ������������ */
	public NumberWithIndependentUnitsDataInd getNumWithIndependentUnitsDataIndicator(String title) {
		// �������� ������������� ���������� � �������� ������ �� ������ �������.
		Do.dpAssert(numWithIndependentUnitsDataInd.containsKey(title), 
			"Number Indicator with Independent Units Data "+title+" is not supported", NvExceptionFatalNoValue.class);
		return numWithIndependentUnitsDataInd.get(title);
	}
	
	/** �������� ������������� �������� ��������� �� ��� ������������ */
	public NumberWithIndependentUnitsModelEditField getNumWithIndependentUnitsModelEditFieldIndicator(String title) {
		// �������� ������������� ���������� � �������� ������ �� ������ �������.
		Do.dpAssert(numWithIndependentUnitsModelEditField.containsKey(title), 
			"Number Indicator with Independent Units Model Edit Field "+title+" is not supported", NvExceptionFatalNoValue.class);
		return numWithIndependentUnitsModelEditField.get(title);
	}
	
	/** �������� ������� �� ��� ������������ */
	public CheckBox getCheckBox(String title) {
		// �������� ������������� �������� � �������� ������ �� ������ �������.
		Do.dpAssert(checkBoxes.containsKey(title), 
			"CheckBox with Title "+title+" is not supported", NvExceptionFatalNoValue.class);
		return checkBoxes.get(title);
	}
}