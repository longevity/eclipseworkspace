package dpat.coreLib._model.motionSignals;

import java.util.ArrayList;

import dpat.coreLib.common.Do;
import dpat.coreLib.common.config.CommonConfigNew;

/**
 * ����� ������, ���������� ���������� ������� ��� ������� ������� ������ MotionSignals (������).
 * @author v.kulikov
 */
public class SubTabs{
	/**
	 * ������ ���������� �������(Wind/Wave/Current, Draft/Depth/Forces, Additional), ����������� ������
	 * ������� ENV ������ MotionSignals (������).
	 * @author v.kulikov
	 */
	public static enum EnvSubTabs implements CommonConfigNew{
		WIND_WAVE_CURRENT{ // ������� Env->Wind/Wave/Current(Flow).
			@Override
			public void setDataIndAssociate() {
				/*
				 * 0 - ��� ���������.
				 * 1 - ����� �������� (��������� � ������� ������ ����������)
				 * 2 - ����� �����������(��������� � ������� ��������� - title)
				 * 3 - ����� ����������
				 * 4 - ��� �����������
				 */
				dataIndAssociate.add(new String[] {"Wind speed", "edWindSpeed", "lWindSpeed1", "NumberWithIndependentUnitsModelEditField", "WIND_SPEED"}); // �������� ����� [m/s].
				dataIndAssociate.add(new String[] {"Wind speed Change Time", "edWinddTime", "Label17", "NumberWithIndependentUnitsModelEditField", "TIME"}); // ��������� �������� ����� �� �������.
				dataIndAssociate.add(new String[] {"Direction of wind", "edWindAngle", "lWindAngle1", "NumberWithIndependentUnitsModelEditField", "ANGLE"}); // ����������� ����� [�������].
				dataIndAssociate.add(new String[] {"Direction of wind Change Time", "edWinddDirTime", "Label18", "NumberWithIndependentUnitsModelEditField", "TIME"}); // ��������� ����������� ����� �� �������.
				dataIndAssociate.add(new String[] {"�", "windKA", "Label16", "NumberWithIndependentUnitsModelEditField", "ANGLE"}); // ��������� ����������� ����� �� �������. [�].
				dataIndAssociate.add(new String[] {"s", "windKT", "lbls3", "NumberWithIndependentUnitsModelEditField", "TIME"}); // �������� ����� [m/s].
				dataIndAssociate.add(new String[] {"Current speed", "edCurrSpeed", "Label1", "NumberWithIndependentUnitsModelEditField", "VELOCITY"}); // �������� �������.
				dataIndAssociate.add(new String[] {"Current speed Change Time", "edCurrdTime", "Label52", "NumberWithIndependentUnitsModelEditField", "TIME"}); // ��������� �������� ������� �� �������.
				dataIndAssociate.add(new String[] {"Direction of current", "edCurrAngle", "Label3", "NumberWithIndependentUnitsModelEditField", "ANGLE"}); // ����������� �������.
				dataIndAssociate.add(new String[] {"Direction of current Change Time", "edCurrdDirTime", "Label50", "NumberWithIndependentUnitsModelEditField", "TIME"}); // ��������� ����������� ������� �� �������.
				dataIndAssociate.add(new String[] {"Wave height (significant)", "edWaveHeight", "Label4", "NumberWithIndependentUnitsModelEditField", "SHORT_DISTANCE"}); // ������ �����.
				dataIndAssociate.add(new String[] {"Direction of wave", "edWaveAngle", "Label6", "NumberWithIndependentUnitsModelEditField", "ANGLE"}); // ����������� �����.
				
				dataIndAssociate.add(new String[] {"Variable wind direction", null, "cbWindVar", "CheckBox", null});
				dataIndAssociate.add(new String[] {"Direction of wind", null, "chHelicopter", "CheckBox", null});
			}
		},
		DRAFT_DEPTH_FORCES{ // ������� Env->Draft/Depth/Forces.
			@Override
			public void setDataIndAssociate() {
				dataIndAssociate.add(new String[] {"Water depth", "edDepth", "Label5", "NumberWithIndependentUnitsModelEditField", "SHORT_DISTANCE"}); // �������.
				dataIndAssociate.add(new String[] {"Draft froward", "edDraftFwd", "lblDraftFwdTitle", "NumberWithIndependentUnitsModelEditField", "SHORT_DISTANCE"}); // ������ � ����.
				dataIndAssociate.add(new String[] {"Draft aft", "edDraftAft", "Label21", "NumberWithIndependentUnitsModelEditField", "SHORT_DISTANCE"}); // ������ � �����.	
				dataIndAssociate.add(new String[] {"Ice Force", "edIceForce", "Label44", "NumberWithIndependentUnitsModelEditField", "FORCE"}); // ������������� ���� �������� �����
				
				dataIndAssociate.add(new String[] {"Up", null, "chkUp", "CheckBox", null});
				dataIndAssociate.add(new String[] {"Down", null, "chkDown", "CheckBox", null});
				
				dataIndAssociate.add(new String[] {"Fx", "edAddFx", "Label12", "NumberWithIndependentUnitsModelEditField", "FORCE"});
				dataIndAssociate.add(new String[] {"Fy", "edAddFy", "Label12", "NumberWithIndependentUnitsModelEditField", "FORCE"});
				dataIndAssociate.add(new String[] {"Mz", "edAddMz", "Label12", "NumberWithIndependentUnitsModelEditField", "FORCE"});
			}
		},
		ADDITIONAL{
			@Override
			public void setDataIndAssociate() { 						
				dataIndAssociate.add(new String[] {"Rudder Disturbance", null, "cbDisturbance", "CheckBox", null});
			}
		};
		
		// ��������� ��� �������� �������� �������� ��������� ����������� ������
		protected ArrayList<String[]> dataIndAssociate = new ArrayList<String[]>();
		
		/**
		 * ��������� �������� �������� ��������� ����������� ������. 
		 * ����������� ������������� ��� ������ ������ (���������� enum) ����������.
		 */
		abstract void setDataIndAssociate();
		
		private EnvSubTabs(){
			setDataIndAssociate(); // ��������� ��� ������ ���������� ������� �������� � �������������� �����������.		
		}
		
		/**
		 * ��������� �������� �������� ��������� ����������� ������
		 * @return ��������� ��� �������� �������� �������� ��������� ����������� ������
		 */
		@Override
		public ArrayList<String[]> getDataIndAssociate(){
			Do.dpAssert(!this.dataIndAssociate.isEmpty(), "dataIndAssociate is Empty For Tab "+this.name());
			return this.dataIndAssociate; 
		}
	}
}