package dpat.coreLib._model.motionSignals;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import dpat.coreLib._model.motionSignals.Tabs;
import dpat.coreLib.common.Do;
import dpat.coreLib.common.config.CommonConfigNew;
import dpat.coreLib.common.config.IniFilesParameters.NvExceptionFatalNoValue;
import dpat.coreLib.indicators.NumberWithIndependentUnitsModelEditField;

/**
 * ��������� ������ Motion Analysis(������� Info, ENV, Motion...).
 * @author v.kulikov
 */
public class ModelPanel {
	private String commonIDpart = "fmMotionAnalysis:"; // ������� ��� ������������� ������ Motion Analysis
	private String appID = "Model"; // ������������� ����������.
	
	private Map<Tabs, Tab> tabs = new HashMap<>(); // ������� �������(Info, ENV, Motion) ������ MotionSignals.
	private Map<Tabs, Map<CommonConfigNew, Tab>> subtabs = new HashMap<>(); // ���������� ������� ������� �������.
	
	public ModelPanel(){
		for(Tabs tab : Tabs.values()){
			Do.debug("Initializated Tab "+tab.name());
			
			Map<CommonConfigNew, Tab> subTabsPanels = new HashMap<>();
			
			for(CommonConfigNew subTab : tab.getSubTabs()){
				subTabsPanels.put(subTab, new Tab(appID, commonIDpart, subTab));
			}
			
			if(subTabsPanels.isEmpty()){
				tabs.put(tab, new Tab(appID, commonIDpart, tab));
			} else {
				subtabs.put(tab, subTabsPanels);
			}
		}
	}
	
	/**
	 * ����� ��� ��������� ���������� ����� � ��� ��������� � ������.
	 * @param windSpeed �������� �����. ����������� m/s.
	 * @param windDir ����������� �����. 0-360 ��������.
	 */
	public void setWind(double windSpeed, double windDir){
		setDisturbance("Wind speed", "Direction of wind", windSpeed, windDir);
	}
	
	/**
	 * ����� ��� ��������� ���������� ������� � ��� ��������� � ������.
	 * @param currentSpeed �������� �������. ����������� �/�.
	 * @param currentDir ����������� �������. 0-360 ��������.
	 */
	public void setCurrent(double currentSpeed, double currentDir){
		setDisturbance("Current speed", "Direction of current", currentSpeed, currentDir);
	}
	
	/**
	 * ����� ��� ��������� ���������� ����� � � ��������� � ������.
	 * @param waveHeight ������ �����. ����������� �����.
	 * @param waveDir ����������� �����. 0-360 ��������.
	 */
	public void setWave(double waveHeight, double waveDir){
		setDisturbance("Wave height (significant)", "Direction of wave", waveHeight, waveDir);
	}
	
	/**
	 * ����� ��� ��������� ���������� �������� ����������(�����, �����, �������) � ��� ��������� � ������.
	 * @param speedOrHeight �������� ��� ������. (������������ ����� ��� ��������� ����������)
	 * @param dir ����������� (������������ ����� ��� ��������� ����������)
	 * @param valSpeedOrHeight �������� �������� ��� ������.
	 * @param valDirection �������� �����������
	 */
	private void setDisturbance(String speedOrHeight, String dir, double valSpeedOrHeight, double valDirection){
		Tab pnWindWaveCurrent = this.getSubTabs(Tabs.ENV).get(SubTabs.EnvSubTabs.WIND_WAVE_CURRENT);
		
		NumberWithIndependentUnitsModelEditField indSpeedOrHeight = pnWindWaveCurrent.getNumWithIndependentUnitsModelEditFieldIndicator(speedOrHeight);
		NumberWithIndependentUnitsModelEditField indDirection = pnWindWaveCurrent.getNumWithIndependentUnitsModelEditFieldIndicator(dir);
		Do.debug("Change "+speedOrHeight+" in Model to value "+indSpeedOrHeight+" "+indSpeedOrHeight.getUnits()+
														" and direction to "+indDirection+" "+indDirection.getUnits()+".");
		indSpeedOrHeight.setValue(valSpeedOrHeight);
		indDirection.setValue(valDirection);
		Tabs.ENV.pressOk();
	}
	
	/**
	 * ���������� ���� ��� ������ � ������.
	 * @param force Fx, Fy, Mz, Ice Force (����. �����)
	 * @param value ��������� ��������
	 */
	public void setForce(ForceModes force, double value){
		Tab pnDraftDepthForces = this.getSubTabs(Tabs.ENV).get(SubTabs.EnvSubTabs.DRAFT_DEPTH_FORCES);
		
		NumberWithIndependentUnitsModelEditField _force = pnDraftDepthForces.getNumWithIndependentUnitsModelEditFieldIndicator((ForceModes.Ice_Force.equals(force)) ? "Ice Force" : force.name());

		Do.debug("Change force "+force+" in Model to value "+value+" "+_force.getUnits());
		
		_force.setValue(value);
		Tabs.ENV.pressOk();
	}
	
	/**
	 * ���������� ������ �� ���� ��� ����� ����� � ������.
	 * @param ModesDraft Up, Down, ValDraftForwardWithOutUp, ValDraftAftWithOutDown
	 * @param value ��������� ��������.
	 */
	public void setDraft(ModesDraft modeDraft, double value){
		Do.dpAssert(value > 0.0, "Value of Draft is not positive number");
		
		Tab pnDraftDepthForces = this.getSubTabs(Tabs.ENV).get(SubTabs.EnvSubTabs.DRAFT_DEPTH_FORCES);
		
		NumberWithIndependentUnitsModelEditField indDraft = pnDraftDepthForces.getNumWithIndependentUnitsModelEditFieldIndicator(modeDraft.displayName());
		indDraft.setValue(value);

		Tabs.ENV.pressOk();
	}
	
	/**
	 * ����� ��� �������� ���������� ������� ������� � ������(�����/�������/�����).
	 * @return true - �����������, false - ���������� ������� ������� � ������.
	 */
	public boolean isGeneralDisturbanceExists(){
		Tab pnWindWaveCurrent = this.getSubTabs(Tabs.ENV).get(SubTabs.EnvSubTabs.WIND_WAVE_CURRENT);

		// ��������� ����������� ���� + �������� ������� Env -> Wind_Wave_Current �� ���������� �����.
		for(String keyChBox : pnWindWaveCurrent.getCheckBoxesKeySet()){
			Do.dpAssert(null != pnWindWaveCurrent.getCheckBox(keyChBox), "Failed! CheckBox "+pnWindWaveCurrent.getCheckBox(keyChBox).getCaption()+" is null!");
			if(pnWindWaveCurrent.getCheckBox(keyChBox).isTickON()) return true; 
		}
		
		for(String key : pnWindWaveCurrent.getNumWithIndependentUnitsModelEditFieldKeySet()){
			if(!pnWindWaveCurrent.getNumWithIndependentUnitsModelEditFieldIndicator(key).isEqualToValue(0.0)) return true;
		}		
		return false;
	}

	/**
	 * ����� ��� ����������� ������� ������� � ������(�����/�������/�����). 
	 */
	public void deactivateGeneralDisturbance(){
		// �������� ���� ������������� ����������� � ������ ����� � ���������
		if(isGeneralDisturbanceExists()){
			Tab pnWindWaveCurrent = this.getSubTabs(Tabs.ENV).get(SubTabs.EnvSubTabs.WIND_WAVE_CURRENT);
			
			for(String keyChBox : pnWindWaveCurrent.getCheckBoxesKeySet()){
				Do.dpAssert(null != pnWindWaveCurrent.getCheckBox(keyChBox), "Failed! CheckBox "+pnWindWaveCurrent.getCheckBox(keyChBox).getCaption()+" is null!");
				pnWindWaveCurrent.getCheckBox(keyChBox).tickOFF();
				pnWindWaveCurrent.getCheckBox(keyChBox).mustBeTickOFF();
			}
			
			for(String key : pnWindWaveCurrent.getNumWithIndependentUnitsModelEditFieldKeySet()){
				pnWindWaveCurrent.getNumWithIndependentUnitsModelEditFieldIndicator(key).setValue(0.0);
			}
			Tabs.ENV.pressOk();
		}
		Do.debug("All fields of Motion->Env aligned To Zero in Model.");
	}
	
	/** �������� ����� ������ ������� �������. */
	public Set<Tabs> getTabsKeySet() {
		return this.tabs.keySet();
	}
	
	/** ��������� ������ ����������� ������� **/
	public Tab getTab(Tabs tab) {
		// �������� ������������� ������� ������� (�������� ENV).
		Do.dpAssert(tabs.containsKey(tab), "Current tab : "+tab.name()+" is not supported", NvExceptionFatalNoValue.class);
		return tabs.get(tab);
	}
	
	/** �������� ����� ������ ���������� �������. */
	public Set<Tabs> getSubTabsKeySet() {
		return subtabs.keySet();
	}

	/**
	 * �����, ��� ���� - �������� ����� ����������� �������, 
	 * �������� - �����, ��� ���� - ���������� �������, �������� - ������.
	 * @return Map<Tabs, Map<SubTabs, Tab>> subtabs 
	 */
	public Map<CommonConfigNew, Tab> getSubTabs(Tabs tab) {
		// �������� ������������� ������� ������� (�������� ENV).
		Do.dpAssert(subtabs.containsKey(tab), "Current tab : "+tab.name()+" is not supported", NvExceptionFatalNoValue.class);
		return subtabs.get(tab);
	}
	
	public enum ModesDraft { 
		DraftForward("Draft froward"), DraftAft("Draft aft"); // ������� ������ �� ������
		
		private String draft;
		
		ModesDraft(String draft) { this.draft = draft; }
	
	    public String displayName() { return draft; }
	}
	
	public enum ForceModes { Fx, Fy, Mz, Ice_Force; }
}